unit IkkatsuHenkou4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, DB, DBTables;

type
  TfrmIkkatsuHenkou4 = class(TfrmMaster)
    Label3: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    Label17: TLabel;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    Edit1: TEdit;
    Label4: TLabel;
    Label7: TLabel;
    Edit2: TEdit;
    Label8: TLabel;
    Panel4: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    RadioGroup2: TRadioGroup;
    ComboBox1: TComboBox;
    ComboBox2: TComboBox;
    ComboBox3: TComboBox;
    Edit3: TEdit;
    Edit4: TEdit;
    RadioButton3: TRadioButton;
    RadioButton4: TRadioButton;
    Edit5: TEdit;
    Edit6: TEdit;
    Panel5: TPanel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RadioGroup3: TRadioGroup;
    CBCode1: TComboBox;
    CBName: TComboBox;
    CBYomi: TComboBox;
    EditTankaS: TEdit;
    EditTankaL: TEdit;
    RadioButton5: TRadioButton;
    RadioButton6: TRadioButton;
    EditYen: TEdit;
    EditPercent: TEdit;
    Label27: TLabel;
    EditTankaS_New: TEdit;
    EditTankaL_New: TEdit;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    EditChainCode: TEdit;
    Query1: TQuery;
    LabelSID: TLabel;
    Label32: TLabel;
    CBCode2: TComboBox;
    Query2: TQuery;
    Query3: TQuery;
    Label33: TLabel;
    RG4: TRadioGroup;
    RB_S: TRadioButton;
    RB_L: TRadioButton;
    Label31: TLabel;
    EditAria: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure CBYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure RadioButton5Click(Sender: TObject);
    procedure RadioButton6Click(Sender: TObject);
    procedure EditShuukeiCodeChange(Sender: TObject);
    procedure EditYenExit(Sender: TObject);
    procedure EditPercentExit(Sender: TObject);
    procedure EditChainCodeEnter(Sender: TObject);
    procedure RB_SClick(Sender: TObject);
    procedure RB_LClick(Sender: TObject);
    procedure EditAriaEnter(Sender: TObject);
  private
    { Private declarations }
		procedure MakeCBName(sKey, sWord:String; iKubun:Integer);
		procedure MakeForm(sid:integer);
  public
    { Public declarations }
  end;

var
  frmIkkatsuHenkou4: TfrmIkkatsuHenkou4;

implementation
uses
	HKLib, Inter, DM1, PasswordDlg;

{$R *.dfm}

procedure TfrmIkkatsuHenkou4.FormCreate(Sender: TObject);
begin
  inherited;
	Width := 550;
  Height :=380;

end;

procedure TfrmIkkatsuHenkou4.CBYomiKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    EditTankaS_New.Text := '0';
    EditTankaL_New.Text := '0';
    CBName.SetFocus;
	 	MakeCBName('Yomi', CBYomi.Text, 1);
  end;
end;

{
	仕入先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmIkkatsuHenkou4.MakeCBName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('SID').AsString +
       ',' + FieldByName('Name').AsString +
       ',' + FieldByName('Irisuu').AsString +
       ',' + FieldByName('Kikaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBName.DroppedDown := True;
end;

procedure TfrmIkkatsuHenkou4.CBNameExit(Sender: TObject);
var
	sid : integer;
  sItemName : string;
begin
  if CBName.Text = '' then begin
    exit;
  end;
	//SIDを取得して検索する
  if (pos(',', CBName.Text)>0) then begin
    sid := StrToInt(Copy(CBName.Text, 1, (pos(',', CBName.Text)-1)));
    sItemName := Copy(
                     CBName.Text,
                     (pos(',', CBName.Text)+1),
                     Length(CBName.Text)
                    );
    if (pos(',',sItemName)>1) then begin
      sItemName := Copy(sItemName, 1, pos(',',sItemName)-1);
    end;
    CBName.Text :=sItemName;
  end else begin
     if (LabelSID.caption <> '' ) then begin
      sid := StrToInt(LabelSID.caption);
     end else begin
      sid := 0;
     end;
  end;

  if sid > 0 then begin
	  MakeForm(sid);
  end;

end;

//SIDからフォームを表示する
procedure TfrmIkkatsuHenkou4.MakeForm(sid:integer);
var
	sSql : String;
  i : Integer;
begin
	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' SID =  ' + IntToStr(sid);
  with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
      LabelSID.Caption  := FieldbyName('SID').AsString;
			CBYomi.Text       := FieldbyName('Yomi').AsString;;
			//CBTanni.Text      := FieldbyName('Tanni').AsString;;
			//CBCode2.Text      := FieldbyName('Code2').AsString;;
			//CBIrisuu.Text     := FieldbyName('Irisuu').AsString;;
			//CBCase1.Text      := FieldbyName('Case1').AsString;;
{			CBF1.Text         := FieldbyName('F1').AsString;;
			CBF2.Text         := FieldbyName('F2').AsString;;
}
			EditTankaS.Text := FieldbyName('Tanka420').AsString;;
			EditTankaL.Text := FieldbyName('Tanka450').AsString;;
{
			EditGenka.Text    := FieldbyName('Genka').AsString;;
			CBMaker.Text      := FieldbyName('Maker').AsString;;
			CBShiiresaki.Text := FieldbyName('ShiiresakiCode').AsString;;
			CBKikaku.Text     := FieldbyName('Kikaku').AsString;;
			//CBBunrui.Text     := FieldbyName('Bunrui').AsString;;
}
			CBCode1.Text     := FieldbyName('Code1').AsString;;
			CBCode2.Text     := FieldbyName('Code2').AsString;;
			//EditMemo.Text     := FieldbyName('Memo').AsString;;

    end;
    Close;
  end;
end;

//一括変更ボタンがクリックされた
procedure TfrmIkkatsuHenkou4.BitBtn2Click(Sender: TObject);
var
  S_Tanka, L_Tanka : Integer;
  iYen, iPercent, iChainCode, iShuukeiCode :Integer;
  sTemp, sSql,sSql2 : String;

  sSqlT,sSqlM,sSqlSet,sSqlSet2,sDate,sSqlWhere :String;//add utada 2009.06
  sTanka, sName,sID : String;
  sCode1, sCode2, sTokuisakiCode1 : String;
begin
//エラーチェック
  iYen         := StrToInt(EditYen.Text);
  iPercent     := StrToInt(EditPercent.Text);
  iChainCode   := StrToInt(EditChainCode.Text);
  sCode1       := CBCode1.Text;
  sCode2       := CBCode2.Text;
  S_Tanka      := StrToInt(EditTankaS_New.Text);
  L_Tanka      := StrToInt(EditTankaL_New.Text);
  if (iYen=0) and (iPercent = 0) then begin
    ShowMessage('変更する円か%どちらかを入力してください');
    exit;
  end;

  if (sCode1='') and (sCode2 = '') then begin
    ShowMessage('商品コードが特定されていません');
    exit;
  end;

	//確認メッセージ
  if MessageDlg('価格一括変更しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

//書き換え
// Code1 Code2 で商品が特定されて、
// TokuisakiCodeでループを回す
//     ChainCode か　ShuukeiCode でセレクト
//　　 Kubun420450 で条件分岐
  sSql2 := 'Select TokuisakiCode1, ChainCode, ShuukeiCode, Kubun420450';
  sSql2 := sSql2 + ' from ' + CtblMTokuisaki;
  if (iChainCode <> 0) then begin
    sSql2 := sSql2 + ' where ';
    sSql2 := sSql2 + ' ChainCode = ' + IntToStr(iChainCode);
  end else if (RB_S.Checked=True) then begin
    sSql2 := sSql2 + ' where ';
    sSql2 := sSql2 + ' kubun420450 = 420'
  end else if (RB_L.Checked=True) then begin
    sSql2 := sSql2 + ' where ';
    sSql2 := sSql2 + ' kubun420450 = 450'
  end else if (EditAria.Text<>'0') then begin
    sSql2 := sSql2 + ' where ';
    sSql2 := sSql2 + ' TokuisakiCode2 = ' + Trim(EditAria.Text);
  end;

  sDate := DateToStr(Date);

  sSql2 := sSql2 + ' order by TokuisakiCode1';
  with Query2 do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql2);
		Open;
    while not EOF do begin
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      {
      if FieldByName('Kubun420450').AsString = '420' then begin
        sTanka := IntToStr(S_Tanka);
      end else if FieldByName('Kubun420450').AsString = '450' then begin
        sTanka := IntToStr(L_Tanka);
      end else begin
        ShowMessage('得意先コード=(' + sTokuisakiCode1 + ')の価格区分が明確ではありません。処理を注しします。');
        exit;
      end;
      }

	    sSqlT := 'UPDATE ' + CtblMItem2;

      sSqlSet := ' SET ';
//      sSql := sSql + ' Tanka = ' + sTanka;
      if iYen<>0 then begin
        sSqlSet := sSqlSet + ' Tanka = Tanka + ' + IntToStr(iYen);
      end; //og if
      if iPercent <> 0 then begin
        sSqlSet := sSqlSet + ' Tanka = Tanka * ' + FloatToStr(1+iPercent/100);
      end; //og if

      //InfoSendDateのを初期化 add 2014.09.10
      sSqlSet2 := sSqlSet + ',InfoSendDate = NULL';

    	sSqlWhere := ' WHERE ';
      sSqlWhere := sSqlWhere + ' TokuisakiCode = ' + sTokuisakiCode1;
      sSqlWhere := sSqlWhere + ' and Code1 = ''' + sCode1 + '''';
      sSqlWhere := sSqlWhere + ' and Code2 = ''' + sCode2 + '''';

      sSqlT := sSqlT + sSqlSet2 + sSqlWhere; //tem2   Add utada 2009.06
      with Query3 do begin
        Close;
		    Sql.Clear;
        Sql.Add(sSqlT);
		    ExecSql;
        Close;
      end;//of with


	    sSqlM := 'UPDATE ' + CtblMItem3;
      sSqlSet := sSqlSet + ', Bikou = ''' + sDate + '価格変更, '' + Bikou';
      sSqlM := sSqlM + sSqlSet + sSqlWhere; //item3  Add utada 2009.06
      with Query3 do begin
        Close;
		    Sql.Clear;
        Sql.Add(sSqlM);
		    ExecSql;
        Close;
      end;//of with

      sTemp := sTemp + sTokuisakiCode1 + ',';
      SB1.SimpleText := sTokuisakiCode1;
      SB1.Update;
      next;
    end;//of while



  end;// of whith

  ShowMessage('価格変更完了しました。');

end;

procedure TfrmIkkatsuHenkou4.RadioButton5Click(Sender: TObject);
begin
  EditPercent.Text := '0';
  EditYen.SetFocus;
end;

procedure TfrmIkkatsuHenkou4.RadioButton6Click(Sender: TObject);
begin
  EditYen.Text := '0';
  EditPercent.SetFocus;
end;

procedure TfrmIkkatsuHenkou4.EditShuukeiCodeChange(Sender: TObject);
begin
  EditChainCode.Text := '0';
end;

procedure TfrmIkkatsuHenkou4.EditYenExit(Sender: TObject);
begin
  if StrToInt(EditYen.Text)<>0 then begin
    EditTankaS_New.Text := IntToStr(StrToInt(EditTankaS.Text) + StrToInt(EditYen.Text));
    EditTankaL_New.Text := IntToStr(StrToInt(EditTankaL.Text) + StrToInt(EditYen.Text));
  end;

end;

procedure TfrmIkkatsuHenkou4.EditPercentExit(Sender: TObject);
begin
  if StrToInt(EditPercent.Text)<>0 then begin
    EditTankaS_New.Text := FloatToStr(
       StrToInt(EditTankaS.Text) *
       (1 + StrToInt(EditPercent.Text)/100 )
     );
    EditTankaL_New.Text := FloatToStr(StrToInt(EditTankaL.Text) *
     (1 + StrToInt(EditPercent.Text)/100));
  end;

end;

procedure TfrmIkkatsuHenkou4.EditChainCodeEnter(Sender: TObject);
begin
  RB_S.Checked := False;
  RB_L.Checked := False;
  EditAria.Text := '0';
end;

procedure TfrmIkkatsuHenkou4.RB_SClick(Sender: TObject);
begin
  EditChainCode.Text:='0';
  EditAria.Text := '0';
end;

procedure TfrmIkkatsuHenkou4.RB_LClick(Sender: TObject);
begin
  EditChainCode.Text:='0';
  EditAria.Text := '0';
end;

procedure TfrmIkkatsuHenkou4.EditAriaEnter(Sender: TObject);
begin
  RB_S.Checked := False;
  RB_L.Checked := False;
  EditChainCode.Text:='0';
end;

end.
