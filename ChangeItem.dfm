object frmChangeItem: TfrmChangeItem
  Left = 568
  Top = 265
  Width = 450
  Height = 236
  Caption = 'frmChangeItem'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = True
  Position = poMainFormCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Label3: TLabel
    Left = 56
    Top = 64
    Width = 96
    Height = 15
    Caption = #20803#20181#20837#20808#26908#32034
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 56
    Top = 104
    Width = 96
    Height = 15
    Caption = #26032#20181#20837#20808#26908#32034
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clRed
    Font.Height = -15
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 442
    Height = 41
    Align = alTop
    Color = clTeal
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 180
      Height = 19
      Alignment = taCenter
      Caption = #20181#20837#20808#19968#25324#22793#26356#30011#38754
      Color = clTeal
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label2: TLabel
      Left = 218
      Top = 24
      Width = 251
      Height = 13
      AutoSize = False
      Caption = #40441#26494#23627#22770#25499#31649#29702#12471#12473#12486#12512' Ver5.00'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 161
    Width = 442
    Height = 41
    Align = alBottom
    Color = clTeal
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 304
      Top = 8
      Width = 89
      Height = 25
      Caption = #38281#12376#12427'(&Z)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkIgnore
    end
    object btnUpdate: TBitBtn
      Left = 192
      Top = 8
      Width = 97
      Height = 25
      Caption = #19968#25324#22793#26356
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btnUpdateClick
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333444444
        33333333333F8888883F33330000324334222222443333388F3833333388F333
        000032244222222222433338F8833FFFFF338F3300003222222AAAAA22243338
        F333F88888F338F30000322222A33333A2224338F33F8333338F338F00003222
        223333333A224338F33833333338F38F00003222222333333A444338FFFF8F33
        3338888300003AAAAAAA33333333333888888833333333330000333333333333
        333333333333333333FFFFFF000033333333333344444433FFFF333333888888
        00003A444333333A22222438888F333338F3333800003A2243333333A2222438
        F38F333333833338000033A224333334422224338338FFFFF8833338000033A2
        22444442222224338F3388888333FF380000333A2222222222AA243338FF3333
        33FF88F800003333AA222222AA33A3333388FFFFFF8833830000333333AAAAAA
        3333333333338888883333330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
    end
  end
  object cbxShiiresakiNameOld: TComboBox
    Left = 168
    Top = 64
    Width = 193
    Height = 20
    ImeMode = imOpen
    ItemHeight = 12
    TabOrder = 2
    OnKeyDown = cbxShiiresakiNameOldKeyDown
  end
  object cbxShiiresakiNameNew: TComboBox
    Left = 168
    Top = 104
    Width = 193
    Height = 20
    ImeMode = imOpen
    ItemHeight = 12
    TabOrder = 3
    OnKeyDown = cbxShiiresakiNameNewKeyDown
  end
  object QueryMShiiresaki: TQuery
    DatabaseName = 'taka'
    Left = 16
    Top = 56
  end
  object QueryShiiresakiUpdate: TQuery
    DatabaseName = 'taka'
    Left = 16
    Top = 96
  end
end
