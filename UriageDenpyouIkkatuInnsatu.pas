unit UriageDenpyouIkkatuInnsatu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables, DBCtrls;

type
  TfrmUriageDenpyouIkkatuInnsatu = class(TfrmMaster)
    pnlNouhinDate: TPanel;
    pnlCourse: TPanel;
    pnlDenpyouBanngou: TPanel;
    pnlTokuisakiName: TPanel;
    pnlStart: TPanel;
    pnlEnd: TPanel;
    dtpNouhinDate: TDateTimePicker;
    edbStart: TEdit;
    edbEnd: TEdit;
    Label3: TLabel;
    cmbTokuisakiName: TComboBox;
    Label4: TLabel;
    qryUriageDenpyouIkkatuInnsatu: TQuery;
    cmbCourse: TComboBox;
    procedure cmbTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
    procedure Button1Click(Sender: TObject);
    procedure cmbTokuisakiNameExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbCourseExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  frmUriageDenpyouIkkatuInnsatu: TfrmUriageDenpyouIkkatuInnsatu;

implementation

uses Inter, DMMaster, TokuisakiKensaku, DenpyouPrint;

{$R *.DFM}


procedure TfrmUriageDenpyouIkkatuInnsatu.FormCreate(Sender: TObject);
var
 sSql : String;
begin

  // 納品日を当日に設定
  dtpNouhinDate.Date := Date();

  // コンボボックスクリア
  cmbCourse.Items.Clear;

try

  // コンボボックスリスト作成
	sSql := 'SELECT CourseName FROM ' + CtblMCourse;
  sSql := sSql + ' order by CourseID ';

  cmbCourse.Clear;

	with qryUriageDenpyouIkkatuInnsatu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	cmbCourse.Items.Add(FieldByName('CourseName').AsString);
    	Next;
    end;
    Close;
  end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmUriageDenpyouIkkatuInnsatu.cmbTokuisakiNameKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin

  // 得意先名簡易検索
	if (Key=VK_F1) then
    begin
     	MakeCBTokuisakiName('TokuisakiNameYomi', cmbTokuisakiName.Text, 1);
    end;

end;


procedure TfrmUriageDenpyouIkkatuInnsatu.MakeCBTokuisakiName(
  sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  // コンボボックスクリア
  cmbTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  cmbTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	cmbTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
      FieldByName('TokuisakiNameUp').AsString + ' ' + 
      FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  cmbTokuisakiName.DroppedDown := True;

end;


procedure TfrmUriageDenpyouIkkatuInnsatu.Button1Click(Sender: TObject);
begin

  // 得意先簡易検索画面オープン
  TfrmTokuisakiKensaku.Create(Self);

end;

procedure TfrmUriageDenpyouIkkatuInnsatu.cmbTokuisakiNameExit(
  Sender: TObject);
var
 sSql              : String;
 strTokuisakiCode1 : String;
 strTokuisakiCode2 : String;

begin


 // 得意先名が決まるとコースが一意に決まる筈なので，
 // コースの表記をその得意先名に応じたものに自動的に変更する．

  strTokuisakiCode1 := Copy(cmbTokuisakiName.Text, 1, (pos(',', cmbTokuisakiName.Text)-1));

//#thuyptt 20190313
  if strTokuisakiCode1 = '' then
     strTokuisakiCode1 := '0';

try

  // tblMTokuisaki.TokuisakiCode2がコースIDである．これを引く
  sSql := 'SELECT TokuisakiCode2 FROM ' + CtblMTokuisaki;
//  sSql := sSql + ' WHERE TokuisakiCode1 = ' + '''' + strTokuisakiCode1 + '''';  #thuyptt 20190313
     sSql := sSql + ' WHERE TokuisakiCode1 = ' + '' + strTokuisakiCode1 + '';
  with qryUriageDenpyouIkkatuInnsatu do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    strTokuisakiCode2 := FieldbyName('TokuisakiCode2').AsString;
    Close;
  end;

// #thuyptt 20190313
  if strTokuisakiCode2 = '' then
    strTokuisakiCode2 := '0';  

 // コースのコンボボックスの表記を得意先名に合った物にする．
  sSql := 'SELECT CourseName FROM ' + CtblMCourse;
//  sSql := sSql + ' WHERE CourseID = ' + '''' + strTokuisakiCode2 + ''''; #thuyptt 20190313
  sSql := sSql + ' WHERE CourseID = ' + '' + strTokuisakiCode2 + '';
  with qryUriageDenpyouIkkatuInnsatu do begin
	 	Close;
    Sql.Clear;
  	Sql.Add(sSql);
    Open;
    cmbCourse.Text := FieldbyName('CourseName').AsString;
    Close;
  end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmUriageDenpyouIkkatuInnsatu.cmbCourseExit(Sender: TObject);
begin

 // コースを変更したら，得意先名をクリアする．
 cmbTokuisakiName.Text := '';

end;


procedure TfrmUriageDenpyouIkkatuInnsatu.BitBtn2Click(Sender: TObject);
var
 sNouhinDate     : String;
 sCourseID       : String;
 sTokuisakiCode1 : String;
 sStart          : String;
 sEnd            : String;
 sSql            : String;
 i               : Integer;
 iRecordCount    : Integer;
 pMessage        : PChar;

begin

 // 画面に入力された条件に該当する伝票番号のリストを作成・グローバル変数にセットし，
 // 確認の小窓を表示する．（その小窓でDenpyouPrintを呼び出して印刷をする）

try

 //
 // 各パラメータ作成
 //

  // 納品日
  DateTimeToString(sNouhinDate, 'yyyy/mm/dd', dtpNouhinDate.Date);

  // コースID
	sSql := 'SELECT CourseID FROM ' + CtblMCourse;
  sSql := sSql + ' WHERE CourseName = ' + '''' + cmbCourse.Text + '''';
	with qryUriageDenpyouIkkatuInnsatu do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sCourseID :=FieldbyName('CourseID').AsString;
    Close;
  end;

  // 得意先名
  sTokuisakiCode1 := Copy(cmbTokuisakiName.Text, 1, (pos(',', cmbTokuisakiName.Text)-1));

//#thuyptt 20190313
  if sTokuisakiCode1 = '' then
     sTokuisakiCode1 := '0';

  // 伝票番号 Start，End
  sStart := edbStart.Text;
  sEnd   := edbEnd.Text;

 //
 // Sql文作成
 //

// sSql := 'SELECT DISTINCT DenpyouCode From ' + CtblTDenpyouDetail + ' D, ' + CtblMTokuisaki + ' T'; #thuyptt 20190313
 sSql := 'SELECT DISTINCT DenpyouCode From ' + CtblTDenpyouDetail + ' AS D, ' + CtblMTokuisaki + ' AS T';
 sSql := sSql + ' Where (D.TokuisakiCode1 = T.TokuisakiCode1)';

// sSql := sSql + ' AND ( D.NouhinDate = ' + '''' + sNouhinDate + ''''; #thuyptt 20190313
 sSql := sSql + ' AND ( D.NouhinDate = ' + '#' + sNouhinDate + '#';

 if cmbCourse.Text <> '' then
   sSql := sSql + ' and T.TokuisakiCode2 = ' +  sCourseID ;
//   sSql := sSql + ' and T.TokuisakiCode2 = ' + '''' + sCourseID + ''''; #thuyptt 20190313

// if cmbTokuisakiName.Text <> '' then
  if cmbTokuisakiName.Text <> '' then
   sSql := sSql + ' and T.TokuisakiCode1 = ' + sTokuisakiCode1;

 if edbStart.Text <> '' then
   sSql := sSql + ' and DenpyouCode >= ' + '''' + sStart + '''';

 if edbEnd.Text <> '' then
   sSql := sSql + ' and DenpyouCode <= ' + '''' + sEnd + '''';

// sSql := sSql + ' ) ORDER BY DenpyouCode'; #thuyptt 20190313
  sSql := sSql + ' ) ORDER BY DenpyouCode ASC';

 // 伝票番号リスト作成

 with qryUriageDenpyouIkkatuInnsatu do begin
   Close;
   Sql.Clear;
   Sql.Add(sSql);
   Open;

   // メッセージボックスのメッセージに備え，RecordCountを保存
   iRecordCount := RecordCount;

   // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
   if RecordCount =0 then
     begin
       ShowMessage('該当する伝票番号はありません');
       Close;
       Exit;
     end;

   // 次元セット
   SetLength(GArrayOfDenpyouBanngou, RecordCount);

   i := 0;
   while not EOF do begin
     GArrayOfDenpyouBanngou[i] := FieldByName('DenpyouCode').AsString;
     i := i + 1;
     Next;
   end;
   Close;
 end;

 //
 // 一括印刷
 //

 pMessage := PChar( IntToStr(iRecordCount) + '件(' + IntToStr(iRecordCount*2)
                  + 'セット)の伝票を印刷します．よろしいですか？' );

 if Application.MessageBox(pMessage, '印刷確認', MB_OKCANCEL or MB_ICONINFORMATION) = IDOK then
   begin
     TfrmDenpyouPrint.Create(Self);
     ShowMessage('印刷が完了しました');
   end
 else
   begin
     Exit;
   end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


end.
