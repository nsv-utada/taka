unit ListInfoMart;


interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Master, Grids, StdCtrls, Buttons, ComCtrls, ExtCtrls, DB,
  DBTables;

type
  TfrmListInfoMart = class(TfrmMaster)
    DTP1: TDateTimePicker;
    SG1: TStringGrid;
    cmdKennsaku: TButton;
    QueryDenpyou: TQuery;
    BitBtn3: TBitBtn;
    BitBtn7: TBitBtn;
    pnlDenpyouBanngou: TPanel;
    pnlStart: TPanel;
    edbStart: TEdit;
    pnlEnd: TPanel;
    edbEnd: TEdit;

    procedure FormCreate(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure cmdKennsakuClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SG1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);

  private
    { Private declarations }
  	 procedure MakeSG;
  public
    { Public declarations }
  end;
  const
  
  //グリッドの列の定義
  
  CintNum           = 0;
  CintItemCode      = 1;
  CintItemInfoCode  = 2;
  CintItemName      = 3;
  CintItemCount     = 4;
  CintEnd           = 5;

var
  frmListInfoMart: TfrmListInfoMart;

implementation
uses
Inter, DenpyouPrint, Denpyou;

{$R *.dfm}



procedure TfrmListInfoMart.FormCreate(Sender: TObject);
begin
  top  := 5;
  Left := 5;
	Width := 700+50;
  Height := 500;
  DTP1.Date := Date();

  //ストリンググリッドの作成
  MakeSG;
end;

procedure TfrmListInfoMart.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing     := True;
    RowCount           := 10;   // とりあえず最初は10にしておく．MakeSG2の中で動的に設定
    ColCount           := CintEnd;
    Align              := alBottom;
    FixedCols          := 0;
    FixedColor         := clYellow;
    Canvas.Font.Size   := 11;
    DefaultRowHeight   := 20;
    ColWidths[0]       := 64;
    Font.Color         := clNavy;

    // ColWidths[CintNum]:= 0; // 並び順に変更があった為，ここの順番は滅茶苦茶
    // Cells[CintNum, 0] := '分類名';

    ColWidths[CintNum]:= 70;
    Cells[CintNum, 0] := '伝票ID';

    ColWidths[CintItemCode]:= 150;
    Cells[CintItemCode, 0] := '伝票番号';

    ColWidths[CintItemInfoCode]:= 150;
    Cells[CintItemInfoCode, 0] := 'Infomart伝票No';

    ColWidths[CintItemName]:= 180;
    Cells[CintItemName, 0] := '得意先名';

    ColWidths[CintItemCount]:= 130;
    Cells[CintItemCount, 0] := '売上金額';

  end;
end;

procedure TfrmListInfoMart.BitBtn7Click(Sender: TObject);
var
	i,j : Integer;
begin

  // 表示をクリアする．

  with SG1 do begin
    for i := 0 to ColCount - 1 do begin
      for j := 1 to RowCount - 1 do begin
       Cells[i,j] := '';
      end;
    end;
  end;

  edbStart.Text :='';
  edbEnd.Text   :='';

end;

procedure TfrmListInfoMart.cmdKennsakuClick(Sender: TObject);
var
  sNouhinbi        : String;
  sSql             : String;
  i                : Integer;
  gRowNum          : Integer;
begin
  inherited;
    DateTimeToString(sNouhinbi, 'yyyy/mm/dd', DTP1.Date);
    
  // Make SQL
 	  sSql := 'SELECT D.ID,D.DenpyouCode, D.InfoDenpyouNo, D.UriageKingaku, T.TokuisakiName FROM ' + CtblTDenpyou;
    sSql := sSql + ' D INNER JOIN ' + CtblMTokuisaki + ' T ON';
    sSql := sSql + ' D.TokuisakiCode1 = T.TokuisakiCode1 ';
    sSql := sSql + ' WHERE D.DDate = ' + '''' + sNouhinbi + '''';
    sSql := sSql + ' AND (D.InfoDenpyouNo IS NOT NULL)';
    //sSql := sSql + ' ORDER BY CAST (D.InfoDenpyouNo as int) ASC';
    sSql := sSql + ' ORDER BY ID ASC';


    with QueryDenpyou do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      // レコードがなければメッセージを出力して抜ける
      if RecordCount = 0 then
        begin
         ShowMessage('該当する伝票はありません');
         Exit;
        end;
      // RowCountセット
      gRowNum := RecordCount + 1;  // タイトル分1行足してる
      SG1.RowCount := gRowNum;
      i := 0;
      while not EOF do begin
    	  i := i+1;
        with SG1 do begin
      	  Cells[CintNum,           i] := FieldbyName('ID').AsString;
      	  Cells[CintItemCode,      i] := FieldbyName('DenpyouCode').AsString;
          Cells[CintItemInfoCode,  i] := FieldbyName('InfoDenpyouNo').AsString;
  	      Cells[CintItemName,      i] := FieldbyName('TokuisakiName').AsString;
          Cells[CintItemCount,     i] := FieldbyName('UriageKingaku').AsString;
        end;
        next
      end;
      Close;        
    end;  
end;

procedure TfrmListInfoMart.BitBtn3Click(Sender: TObject);
var
 sNouhinbi       : String;
 sSql            : String;
 i               : Integer;
 iRecordCount    : Integer;
 pMessage        : PChar;
 sStart          : String;
 sEnd            : String;

begin

 // 画面に入力された条件に該当する伝票番号のリストを作成・グローバル変数にセットし，
 // 確認の小窓を表示する．（その小窓でDenpyouPrintを呼び出して印刷をする）

try

 //
 // 各パラメータ作成
 //

  // 納品日
  DateTimeToString(sNouhinbi, 'yyyy/mm/dd', DTP1.Date);

   // 伝票番号 Start，End
  sStart := edbStart.Text;
  sEnd   := edbEnd.Text;

  // Make SQL
 	  sSql := 'SELECT D.DenpyouCode, D.InfoDenpyouNo, D.UriageKingaku, T.TokuisakiName FROM ' + CtblTDenpyou;
    sSql := sSql + ' D INNER JOIN ' + CtblMTokuisaki + ' T ON';
    sSql := sSql + ' D.TokuisakiCode1 = T.TokuisakiCode1 ';
    sSql := sSql + ' WHERE D.DDate = ' + '''' + sNouhinbi + '''';
    sSql := sSql + ' AND (D.InfoDenpyouNo IS NOT NULL)';
    if edbStart.Text <> '' then begin
       sSql := sSql + ' and D.ID >= ' + '''' + sStart + '''';
    end;
    if edbEnd.Text <> '' then begin
       sSql := sSql + ' and D.ID <= ' + '''' + sEnd + '''';
    end;

    sSql := sSql + ' ORDER BY D.ID';

 // 伝票番号リスト作成

 with QueryDenpyou do begin
   Close;
   Sql.Clear;
   Sql.Add(sSql);
   Open;

   // メッセージボックスのメッセージに備え，RecordCountを保存
   iRecordCount := RecordCount;

   // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
   if RecordCount =0 then
     begin
       ShowMessage('該当する伝票番号はありません');
       Close;
       Exit;
     end;

   // 次元セット
   SetLength(GArrayOfDenpyouBanngou, RecordCount);

   i := 0;
   while not EOF do begin
     GArrayOfDenpyouBanngou[i] := FieldByName('DenpyouCode').AsString;
     i := i + 1;
     Next;
   end;
   Close;
 end;

 //
 // 一括印刷
 //

 pMessage := PChar( IntToStr(iRecordCount) + '件(' + IntToStr(iRecordCount*2)
                  + 'セット)の伝票を印刷します．よろしいですか？' );

 if Application.MessageBox(pMessage, '印刷確認', MB_OKCANCEL or MB_ICONINFORMATION) = IDOK then
   begin
     TfrmDenpyouPrint.Create(Self);
     ShowMessage('印刷が完了しました');
   end
 else
   begin
     Exit;
   end;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;

procedure TfrmListInfoMart.SG1DblClick(Sender: TObject);
var
iRow          : Integer;
begin
   iRow := SG1.Row;
   //GDenpyouCode := SG1.Cells[1, iRow];
   //GInfoDenpyouno := SG1.Cells[2, iRow];

   frmDenpyou := TfrmDenpyou.Create(Self);

   // Add utada 2012.11.28
   frmDenpyou.cmbDenpyouBanngou.Text  :=  SG1.Cells[1, iRow];
   frmDenpyou.cmdKennsaku.OnClick(Sender);

   {
   frmDenpyou.Visible := False;
   if  (frmDenpyou.ShowModal = mrOk)  then  begin
   end;
   frmDenpyou.Release;
   }
end;

procedure TfrmListInfoMart.FormActivate(Sender: TObject);
begin
  inherited;
  if GDDate <> '' then begin
    DTP1.Date := StrToDate(GDDate);
    cmdKennsaku.OnClick(Sender);
  end;
end;

procedure TfrmListInfoMart.SG1DrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
  var
  dx: integer;
  Text: string;
begin
  if aCol in [4] then
    with SG1.Canvas do
    begin
      FillRect(Rect);
      Text := SG1.cells[aCol, aRow];
      dx := TextWidth(Text) + 2;
      TextOut(Rect.Right - dx, Rect.Top, Text);
    end;
end;

end.
