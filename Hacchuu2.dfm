inherited frmHacchuu2: TfrmHacchuu2
  Left = 453
  Top = 349
  Caption = 'frmHacchuu2'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    inherited Label1: TLabel
      Width = 134
      Caption = '商品発注画面２'
    end
  end
  inherited Panel2: TPanel
    inherited BitBtn1: TBitBtn
      Cancel = True
      Kind = bkCustom
    end
    inherited BitBtn2: TBitBtn
      Left = 306
      Width = 105
      Caption = '発注書印刷'
      Glyph.Data = {00000000}
      Kind = bkCustom
    end
  end
  inherited Panel3: TPanel
    Height = 32
    Align = alTop
    object Label3: TLabel
      Left = 40
      Top = 10
      Width = 218
      Height = 13
      Caption = '仕入先名　：　有限会社ネット･サーフ'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'ＭＳ Ｐゴシック'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object SG1: TStringGrid
    Left = 0
    Top = 73
    Width = 535
    Height = 213
    Align = alClient
    TabOrder = 4
  end
end
