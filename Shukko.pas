unit Shukko;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables,ShellAPI;

type
  TfrmShukko = class(TfrmMaster)
    Label3: TLabel;
    Label4: TLabel;
    chkB1F: TCheckBox;
    chk1F: TCheckBox;
    chk2F: TCheckBox;
    chk3F: TCheckBox;
    chkAll: TCheckBox;
    Label8: TLabel;
    qryShukko: TQuery;
    dtpNouhinDate: TDateTimePicker;
    Label6: TLabel;
    Query1: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure chkAllMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn2Click(Sender: TObject);
    function GetCourseID(CourseName: String) : String;
    function GetShiiresaki(sCode1,sCode2: String) : String;
    procedure chkAllClick(Sender: TObject);
  private
    { Private 宣言 }
    chkCourse:array[1..100] of TCheckBox;
  public
    { Public 宣言 }

  end;

var
  frmShukko: TfrmShukko;

implementation

uses Inter, HKLib;

{$R *.DFM}

procedure TfrmShukko.FormCreate(Sender: TObject);
var
 sSql  : String;
 c     : Integer;
 itemp : Integer;
 strCourseID : String;
 cb: TCheckBox;
 i     : Integer;
 iLeft : Integer;
 iTop  : Integer;

begin

  // 納品日を当日に設定
  dtpNouhinDate.Date := Date();

  // コンボボックスクリア
  //cmbCourse1.Items.Clear;
  //cmbCourse2.Items.Clear;

  i := 0;
  c  := 0;
  iLeft := 105;
  iTop := 110;
try

  // コンボボックスリスト作成
	sSql := 'SELECT CourseID,CourseName FROM ' + CtblMCourse;
  sSql := sSql + ' order by CourseID ';

  //cmbCourse1.Clear;
  //cmbCourse2.Clear;

	with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin

    strCourseID := FieldByName('CourseID').AsString;

    {
      if (StrToInt(strCourseID) >=70) and (StrToInt(strCourseID) < 80) then begin
          iTop70 := iTop70 + 18 ;
          iTop := iTop70;
          iLeft :=  iLeft70;
      end;

      if (StrToInt(strCourseID) >=80) and (StrToInt(strCourseID) < 90) then begin
          iTop80 := iTop80 + 18 ;
          iTop := iTop80;
          iLeft :=  iLeft80;
      end;
      if (StrToInt(strCourseID) >=90) and (StrToInt(strCourseID) < 100) then begin
          iTop90 := iTop90 + 18 ;
          iTop := iTop90;
          iLeft :=  iLeft90;
      end;
    }
      {
      cb := TCheckBox.Create(Self) ;

      with cb do begin //生成時にオーナーを指定
        Parent  :=Self;                  //親ウィンドウを指定
        Left    :=iLeft;                     //表示位置はマウスクリック位置
        Top     :=iTop;
        Name    := 'chkCourse_' + FieldByName('CourseID').AsString;
        Caption := FieldByName('CourseName').AsString;

      end;
      }




      i := StrToInt(FieldByName('CourseID').AsString);
      chkCourse[i] :=TCheckBox.Create(Self);
      chkCourse[i].Parent:=Self;
      chkCourse[i].Left:=iLeft;
      chkCourse[i].Top:=iTop;
      chkCourse[i].Width:=150;
      chkCourse[i].Caption:=FieldByName('CourseName').AsString;

      iTop := iTop + 18 ;

    //	cmbCourse1.Items.Add(FieldByName('CourseName').AsString);
    //	cmbCourse2.Items.Add(FieldByName('CourseName').AsString);

         c := c + 1;
    	Next;


    end;
    Close;

  end;


  Self.Height := 650;

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;


procedure TfrmShukko.chkAllMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin

  if chkAll.Checked = False then
    begin
      // コースをクリアして入力不可にする
      // (本当はチェックされている時，つまり Checked = Trueの時これを行う筈だが，
      // 何故か実行時の動きが逆になる．よってfalseの時以下の処理を行う様にしている．
      // Delphiのバグ？)
      //cmbCourse1.Text    := '';
      //cmbCourse2.Text    := '';
      //cmbCourse1.Enabled := False;
      //cmbCourse2.Enabled := False;
      //cmbCourse1.Color   := clMenu;
      //cmbCourse2.Color   := clMenu;
    end
  else if chkAll.Checked = True then
    begin
      // コースをクリアして入力可能にする
      {
      cmbCourse1.Text    := '';
      cmbCourse2.Text    := '';
      cmbCourse1.Enabled := True;
      cmbCourse2.Enabled := True;
      cmbCourse1.Color   := clWindow;
      cmbCourse2.Color   := clWindow;
      }
    end
  else
    begin
      ShowMessage('エラー発生');
    end;

end;


procedure TfrmShukko.BitBtn2Click(Sender: TObject);
//
// 出庫表を印刷する．
//
var
  sLine            : String;
 	F                : TextFile;
  sNouhinDate      : String;
  sSql             : String;
  sWhere           : String;
  sList            : String;
  GResult          : array of array of String;
  i,iRecordCount   : Integer;
  strCourseID : String;
  strChkName  : String;
  intCourseID : Integer;
  sCourseIn   : String;
  strCourseIn : String;
begin

// 確認メッセージ出力

  if MessageDlg('出力しますか?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then
    begin
      Exit;
    end;


//出力するファイルを作成する、すでにあれば削除する

 	AssignFile(F, CFileName_Shukkohyou);
  Rewrite(F);
	CloseFile(F);

//タイトル部分の出力

  DateTimeToString(sNouhinDate, 'yyyy/mm/dd', dtpNouhinDate.Date);

  sLine := '納品日  ' + sNouhinDate;
  HMakeFile(CFileName_Shukkohyou, sLine);

  sLine := '';
  HMakeFile(CFileName_Shukkohyou, sLine);



//
// SQL作成・実行 〜 抽出データを GResultに格納する．
//

// Where部作成

  // �@ 納品日
  sWhere := 'WHERE ( D.NouhinDate = ' + '''' + sNouhinDate + ''')';

  // �A コース
  if chkAll.Checked = False then begin

	  sSql := 'SELECT CourseID,CourseName FROM ' + CtblMCourse;
    sSql := sSql + ' order by CourseID ';

	  with qryShukko do begin
  	  Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      while not EOF do begin

        intCourseID := StrToInt(FieldByName('CourseID').AsString);

        if (chkCourse[intCourseID].Checked = True) then begin

             sCourseIn := sCourseIn + IntToStr(intCourseID) + ',';
             strCourseIn := strCourseIn + IntToStr(intCourseID) + ' ';


        end;

      Next;
      end;
      Close;
    end;

    sLine := ' コース : ' + strCourseIn;

    sCourseIn := sCourseIn + '0';
    sWhere := sWhere + ' AND ( T.TokuisakiCode2 IN(' + sCourseIn + '))';

 //   begin
     // コース１，コース２共に指定
  //   if (cmbCourse1.Text <> '') and (cmbCourse2.Text <> '') then
   //    begin
    //     sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse1.Text) + '''';
    //     sWhere := sWhere + ' OR T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse2.Text) + ''')';
    //   end
     // コース１指定，コース２が空
    // else if (cmbCourse1.Text <> '') and (cmbCourse2.Text = '') then
    //   begin
    //     sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse1.Text) + ''')';
    //   end
     // コース１空，コース２指定
    // else if (cmbCourse1.Text = '') and (cmbCourse2.Text <> '') then
    //   begin
    //     sWhere := sWhere + ' AND ( T.TokuisakiCode2 = ' + '''' + GetCourseID(cmbCourse2.Text) + ''')';
    //   end
     // コース１空，コース２空　（→ コース＝''つまり0件が抽出される）
   //  else if (cmbCourse1.Text = '') and (cmbCourse2.Text = '') then
   //    begin
       // sWhere := sWhere + ' AND ( T.TokuisakiCode2 = '''' )';
   //    end;
  end else begin
        sLine := 'コース :   全コース分';

  end;

  HMakeFile(CFileName_Shukkohyou, sLine);


  // �B 階数
  //   チェックが1個でもついている場合は，以下の処理に入る．
  //　 チェックが1個もついていない場合は，F1=''つまり0件が抽出される．
  if (chkB1F.Checked = True) or (chk1F.Checked = True)
     or (chk2F.Checked = True) or (chk3F.Checked = True) then begin

     sList := '( ';

    if chkB1F.Checked = True then
      sList := sList +  '''B1'', ';
    if chk1F.Checked = True then
      sList := sList +  '''1F'', ';
    if chk2F.Checked = True then
      sList := sList +  '''2F'', ';
    if chk3F.Checked = True then
      sList := sList +  '''3F'', ';

    sList := sList + ' ''XX'' )';
     // 上のif群でリストを作成しても最後のカンマがどうしても余る.
     // 例えば，1Fと2Fがチェックされている場合，('1F','2F',)となり，
     // 最後のカンマがどうしても余ってしまう．どうしようも無いので，
     // 絶対有りえないXXを最後に加えて，('1F','2F','XX' )と逃げている．
    sWhere := sWhere + ' AND F1 IN ' + sList;

  end else
    begin
      sWhere := sWhere + ' AND ( F1 = '''' )';
    end;

// SQL作成

	sSql := 'SELECT Tanni,Irisuu,I.F1 F1, I.F2 F2, I.Code1 Code1, I.Code2 Code2, ';
  sSql := sSql + 'I.Name Name, SUM(D.Suuryou) AS Suuryou FROM ' + CtblMItem;
  sSql := sSql + ' I INNER JOIN ' + CtblTDenpyouDetail + ' D ON';
  sSql := sSql + ' I.Code1 = D.Code1 AND I.Code2 = D.Code2 ';
  sSql := sSql + ' INNER JOIN ' + CtblMTokuisaki + ' T ON';
  sSql := sSql + ' D.TokuisakiCode1 = T.TokuisakiCode1 ';
  sSql := sSql + sWhere;
  sSql := sSql + ' GROUP BY I.F1, I.F2, I.Code1, I.Code2, I.Name, I.Irisuu,I.Tanni';
  sSql := sSql + ' ORDER BY I.F1, I.F2, I.Code1, I.Code2';

  // データをとりあえず GResultに格納する．

try

  with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;

    // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
    if RecordCount =0 then
      begin
        ShowMessage('本日の出庫対象は存在しません．');
        Close;
        Exit;
      end;

    // 動的配列の次元数を決定
    iRecordCount := RecordCount;         // 次のFor文に向けてレコードカウントを保存

    //2002.08.10
    //SetLength(GResult, RecordCount, 6);
    //SetLength(GResult, RecordCount, 8);
    //2005.06.16
    SetLength(GResult, RecordCount, 9);

    // データをGResultに格納
    i := 0;
    While not EOF do begin
 	    GResult[i,   0] := FieldbyName('F1').AsString;
 	    GResult[i,   1] := FieldbyName('F2').AsString;
 	    GResult[i,   2] := FieldbyName('Code1').AsString;
 	    GResult[i,   3] := FieldbyName('Code2').AsString;
 	    GResult[i,   4] := FieldbyName('Name').AsString;
 	    GResult[i,   5] := FieldbyName('Suuryou').AsString;
      //added by H.K. 2002/08/10
 	    GResult[i,   6] := FieldbyName('Irisuu').AsString;
 	    GResult[i,   7] := FieldbyName('Tanni').AsString;

      //added by H.K. 2005/06/16
      //仕入先コード、仕入先名、メーカー名
 	    GResult[i,   8] := GetShiiresaki(FieldbyName('Code1').AsString,
                                       FieldbyName('Code2').AsString
                                      );


      i := i + 1;
      next
    end;

    Close;

  end;


//
// データをテキストファイルへ書き出す．
//

   For i := 0 to iRecordCount - 1 do begin

    // 階数の出力
    if ( i=0 ) or ( GResult[i-1,0] <> GResult[i,0] ) then
      begin
        sLine := '階数 ： ' + GResult[i,0];
        HMakeFile(CFileName_Shukkohyou, sLine);
      end;

    // 棚の出力
    if ( i=0 ) or ( GResult[i-1,1] <> GResult[i,1] ) then
      begin
        sLine := ',棚数 ： ' + GResult[i,1];
        HMakeFile(CFileName_Shukkohyou, sLine);
      end;

    // Code1, Code2, Name, SUM(Suuryou)の出力
      //sLine := ',,' + GResult[i,2] + ',' + GResult[i,3] + ',';
      sLine := ',' + ',';
      //sLine := sLine + GResult[i,4] + ','+ GResult[i,5] + ',';
      sLine := sLine + GResult[i,4] + ','+ GResult[i,6] + ',';

      //2002.08.10
      sLine := sLine + GResult[i,5] + ',' + GResult[i,7];

      //2005.06.16
      sLine := sLine + ',' + GResult[i,8];

      HMakeFile(CFileName_Shukkohyou, sLine);

   end;

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', '出庫表.xls', '', SW_SHOW);

except
  on E: EDBEngineError do begin
   	ShowMessage(E.Message);
  end;
end;

end;

//商品マスターから仕入先・メーカーを返す
function TfrmShukko.GetShiiresaki(sCode1,sCode2 : String) : String;
var
  sSql,sRet      : String;
begin
  sSql := 'SELECT ShiiresakiCode, Maker FROM ' + CtblMItem;
  sSql := sSql + ' WHERE Code1 = ' + '''' + sCode1 + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + ' Code2 = ' + '''' + sCode2 + '''';
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sRet :=FieldbyName('Maker').AsString;
    sRet :=sRet + ',' + FieldbyName('ShiiresakiCode').AsString;
    Close;
  end;

  Result := sRet;

end;


function TfrmShukko.GetCourseID(CourseName: String) : String;
var
	sSql       : String;
  sCourseID  : String;

begin

  // コースID
	sSql := 'SELECT CourseID FROM ' + CtblMCourse;
  sSql := sSql + ' WHERE CourseName = ' + '''' + CourseName + '''';
	with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    sCourseID :=FieldbyName('CourseID').AsString;
    Close;
  end;

  Result := sCourseID;

end;


procedure TfrmShukko.chkAllClick(Sender: TObject);
var
  intCourseID : Integer;
  sSql : String;
  bFlg1  : Boolean;
  bFlg2  : Boolean;

begin
  inherited;

    // コンボボックスリスト作成

   if chkAll.Checked = True then begin
      bFlg1 := True;
      bFlg2 := False;
   end else begin
      bFlg1 := False;
      bFlg2 := True;
   end;

	sSql := 'SELECT CourseID,CourseName FROM ' + CtblMCourse;
  sSql := sSql + ' order by CourseID ';

	with qryShukko do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin

    intCourseID := StrToInt(FieldByName('CourseID').AsString);
    if chkAll.Checked = True then begin
      chkCourse[intCourseID].Checked := bFlg1;
    end ;
    chkCourse[intCourseID].Enabled := bFlg2;
    Next;
    end;
    Close;
  end;



end;

end.
