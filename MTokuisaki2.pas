unit MTokuisaki2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, StdCtrls, ExtCtrls, ComCtrls, DBTables, ShellAPI,
  Buttons, Db;

type
  TfrmMTokuisaki2 = class(TfrmMaster)
    Panel4: TPanel;
    PageControl1: TPageControl;
    tsShop: TTabSheet;
    tsOffice: TTabSheet;
    tsHome: TTabSheet;
    Label3: TLabel;
    CBTokuisakiCode1: TComboBox;
    Label4: TLabel;
    CBTokuisakiCode2: TComboBox;
    Label5: TLabel;
    CBTokuisakiNameYomi: TComboBox;
    CBTokuisakiName: TComboBox;
    Label6: TLabel;
    Label7: TLabel;
    CBCName: TComboBox;
    CBFName: TComboBox;
    Label8: TLabel;
    BitBtn3: TBitBtn;
    Panel6: TPanel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    CBTelOffice: TComboBox;
    CBAdd3Office: TComboBox;
    CBAdd1Office: TComboBox;
    CBZipOffice: TComboBox;
    Label32: TLabel;
    CBAdd2Office: TComboBox;
    Label33: TLabel;
    CBFaxOffice: TComboBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    CBTelHome: TComboBox;
    CBAdd3Home: TComboBox;
    CBAdd1Home: TComboBox;
    CBZipHome: TComboBox;
    Label38: TLabel;
    CBAdd2Home: TComboBox;
    Label39: TLabel;
    CBFaxHome: TComboBox;
    Label42: TLabel;
    EditAtenaOffice: TEdit;
    Label43: TLabel;
    EditAtenaHome: TEdit;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    Label44: TLabel;
    CBGCode: TComboBox;
    Label45: TLabel;
    CBChainCode: TComboBox;
    Label46: TLabel;
    CBAtenaHome: TComboBox;
    Query1: TQuery;
    Label47: TLabel;
    edTokuisakiNameUp: TEdit;
    Label50: TLabel;
    TabSheet2: TTabSheet;
    Label53: TLabel;
    Yobi_c_2: TComboBox;
    Label54: TLabel;
    Yobi_c_4: TComboBox;
    Label55: TLabel;
    Yobi_c_3: TComboBox;
    TabSheet3: TTabSheet;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Yobi_i_2: TEdit;
    Yobi_i_3: TEdit;
    Yobi_i_4: TEdit;
    Label60: TLabel;
    Yobi_i_5: TEdit;
    Label61: TLabel;
    Yobi_c_5: TComboBox;
    CBZip: TComboBox;
    Label16: TLabel;
    CBAdd1: TComboBox;
    Label17: TLabel;
    Label18: TLabel;
    CBAdd2: TComboBox;
    CBAdd3: TComboBox;
    Label19: TLabel;
    Label13: TLabel;
    CBTel: TComboBox;
    Label27: TLabel;
    CBFax: TComboBox;
    Label20: TLabel;
    Label14: TLabel;
    CBMemo: TMemo;
    RMemo: TMemo;
    TextBikou: TMemo;
    Label40: TLabel;
    Label15: TLabel;
    CBZanOnOff: TComboBox;
    Label23: TLabel;
    EditKa: TEdit;
    Label25: TLabel;
    EditChainCode: TEdit;
    Label41: TLabel;
    EditShuukeiCode: TEdit;
    Label26: TLabel;
    CBSeikyuushoFlag: TComboBox;
    Label52: TLabel;
    Yobi_c_1: TComboBox;
    Label24: TLabel;
    EditTantoushaCode: TEdit;
    Label51: TLabel;
    Tourokubi: TDateTimePicker;
    RB420: TRadioButton;
    RB450: TRadioButton;
    Label63: TLabel;
    Label62: TLabel;
    KeitaiDenwa: TEdit;
    KyuugyouCheck: TCheckBox;
    KyuugyouDate: TDateTimePicker;
    cbHeitenFlag: TCheckBox;
    HeitenDate: TDateTimePicker;
    GB1: TGroupBox;
    rbSShop: TRadioButton;
    rbSOffice: TRadioButton;
    rbSHome: TRadioButton;
    rbSJisan: TRadioButton;
    Label21: TLabel;
    Label22: TLabel;
    GB2: TGroupBox;
    rbNShop: TRadioButton;
    rbNOffice: TRadioButton;
    rbNHome: TRadioButton;
    rbNNo: TRadioButton;
    Label64: TLabel;
    Label10: TLabel;
    CBSeikyuuSimebi: TComboBox;
    Label11: TLabel;
    CBShiharaibiM: TComboBox;
    Label12: TLabel;
    CBShiharaibiD: TComboBox;
    Label9: TLabel;
    CBShiharaiKubun: TComboBox;
    Label65: TLabel;
    Label66: TLabel;
    Label56: TLabel;
    Yobi_i_1: TEdit;
    ShuukinCode: TEdit;
    UriageCode: TEdit;
    Label48: TLabel;
    Label49: TLabel;
    Label67: TLabel;
    Button1: TButton;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    InfomartFlag: TCheckBox;
    FoodParkFlg: TCheckBox;
    procedure BitBtn3Click(Sender: TObject);
    procedure CBTokuisakiCode1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure EditKaExit(Sender: TObject);
    procedure EditTantoushaCodeExit(Sender: TObject);
    procedure EditShuukeiCodeExit(Sender: TObject);
    procedure CBSeikyuushoFlagEnter(Sender: TObject);
    procedure CBSeikyuushoFlagDropDown(Sender: TObject);
    procedure CBSeikyuushoFlagExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure CBCNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure CBChainCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBAtenaHomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBChainCodeChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
		procedure MakeForm(sTokuisakiCode1:String);
		procedure UpdateTokuisaki(sTokuisakiCode1:String);
		procedure InsertTokuisaki(sTokuisakiCode1:String);
    procedure MakeTokuisakibetuShouhinMaster(sTokuisakiCode1, sF420450 : String);
    procedure FromClear;
  public
    { Public 宣言 }
  end;

var
  frmMTokuisaki2: TfrmMTokuisaki2;

implementation
uses
	MTokuisaki, DMMaster, Inter, HKLib, DM1, PasswordDlg;

var
  //請求書を出す出さないの文字列
  GCBSeikyuushoFlag : String;
  //業種コード
	sGCode : String;

  {$R *.DFM}

procedure TfrmMTokuisaki2.BitBtn3Click(Sender: TObject);
var
	sTokuisakiName, sTokuisakiCode1 : String;
  f420450 : string;
  iTemp : integer;
begin
//2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
  //コメントアウト 2009.04.08 utada
	//end else if GPassWord = CPassword2 then begin
    //Beep;
	//end else if GPassWord = CPassword3 then begin
    //Beep;
  //ここまで
	end else begin
	//ShowMessage('PassWordが違います');
   exit;
	end;
	GPassWord := '';


	//データチェック
  if CBTokuisakiCode1.Text = '' then begin
  	ShowMessage('得意先コードは省略できません');
    CBTokuisakiCode1.SetFocus;
    Exit;
  end;
  if CBTokuisakiName.Text = '' then begin
  	ShowMessage('得意先名は省略できません');
    CBTokuisakiName.SetFocus;
    Exit;
  end;

  if EditKa.Text = '' then begin
  	ShowMessage('課は省略できません');
    EditKa.SetFocus;
    Exit;
  end;

  if EditTantoushaCode.Text = '' then begin
  	ShowMessage('郵送・持参コードは省略できません');
    EditTantoushaCode.SetFocus;
    Exit;
  end;

  if EditShuukeiCode.Text = '' then begin
  	ShowMessage('コードは省略できません');
    EditShuukeiCode.SetFocus;
    Exit;
  end;

  if EditChainCode.Text = '' then begin
  	ShowMessage('チェーンコードは省略できません');
    EditChainCode.SetFocus;
    Exit;
  end;

  //エラーチェックの強化　2004.07.07
  //業種
  if CBGCode.Text = '' then begin
  	ShowMessage('業種は省略できません');
    CBGCode.SetFocus;
    Exit;
  end;

  //集金コード
  if ShuukinCode.Text = '' then begin
  	ShowMessage('集金コードは省略できません');
    ShuukinCode.SetFocus;
    Exit;
  end;

  //売り上げコード
  if UriageCode.Text = '' then begin
  	ShowMessage('売上コードは省略できません');
    UriageCode.SetFocus;
    Exit;
  end;

  //請求書発行先
	//請求書発送区分
	//SeikyuushoHassouSaki (smallint) <--Add
	//0=持参、1=会社、2=店、3=自宅
  iTemp := 9;
  if rbSShop.Checked = True then iTemp := 2
  else if rbSOffice.Checked = True then iTemp := 1
  else if rbSHome.Checked = True then iTemp := 3
  else if rbSJisan.Checked = True then iTemp := 0;
  if iTemp = 9 then begin
  	ShowMessage('請求書発行先は省略できません');
    UriageCode.SetFocus;
    Exit;
  end;

  //年賀状発行先
	//年賀状発送区分
	//NengajouHassouSaki (smallint) <--Add
	//0=出さない、1=会社、2=店、3=自宅
  iTemp := 9;
  if rbNShop.Checked = True then iTemp := 2
  else if rbNOffice.Checked = True then iTemp := 1
  else if rbNHome.Checked = True then iTemp := 3
  else if rbNNo.Checked = True then iTemp := 0;
  if iTemp = 9 then begin
  	ShowMessage('年賀状発行先は省略できません');
    UriageCode.SetFocus;
    Exit;
  end;

  //課
  if EditKa.Text = '' then begin
  	ShowMessage('課は省略できません');
    EditKa.SetFocus;
    Exit;
  end;

  //郵送持参
  if EditTantoushaCode.Text = '' then begin
  	ShowMessage('郵送持参は省略できません');
    EditTantoushaCode.SetFocus;
    Exit;
  end;

  //チェーンコード
  if EditChainCode.Text = '' then begin
  	ShowMessage('チェーンコードは省略できません');
    EditChainCode.SetFocus;
    Exit;
  end;


	//業種コードの取得
  //2001/06/20
  //2004.05.18

  if Length(CBGCode.Text) = 2 then begin
    sGCode := CBGCode.Text;
  end else begin
    sGCode := Copy(CBGCode.Text, 1, Pos(',',CBGCode.Text)-1);
  end;

	//２重登録の確認
  sTokuisakiCode1 := CBTokuisakiCode1.Text;
  sTokuisakiCode1 := MTokuisaki.CheckTokuisakiCode(sTokuisakiCode1);
  //sTokuisakiName := GetTokuisakiName(sTokuisakiCode1);
  try
	  if sTokuisakiCode1 = '' then begin
			//確認メッセージ
      if RB420.Checked = true then begin
        f420450 := '420';
      end else if RB450.Checked = true then begin
        f420450 := '450';
      end else begin
      	ShowMessage('420,450は省略できません');
        RB420.SetFocus;
        Exit;
      end;

			if MessageDlg('新規データです。登録しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		InsertTokuisaki(CBTokuisakiCode1.Text);
			if MessageDlg('引き続き得意先別商品マスタを登録します',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;

      MakeTokuisakibetuShouhinMaster(CBTokuisakiCode1.Text, f420450);
	  end else begin
			//確認メッセージ
			if MessageDlg('既存データです。修正しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		UpdateTokuisaki(sTokuisakiCode1);
	  end;
  	ShowMessage('登録に成功しました');
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
		  ShowMessage('登録に失敗しました');
    end;
  end;
end;

procedure TfrmMTokuisaki2.MakeTokuisakibetuShouhinMaster(sTokuisakiCode1, sF420450 : String);
var
  sSql,sKakakuKubun : String;
  j,i : Integer;
	myA : TInRecArry;
begin
  //sSql := 'select * from ' + CtblMItem;
  sSql := 'select Code1,Code2,Tanka420,Tanka450 from ' + CtblMItem;
  //2006.06.05
  //sSql := sSql + ' where Code1<>''0'' ';
  sSql := sSql + ' order by ';
  sSql := sSql + ' Code1, Code2';
  with Query1 do begin
  	close;
    sql.Clear;
    sql.Add(sSql);
    open;
    j := 1;
    while not EOF do begin
        SB1.SimpleText := IntToStr(j) + ' ** Code1->' + FieldByName('Code1').asString +
                          ' Code2->'  +FieldByName('Code2').asString;
        SB1.Update;
  	//  InRecArry : TInRecArry;//[フィールド名,フィールド内容, 区切り]
  	i := 1;
  	myA[i][1] := 'Code1';
  	myA[i][2] := FieldByName('Code1').asString;
  	myA[i][3] := '''';

    i := i + 1;
  	myA[i][1] := 'Code2';
  	myA[i][2] := FieldByName('Code2').asString;
  	myA[i][3] := '''';

    i := i + 1;
  	myA[i][1] := 'Tanka';
    if sF420450='420' then begin
  	   myA[i][2] := FieldByName('Tanka420').asString;
       sKakakuKubun := '420';
    end else if sF420450='450' then begin
  	   myA[i][2] := FieldByName('Tanka450').asString;
       sKakakuKubun := '450';
  	end;
    if myA[i][2] = '' then begin
      myA[i][2] := '0';
    end;
    myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'TokuisakiCode';
  	myA[i][2] := CBTokuisakiCode1.Text;
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'KakakuKubun';
  	myA[i][2] := sKakakuKubun;
  	myA[i][3] := '';

    //Adeed by H.K. 2002/03/01
    i := i + 1;
  	myA[i][1] := 'YN';
  	myA[i][2] := '0';
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'CYN';
  	myA[i][2] := '0';
  	myA[i][3] := '';

    i := i + 1;
  	myA[i][1] := 'END';
  	myA[i][2] := 'END';
  	myA[i][3] := 'END';

	  DM1.InsertRecord(myA, CtblMItem2);
    j := j + 1;
    next;
    end;//of while
  end;//of with
end;








procedure TfrmMTokuisaki2.CBTokuisakiCode1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('TokuisakiCode1', CBTokuisakiCode1.Text, 2);
  end;
end;

{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmMTokuisaki2.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' + 
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;

procedure TfrmMTokuisaki2.FormCreate(Sender: TObject);
begin
	width  := 777;
  height := 711;

	DM1.MakeCBAll(CBGCode, CtblMList, 'Code', 'Name');

  //2004.06.21
  //Tourokubi.DateTime := Now();
  //KyuugyouDate.DateTime := Now();
  //HeitenDate.DateTime := Now();

  //2004.07.07
  FromClear;
end;

procedure TfrmMTokuisaki2.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
  	MakeCBTokuisakiName('TokuisakiName', CBTokuisakiName.Text, 1);
  end;
end;

procedure TfrmMTokuisaki2.CBTokuisakiNameYomiKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBTokuisakiName.SetFocus;
	 	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiNameYomi.Text, 1);
  end;
end;

procedure TfrmMTokuisaki2.CBTokuisakiNameExit(Sender: TObject);
var
	sTokuisakiCode1 : String;
begin
	//得意先コード１を取得して得意先を検索する
  sTokuisakiCode1 := Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));
  if sTokuisakiCode1 <> '' then
	  MakeForm(sTokuisakiCode1);
  //frmMTokuisaki.Update;
end;

//得意先コードから得意先マスターフォームを表示する
procedure TfrmMTokuisaki2.MakeForm(sTokuisakiCode1:String);
var
	sDate, sSql, sKubun : String;
  sInfomartFlag :String; // add 20140821 utada
  sFoodParkFlg :String; // add 20150408 thuy
  iSeikyuushoHassouSaki, i : Integer;
begin
	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' TokuisakiCode1 =  ' + sTokuisakiCode1;
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
			CBTokuisakiCode1.Text := sTokuisakiCode1;
			CBTokuisakiCode2.Text := FieldbyName('TokuisakiCode2').AsString;
			CBTokuisakiNameYomi.Text := FieldbyName('TokuisakiNameYomi').AsString;
			CBTokuisakiName.Text := FieldbyName('TokuisakiName').AsString;
			CBCName.Text := FieldbyName('CName').AsString;
			CBFName.Text := FieldbyName('FName').AsString;
			CBShiharaiKubun.Text := FieldbyName('ShiharaiKubun').AsString;
			CBSeikyuuSimebi.Text := FieldbyName('SeikyuuSimebi').AsString;
			CBShiharaibiM.Text := FieldbyName('ShiharaibiM').AsString;
			CBShiharaibiD.Text := FieldbyName('ShiharaibiD').AsString;
			CBTel.Text := FieldbyName('Tel').AsString;
			CBZanOnOff.Text := FieldbyName('ZanOnOff').AsString;
			CBMemo.Text := FieldbyName('Memo').AsString;
			RMemo.Text := FieldbyName('EstimateMemo').AsString;

      //1999/05/17追加
      CBZip.Text := FieldbyName('Zip').AsString;
      CBAdd1.Text := FieldbyName('Add1').AsString;
      CBAdd2.Text := FieldbyName('Add2').AsString;
      CBAdd3.Text := FieldbyName('Add3').AsString;

      //2000/01/14 追加
		  //店ＦＡＸ
      CBFax.Text := FieldbyName('Fax').AsString;

		  //事務所住所
      CBZipOffice.Text := FieldbyName('ZipOffice').AsString;
      CBAdd1Office.Text := FieldbyName('Add1Office').AsString;
      CBAdd2Office.Text := FieldbyName('Add2Office').AsString;
      CBAdd3Office.Text := FieldbyName('Add3Office').AsString;
			CBTelOffice.Text := FieldbyName('TelOffice').AsString;
      CBFaxOffice.Text := FieldbyName('FaxOffice').AsString;
      EditAtenaOffice.Text := FieldbyName('AtenaOffice').AsString;

		  //自宅住所
      CBZipHome.Text := FieldbyName('ZipHome').AsString;
      CBAdd1Home.Text := FieldbyName('Add1Home').AsString;
      CBAdd2Home.Text := FieldbyName('Add2Home').AsString;
      CBAdd3Home.Text := FieldbyName('Add3Home').AsString;
			CBTelHome.Text := FieldbyName('TelHome').AsString;
      CBFaxHome.Text := FieldbyName('FaxHome').AsString;
      EditAtenaHome.Text := FieldbyName('AtenaHome').AsString;
      //
      CBAtenaHome.Text := FieldbyName('AtenaHome').AsString;

			//請求書発行フラグ
			//SeikyuushoFlag	(smallint) <--Add
			//0=発行する, 1=発行しない
  		sSql := sSql + 'SeikyuushoFlag  = '   + Copy(CBSeikyuushoFlag.Text, 1, 1);
      if  FieldbyName('SeikyuushoFlag').AsInteger = 0 then
	      CBSeikyuushoFlag.Text := '0 出す'
      else if FieldbyName('SeikyuushoFlag').AsInteger = 1 then
	      CBSeikyuushoFlag.Text := '1 出さない';

			//請求書発送区分
			//SeikyuushoHassouSaki (smallint) <--Add
			//0=持参、1=会社、2=店、3=自宅
      iSeikyuushoHassouSaki := FieldbyName('SeikyuushoHassouSaki').AsInteger;
      if FieldbyName('SeikyuushoHassouSaki').AsInteger = 0 then
				rbSJisan.Checked := True
      else if FieldbyName('SeikyuushoHassouSaki').AsInteger = 1 then
				rbSOffice.Checked := True
      else if FieldbyName('SeikyuushoHassouSaki').AsInteger = 2 then
				rbSShop.Checked := True
      else if FieldbyName('SeikyuushoHassouSaki').AsInteger = 3 then
				rbSHome.Checked := True
      else begin
				rbSHome.Checked   := False;
				rbSShop.Checked   := False;
				rbSOffice.Checked := False;
				rbSJisan.Checked  := False;
      end;

			//年賀状発送区分
			//NengajouHassouSaki (smallint) <--Add
			//0=持参、1=会社、2=店、3=自宅
      i := FieldbyName('NengajouHassouSaki').AsInteger;
      if FieldbyName('NengajouHassouSaki').AsInteger = 0 then
				rbNNo.Checked := True
      else if FieldbyName('NengajouHassouSaki').AsInteger = 1 then
				rbNOffice.Checked := True
      else if FieldbyName('NengajouHassouSaki').AsInteger = 2 then
				rbNShop.Checked := True
      else if FieldbyName('NengajouHassouSaki').AsInteger = 3 then
				rbNHome.Checked := True
      else begin
 				rbNNo.Checked := False;
 				rbNOffice.Checked := False;
 				rbNShop.Checked := False;
 				rbNHome.Checked := False;
        Showmessage('年賀状発送先が不正です('+ IntToStr(i) + ')');
      end;

			//課
			//Ka		(smallint)
      EditKa.Text := FieldbyName('Ka').AsString;

			//担当者コード
			//TantoushaCode	(smallint)
      EditTantoushaCode.Text := FieldbyName('TantoushaCode').AsString;

			//チェーンコード
			//ChainCode		(smallint)
      EditChainCode.Text := FieldbyName('ChainCode').AsString;

			//備考
			//Bikou		(Text)
      textBikou.Text := FieldbyName('Bikou').AsString;

			//集計コード
			//ShuukeiCode	(smallint)
      EditShuukeiCode.Text := FieldbyName('ShuukeiCode').AsString;

			//閉店フラグ
			//HeitenFlag	(smallint)
			//	1=閉店
      if FieldbyName('HeitenFlag').AsInteger = 1 then
				cbHeitenFlag.Checked := True
      else
				cbHeitenFlag.Checked := False;

      //業種コード
      CBGCode.Text  := FieldbyName('GCode').AsString;

      //2001/11/26 追加
      //商品単価区分420,450
      sKubun := FieldbyName('Kubun420450').AsString;
      if sKubun = '420' then begin
        RB420.Checked := True;
      end else if sKubun = '450' then begin
        RB450.Checked := True;
      end else begin
        RB420.Checked := False;
        RB450.Checked := False;
      end;

      //業種コード
      edTokuisakiNameUp.Text  := FieldbyName('TokuisakiNameUp').AsString;

      //2004.04.15 追加 h, Kubota
      //
      ShuukinCode.Text  := FieldbyName('ShuukinCode').AsString;
      UriageCode.Text   := FieldbyName('UriageCode').AsString;

      sDate := FieldbyName('Tourokubi').AsString;
      if Length(sDate) > 0 then begin
       Tourokubi.Date    := StrToDate(sDate);
      end else begin
       Tourokubi.Date    := StrToDate('1990/01/01');
      end;

      sDate := FieldbyName('KyuugyouDate').AsString;
      if Length(sDate) > 0 then begin
       KyuugyouDate.Date    := StrToDate(sDate);
      end else begin
       KyuugyouDate.Date    := StrToDate('1990/01/01');
      end;

      sDate := FieldbyName('HeitenDate').AsString;
      if Length(sDate) > 0 then begin
       HeitenDate.Date    := StrToDate(sDate);
      end else begin
       HeitenDate.Date    := StrToDate('1990/01/01');
      end;

      sKubun := FieldbyName('KyuugyouCheck').AsString;
      if sKubun = '0' then begin
        KyuugyouCheck.Checked := True;
      end else if sKubun = '1' then begin
        KyuugyouCheck.Checked := False;
      end else begin
        KyuugyouCheck.Checked := False;
      end;

      // add start 20140821 utada
      sInfomartFlag := FieldbyName('InfomartFlag').AsString;
      if sInfomartFlag = '0' then begin
        InfomartFlag.Checked := False;
      end else if sInfomartFlag = '1' then begin
        InfomartFlag.Checked := True;
      end else begin
        InfomartFlag.Checked := False;
      end;
      // add end 20140821 utada
	  
	  // add start 20150408 thuy
      sFoodParkFlg := FieldbyName('FoodParkFlg').AsString;
      if sFoodParkFlg = '0' then begin
        FoodParkFlg.Checked := False;
      end else if sFoodParkFlg = '1' then begin
        FoodParkFlg.Checked := True;
      end else begin
        FoodParkFlg.Checked := False;
      end;
      // add end 20140821 20150408 thuy



      Yobi_i_1.Text   := FieldbyName('Yobi_i_1').AsString;
      Yobi_i_2.Text   := FieldbyName('Yobi_i_2').AsString;
      Yobi_i_3.Text   := FieldbyName('Yobi_i_3').AsString;
      Yobi_i_4.Text   := FieldbyName('Yobi_i_4').AsString;
      Yobi_i_5.Text   := FieldbyName('Yobi_i_5').AsString;

      Yobi_c_1.Text   := FieldbyName('Yobi_c_1').AsString;
      Yobi_c_2.Text   := FieldbyName('Yobi_c_2').AsString;
      Yobi_c_3.Text   := FieldbyName('Yobi_c_3').AsString;
      Yobi_c_4.Text   := FieldbyName('Yobi_c_4').AsString;
      Yobi_c_5.Text   := FieldbyName('Yobi_c_5').AsString;

      KeitaiDenwa.Text := FieldbyName('KeitaiDenwa').AsString;
    end;
    Close;
  end;
end;

//得意先マスターの更新
procedure TfrmMTokuisaki2.UpdateTokuisaki(sTokuisakiCode1:String);
var
	sSql,sKubun,sTemp : String;
  sInfomartFlag :String; // add 20140821 utada
  sFoodParkFlg :String; // add 20150408 thuy
  iTemp : Integer;
begin
	sSql := 'UPDATE ' + CtblMTokuisaki;
  sSql := sSql + ' SET ';
  sSql := sSql + 'TokuisakiCode2    = '   + CBTokuisakiCode2.Text    + ',';
  sSql := sSql + 'TokuisakiName     = ''' + CBTokuisakiName.Text     + ''',';
  sSql := sSql + 'TokuisakiNameYomi = ''' + CBTokuisakiNameYomi.Text + ''',';
  sSql := sSql + 'CName             = ''' + CBCName.Text             + ''',';
  sSql := sSql + 'FName             = ''' + CBFName.Text             + ''',';
  if Pos(' ', CBShiharaiKubun.Text) > 0 then begin
    sSql := sSql + 'ShiharaiKubun = ' + Copy(CBShiharaiKubun.Text, 1, Pos(' ', CBShiharaiKubun.Text)-1) + ',';
	end else begin
    sSql := sSql + 'ShiharaiKubun     = '   + CBShiharaiKubun.Text     + ',';
  end;

  if Pos(' ', CBSeikyuuSimebi.Text) > 0 then begin
    sSql := sSql + 'SeikyuuSimebi = ' + Copy(CBSeikyuuSimebi.Text, 1, Pos(' ', CBSeikyuuSimebi.Text)-1) + ',';
	end else begin
    sSql := sSql + 'SeikyuuSimebi     = '   + CBSeikyuuSimebi.Text     + ',';
  end;

  if Pos(' ', CBShiharaibiM.Text) > 0 then begin
    sSql := sSql + 'ShiharaibiM = ' + Copy(CBShiharaibiM.Text, 1, Pos(' ', CBShiharaibiM.Text)-1) + ',';
	end else begin
    sSql := sSql + 'ShiharaibiM     = '   + CBShiharaibiM.Text     + ',';
  end;

  if Pos(' ', CBShiharaibiD.Text) > 0 then begin
    sSql := sSql + 'ShiharaibiD = ' + Copy(CBShiharaibiD.Text, 1, Pos(' ', CBShiharaibiD.Text)-1) + ',';
	end else begin
    sSql := sSql + 'ShiharaibiD     = '   + CBShiharaibiD.Text     + ',';
  end;

  sSql := sSql + 'Tel               = ''' + CBTel.Text               + ''',';
  sSql := sSql + 'Memo              = ''' + CBMemo.Text              + ''',';

  sSql := sSql + 'EstimateMemo      = ''' + RMemo.Text              + ''',';

  sSql := sSql + 'Zip               = ''' + CBZip.Text              + ''',';
  sSql := sSql + 'Add1              = ''' + CBAdd1.Text              + ''',';
  sSql := sSql + 'Add2              = ''' + CBAdd2.Text              + ''',';
  sSql := sSql + 'Add3              = ''' + CBAdd3.Text              + ''',';

  if CBZanOnOff.Text = '' then CBZanOnOff.Text := '1';//標準で載せる
  sSql := sSql + 'ZanOnOff          = '   + Copy(CBZanOnOff.Text, 1, 1) + ',';

  //2000/01/14 追加
  //店ＦＡＸ
  sSql := sSql + 'Fax               = ''' + CBFax.Text               + ''',';

  //事務所住所
  sSql := sSql + 'ZipOffice         = ''' + CBZipOffice.Text         + ''',';
  sSql := sSql + 'Add1Office        = ''' + CBAdd1Office.Text        + ''',';
  sSql := sSql + 'Add2Office        = ''' + CBAdd2Office.Text        + ''',';
  sSql := sSql + 'Add3Office        = ''' + CBAdd3Office.Text        + ''',';
  sSql := sSql + 'TelOffice         = ''' + CBTelOffice.Text         + ''',';
  sSql := sSql + 'FaxOffice         = ''' + CBFaxOffice.Text         + ''',';
  sSql := sSql + 'AtenaOffice       = ''' + EditAtenaOffice.Text     + ''',';

  //自宅住所
  sSql := sSql + 'ZipHome         = ''' + CBZipHome.Text         + ''',';
  sSql := sSql + 'Add1Home        = ''' + CBAdd1Home.Text        + ''',';
  sSql := sSql + 'Add2Home        = ''' + CBAdd2Home.Text        + ''',';
  sSql := sSql + 'Add3Home        = ''' + CBAdd3Home.Text        + ''',';
  sSql := sSql + 'TelHome         = ''' + CBTelHome.Text         + ''',';
  sSql := sSql + 'FaxHome         = ''' + CBFaxHome.Text         + ''',';
  sSql := sSql + 'AtenaHome       = ''' + EditAtenaHome.Text     + ''',';

  //その他
	//請求書発行フラグ
	//SeikyuushoFlag	(smallint) <--Add
	//0=発行する, 1=発行しない
  sSql := sSql + 'SeikyuushoFlag  = '   + Copy(CBSeikyuushoFlag.Text, 1, 1) + ',';

	//請求書発送区分
	//SeikyuushoHassouSaki (smallint) <--Add
	//0=持参、1=会社、2=店、3=自宅
  if rbSShop.Checked = True then iTemp := 2
  else if rbSOffice.Checked = True then iTemp := 1
  else if rbSHome.Checked = True then iTemp := 3
  else if rbSJisan.Checked = True then iTemp := 0;
  sSql := sSql + 'SeikyuushoHassouSaki = '   + IntToStr(iTemp) + ',';

	//年賀状発送区分
	//NengajouHassouSaki (smallint) <--Add
	//0=出さない、1=会社、2=店、3=自宅
  if rbNShop.Checked = True then iTemp := 2
  else if rbNOffice.Checked = True then iTemp := 1
  else if rbNHome.Checked = True then iTemp := 3
  else if rbNNo.Checked = True then iTemp := 0;

  sSql := sSql + 'NengajouHassouSaki  = '   + IntToStr(iTemp) + ',';

	//課
	//Ka		(smallint)
  sSql := sSql + 'Ka  = '   + EditKa.Text + ',';

	//担当者コード
	//TantoushaCode	(smallint)
  sSql := sSql + 'TantoushaCode  = '   + EditTantoushaCode.Text + ',';

	//チェーンコード
	//ChainCode		(smallint)
  sSql := sSql + 'ChainCode  = '   + EditChainCode.Text + ',';

	//備考
	//Bikou		(Text)
  sSql := sSql + 'Bikou      = ''' + TextBikou.Text   + ''',';

	//集計コード
	//ShuukeiCode	(smallint)
  sSql := sSql + 'ShuukeiCode  = '   + EditShuukeiCode.Text + ',';

	//閉店フラグ
	//HeitenFlag	(smallint)
	//	1=閉店
  if cbHeitenFlag.Checked = True then iTemp := 1
  else iTemp := 0;
  sSql := sSql + 'HeitenFlag  = '   + IntToStr(iTemp);

  //業種コード
  sSql := sSql + ',GCode = ''' + sGCode + '''';

  //商品区分コード420,450
  if RB420.Checked = True then begin
     sKubun := '420';
  end else if RB450.Checked = True then begin
     sKubun := '450';
  end else begin
     sKubun := '0';
  end;
  sSql := sSql + ',Kubun420450 = ' + sKubun;

  //2002.08.10
  //
  sSql := sSql + ',TokuisakiNameUp = ''' + trim(edTokuisakiNameUp.Text) + '''';

  //2004.04.15
  //集金コード
  sTemp := trim(ShuukinCode.Text);
  if length(sTemp) > 0 then begin
    sSql := sSql + ',ShuukinCode = ' + sTemp;
  end;

  //売り上げコード
  sTemp := trim(UriageCode.Text);
  if length(sTemp) > 0 then begin
    sSql := sSql + ',UriageCode = ' + sTemp;
  end;//if

  //登録日
  sSql := sSql + ',Tourokubi = ''' + DatetoStr(Tourokubi.date) + '''';

  //休業フラグ
  if KyuugyouCheck.Checked = True then begin
     sKubun := '0';
  end else if KyuugyouCheck.Checked = False then begin
     sKubun := '1';
  end;
  sSql := sSql + ',KyuugyouCheck = ' + sKubun;

  // add start 20140821 utada
  //infoマート対象フラグ
  if InfomartFlag.Checked = True then begin
     sInfomartFlag := '1';
  end else if KyuugyouCheck.Checked = False then begin
     sInfomartFlag := '0';
  end;
  sSql := sSql + ',InfomartFlag = ' + sInfomartFlag;
  // add end 20140821 utada

  // add start 20140408 thuy
  //infoマート対象フラグ
  if FoodParkFlg.Checked = True then begin
     sFoodParkFlg := '1';
  end else if FoodParkFlg.Checked = False then begin
     sFoodParkFlg := '0';
  end;
  sSql := sSql + ',FoodParkFlg = ' + sFoodParkFlg;
  // add end 20140408 thuy
  

  //休業日
  sSql := sSql + ',KyuugyouDate = ''' + DatetoStr(KyuugyouDate.date) + '''';

  //閉店
  sSql := sSql + ',HeitenDate = ''' + DatetoStr(HeitenDate.date) + '''';

  //携帯電話
  sSql := sSql + ',KeitaiDenwa = ''' + Trim(KeitaiDenwa.text) + '''';

  //文字型　予備1,予備2,予備3,予備4,予備5
  sSql := sSql + ',Yobi_i_1 = ''' + Trim(Yobi_i_1.text) + '''';
  sSql := sSql + ',Yobi_i_2 = ''' + Trim(Yobi_i_2.text) + '''';
  sSql := sSql + ',Yobi_i_3 = ''' + Trim(Yobi_i_3.text) + '''';
  sSql := sSql + ',Yobi_i_4 = ''' + Trim(Yobi_i_4.text) + '''';
  sSql := sSql + ',Yobi_i_5 = ''' + Trim(Yobi_i_5.text) + '''';

  //数値型　予備1,予備2,予備3,予備4,予備5
  sSql := sSql + ',Yobi_c_1 = ''' + Trim(Yobi_c_1.text) + '''';
  sSql := sSql + ',Yobi_c_2 = ''' + Trim(Yobi_c_2.text) + '''';
  sSql := sSql + ',Yobi_c_3 = ''' + Trim(Yobi_c_3.text) + '''';
  sSql := sSql + ',Yobi_c_4 = ''' + Trim(Yobi_c_4.text) + '''';
  sSql := sSql + ',Yobi_c_5 = ''' + Trim(Yobi_c_5.text) + '''';



	sSql := sSql + ' WHERE TokuisakiCode1 = ' + sTokuisakiCode1;

  with frmDMMaster.QueryUpdate do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with
end;

procedure TfrmMTokuisaki2.EditKaExit(Sender: TObject);
begin
	try
  	StrToInt(EditKa.Text);
  except
  	ShowMessage('課は半角数字でお願いします');
    Exit;
  end;
end;

procedure TfrmMTokuisaki2.EditTantoushaCodeExit(Sender: TObject);
begin
	try
  	StrToInt(EditTantoushaCode.Text);
  except
  	ShowMessage('担当者コードは半角数字でお願いします');
    Exit;
  end;

end;

procedure TfrmMTokuisaki2.EditShuukeiCodeExit(Sender: TObject);
begin
  try
    StrToInt(EditShuukeiCode.Text);
  except
    ShowMessage('集計コードは半角数字でお願いします');
    Exit;
  end;
end;

procedure TfrmMTokuisaki2.CBSeikyuushoFlagEnter(Sender: TObject);
begin
	GCBSeikyuushoFlag := CBSeikyuushoFlag.Text;
end;

procedure TfrmMTokuisaki2.CBSeikyuushoFlagDropDown(Sender: TObject);
begin
	GCBSeikyuushoFlag := CBSeikyuushoFlag.Text;
end;

procedure TfrmMTokuisaki2.CBSeikyuushoFlagExit(Sender: TObject);
var
	sPass : String;
begin
{
	//請求書出す出さないはパスワード処理
  if GCBSeikyuushoFlag <> CBSeikyuushoFlag.Text then begin
	  sPass:= InputBox('Input Box', 'パスワードを入力してください', '');
    if sPass = 'takako' then begin
	    ShowMessage('請求書発行区分を変更しました。再度確認してください');
    end else begin
	    ShowMessage('パスワードが違います');
    	CBSeikyuushoFlag.Text := GCBSeikyuushoFlag;
    end;
  end;
}

  //2006.12.25
  dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
  if dlgPasswordDlg.ShowModal = mrOK then begin

  end else begin
    Exit;
  end;

  if GPassWord = 'takako' then begin
    ShowMessage('請求書発行区分を変更しました。再度確認してください');
  end else begin
    ShowMessage('パスワードが違います');
    CBSeikyuushoFlag.Text := GCBSeikyuushoFlag;
  end;
  GPassWord := '';
  exit;

end;


//得意先マスターの新規登録
//2000/01/23
procedure TfrmMTokuisaki2.InsertTokuisaki(sTokuisakiCode1:String);
var
	sSql, sKubun, sTemp,sTourokubi : String;
  sInfomartFlag : String; // add 20140821 utada
  sFoodParkFlg : String; // add 20150408 thuy
  iTemp : Integer;
begin
  //2004.07.13
  //新規の場合のみのエラーチェック
  //登録日は必須
{
  sTourokubi := DatetoStr(Tourokubi.date);
  if sTourokubi = '1900/01/01' then begin
    Showmessage('登録日が不正です。商品登録を中止して再度入力してください。');
    exit;
  end;
}

  //基本データの作成
  if CBZanOnOff.Text = '' then CBZanOnOff.Text := '1';//標準で載せる

	sSql := 'INSERT INTO ' + CtblMTokuisaki;
  sSql := sSql + ' (';
  sSql := sSql + ' TokuisakiCode1, TokuisakiCode2, TokuisakiName, ';
  sSql := sSql + ' TokuisakiNameYomi, CName, FName, ShiharaiKubun,';
  sSql := sSql + ' SeikyuuSimebi, ShiharaibiM, ShiharaibiD, Tel, Fax, ';
  sSql := sSql + ' Memo, EstimateMemo, Zip,Add1, Add2, Add3, ';
  sSql := sSql + ' ZanOnOff,';

  //事務所住所
  sSql := sSql + ' ZipOffice,Add1Office, Add2Office, Add3Office, TelOffice, FaxOffice, ';

  //自宅住所
  sSql := sSql + ' ZipHome,Add1Home, Add2Home, Add3Home, TelHome, FaxHome, ';

  //請求関係
  sSql := sSql + 'SeikyuushoFlag, SeikyuushoHassouSaki, NengajouHassouSaki, ';

//得意先コード類
  sSql := sSql + 'Ka, TantoushaCode, ChainCode, Bikou, ShuukeiCode, HeitenFlag,';
  sSql := sSql + 'AtenaOffice, AtenaHome';
  sSql := sSql + ',GCode,Kubun420450';

  //2002.08.10
  sSql := sSql + ',TokuisakiNameUp';

  //2004.04.15
  //集金コード
  sSql := sSql + ',ShuukinCode';

  //売り上げコード
  sSql := sSql + ',UriageCode';

  //登録日
  sSql := sSql + ',Tourokubi';

  //休業フラグ
  sSql := sSql + ',KyuugyouCheck';

  //休業日
  sSql := sSql + ',KyuugyouDate';

  //閉店日
  sSql := sSql + ',HeitenDate';

  //携帯電話
  sSql := sSql + ',KeitaiDenwa';

  //数値型　予備1,予備2,予備3,予備4,予備5
  sSql := sSql + ',Yobi_i_1';
  sSql := sSql + ',Yobi_i_2';
  sSql := sSql + ',Yobi_i_3';
  sSql := sSql + ',Yobi_i_4';
  sSql := sSql + ',Yobi_i_5';

  //文字型　予備1,予備2,予備3,予備4,予備5
  sSql := sSql + ',Yobi_c_1';
  sSql := sSql + ',Yobi_c_2';
  sSql := sSql + ',Yobi_c_3';
  sSql := sSql + ',Yobi_c_4';
  sSql := sSql + ',Yobi_c_5';


  //インフォマート対象フラグ add utada 2014.08.21
  sSql := sSql + ',InfomartFlag';
  
  // add thuy 20150804
  sSql := sSql + ',FoodParkFlg';

  sSql := sSql + ')';



  sSql := sSql + ' VALUES ';
  sSql := sSql + ' (';
  sSql := sSql +       sTokuisakiCode1       + ',';
  sSql := sSql +       CBTokuisakiCode2.Text + ','  ;

  sSql := sSql + '''' + CBTokuisakiName.Text     + ''',' ;
  sSql := sSql + '''' + CBTokuisakiNameYomi.Text + ''',' ;
  sSql := sSql + '''' + CBCName.Text             + ''',' ;
  sSql := sSql + '''' + CBFName.Text             + ''',' ;

  if Pos(' ', CBShiharaiKubun.Text) > 0 then begin
	  sSql := sSql + Copy(CBShiharaiKubun.Text, 1, Pos(' ', CBShiharaiKubun.Text)-1) + ','   ;
  end else begin
	  sSql := sSql + CBShiharaiKubun.Text + ',' ;
  end;

  if Pos(' ', CBSeikyuuSimebi.Text) > 0 then begin
	  sSql := sSql + Copy(CBSeikyuuSimebi.Text, 1, Pos(' ', CBSeikyuuSimebi.Text)-1) + ','   ;
  end else begin
	  sSql := sSql + CBSeikyuuSimebi.Text + ','   ;
  end;

  if Pos(' ', CBShiharaibiM.Text) > 0 then begin
  	sSql := sSql + Copy(CBShiharaibiM.Text,   1, Pos(' ', CBShiharaibiM.Text)-1) + ','   ;
	end else begin
	  sSql := sSql + CBShiharaibiM.Text + ','   ;
  end;

  if Pos(' ', CBShiharaibiD.Text) > 0 then begin
  	sSql := sSql + Copy(CBShiharaibiD.Text,   1, Pos(' ', CBShiharaibiD.Text)-1) + ','   ;
	end else begin
	  sSql := sSql + CBShiharaibiD.Text + ','   ;
  end;

  sSql := sSql + '''' + CBTel.Text               + ''',' ;
  sSql := sSql + '''' + CBFax.Text               + ''',' ;
  sSql := sSql + '''' + CBMemo.Text              + ''',' ;
  sSql := sSql + '''' + RMemo.Text               + ''',' ;
  sSql := sSql + '''' + CBZip.Text               + ''',' ;
	sSql := sSql + '''' + CBAdd1.Text              + ''',' ;
  sSql := sSql + '''' + CBAdd2.Text              + ''',' ;
  sSql := sSql + '''' + CBAdd3.Text              + ''',' ;
  sSql := sSql +        Copy(CBZanOnOff.Text, 1, 1)  + ',';
  //事務所住所
  sSql := sSql + '''' + CBZipOffice.Text         + ''',' ;
	sSql := sSql + '''' + CBAdd1Office.Text        + ''',' ;
  sSql := sSql + '''' + CBAdd2Office.Text        + ''',' ;
  sSql := sSql + '''' + CBAdd3Office.Text        + ''',' ;
  sSql := sSql + '''' + CBTelOffice.Text         + ''',' ;
  sSql := sSql + '''' + CBFaxOffice.Text         + ''',' ;
  //自宅住所
  sSql := sSql + '''' + CBZipHome.Text           + ''',' ;
	sSql := sSql + '''' + CBAdd1Home.Text          + ''',' ;
  sSql := sSql + '''' + CBAdd2Home.Text          + ''',' ;
  sSql := sSql + '''' + CBAdd3Home.Text          + ''',' ;
  sSql := sSql + '''' + CBTelHome.Text           + ''',' ;
  sSql := sSql + '''' + CBFaxHome.Text           + ''',' ;

  sSql := sSql + Copy(CBSeikyuushoFlag.Text, 1, 1) + ','; //請求書発行フラグ

	//請求書発送区分
	//SeikyuushoHassouSaki (smallint) <--Add
	//0=持参、1=会社、2=店、3=自宅
  if rbSShop.Checked = True then iTemp := 2
  else if rbSOffice.Checked = True then iTemp := 1
  else if rbSHome.Checked = True then iTemp := 3
  else if rbSJisan.Checked = True then iTemp := 0;
  sSql := sSql + IntToStr(iTemp) + ',';

	//年賀状発送区分
	//NengajouHassouSaki (smallint) <--Add
	//0=出さない、1=会社、2=店、3=自宅
  if rbNShop.Checked = True then iTemp := 2
  else if rbNOffice.Checked = True then iTemp := 1
  else if rbNHome.Checked = True then iTemp := 3
  else if rbNNo.Checked = True then iTemp := 0;

  sSql := sSql + IntToStr(iTemp) + ',';

	//課 Ka		(smallint)
  sSql := sSql + EditKa.Text + ',';

	//担当者コード TantoushaCode	(smallint)
  sSql := sSql + EditTantoushaCode.Text + ',';

	//チェーンコード ChainCode		(smallint)
  sSql := sSql +  EditChainCode.Text + ',';

	//備考 Bikou		(Text)
  sSql := sSql + '''' + TextBikou.Text + ''',';

	//集計コード ShuukeiCode	(smallint)
  sSql := sSql  + EditShuukeiCode.Text + ',';

	//閉店フラグ HeitenFlag	(smallint)
	//	1=閉店
  if cbHeitenFlag.Checked = True then iTemp := 1
  else iTemp := 0;
  sSql := sSql + IntToStr(iTemp) + ',';

  sSql := sSql + '''' + EditAtenaOffice.Text + ''',';
  sSql := sSql + '''' + EditAtenaHome.Text   + '''';

  //業種コード
  sSql := sSql + ',''' + sGCode + '''';

  //商品区分コード420,450
  if RB420.Checked = True then begin
     sKubun := '420';
  end else if RB450.Checked = True then begin
     sKubun := '450';
  end else begin
     sKubun := '0';
  end;
  sSql := sSql + ',' + sKubun;

  sSql := sSql + ',''' + edTokuisakiNameUp.Text + '''';

  //集金コード
  sTemp := ShuukinCode.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(ShuukinCode.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;

  //売り上げコード
  sTemp := UriageCode.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(UriageCode.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;

  //登録日
  sSql := sSql + ',''' + DatetoStr(Tourokubi.date) + '''';

  //休業フラグ
  if KyuugyouDate.Checked = True then begin
     sKubun := '1';
  end else if KyuugyouDate.Checked = False then begin
     sKubun := '0';
  end else begin
     sKubun := '1';
  end;
  sSql := sSql + ',' + sKubun;

  //休業日
  sSql := sSql + ',''' + DatetoStr(KyuugyouDate.date) + '''';

  //閉店日
  sSql := sSql + ',''' + DatetoStr(HeitenDate.date) + '''';

  //携帯電話
  sSql := sSql + ',''' + Trim(KeitaiDenwa.Text) + '''';

  //数値型　予備1,予備2,予備3,予備4,予備5
  sTemp := Yobi_i_1.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(Yobi_i_1.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;


  sTemp := Yobi_i_2.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(Yobi_i_2.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;

  sTemp := Yobi_i_3.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(Yobi_i_3.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;

  sTemp := Yobi_i_4.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(Yobi_i_4.Text);
   end else begin
    sSql := sSql + ',' + '0';
  end;

  sTemp := Yobi_i_5.Text;
  if Length(sTemp) > 0 then begin
    sSql := sSql + ',' + Trim(Yobi_i_5.Text);
  end else begin
    sSql := sSql + ',' + '0';
  end;


  //文字型　予備1,予備2,予備3,予備4,予備5
  sSql := sSql + ',''' + Trim(Yobi_c_1.Text) + '''';
  sSql := sSql + ',''' + Trim(Yobi_c_2.Text) + '''';
  sSql := sSql + ',''' + Trim(Yobi_c_3.Text) + '''';
  sSql := sSql + ',''' + Trim(Yobi_c_4.Text) + '''';
  sSql := sSql + ',''' + Trim(Yobi_c_5.Text) + '''';


  //インフォマート対象フラグ
  if InfomartFlag.Checked = True then begin
     sInfomartFlag := '1';
  end else if InfomartFlag.Checked = False then begin
     sInfomartFlag := '0';
  end else begin
     sInfomartFlag := '0';
  end;
  sSql := sSql + ',' + sInfomartFlag;
  
  // add 20150408 thuy
  if FoodParkFlg.Checked = True then begin
     sFoodParkFlg := '1';
  end else if FoodParkFlg.Checked = False then begin
     sFoodParkFlg := '0';
  end else begin
     sFoodParkFlg := '0';
  end;
  sSql := sSql + ',' + sFoodParkFlg;


  sSql := sSql + ')';

  with frmDMMaster.QueryUpdate do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with
end;


//エクセルへ出力ボタンがクリックされた
procedure TfrmMTokuisaki2.BitBtn2Click(Sender: TObject);
var
	sSql, sLine, strMemo, sAdd3, sTmp : String;
 	F : TextFile;
  i : integer;
begin
//2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
  //コメントアウト 2009.04.08 utada
	//end else if GPassWord = CPassword2 then begin
  //  Beep;
	end else begin
	//ShowMessage('PassWordが違います');
   exit;
	end;
	GPassWord := '';



	//確認メッセージ
  if MessageDlg('得意先一覧を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMTokuisaki;

  //テスト用
  //sSql := sSql + ' WHERE TokuisakiCode1 < 20 ';

  sSql := sSql + ' ORDER BY TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Tokuisaki);
	Rewrite(F);
	CloseFile(F);

  //作成日
	sLine := DateToStr(Date);
  HMakeFile(CFileName_Tokuisaki, sLine);

  //タイトルを出力する
  sLine := '得意先コード１';
  sLine := sLine + ',' + '得意先コード２';
  sLine := sLine + ',' + '得意先名(追)';
  sLine := sLine + ',' + '得意先名';
  sLine := sLine + ',' + '得意先名2';
  sLine := sLine + ',' + 'よみ';
  sLine := sLine + ',' + 'チェーン名';
  sLine := sLine + ',' + '振込人名';
  sLine := sLine + ',' + '請求書に残を載せる載せない';
  sLine := sLine + ',' + '支払い区分';
  sLine := sLine + ',' + '支払い月';
  sLine := sLine + ',' + '支払い日';
  sLine := sLine + ',' + '請求締め日';
  //店舗
  sLine := sLine + ',' + '(店舗)郵便番号';
  sLine := sLine + ',' + '都道府県';
  sLine := sLine + ',' + '市町村';
  sLine := sLine + ',' + '番地';
  sLine := sLine + ',' + '電話番号';
  sLine := sLine + ',' + 'FAX';
  //事務所
  sLine := sLine + ',' + '(事務所)郵便番号';
  sLine := sLine + ',' + '都道府県';
  sLine := sLine + ',' + '市町村';
  sLine := sLine + ',' + '番地';
  sLine := sLine + ',' + '電話番号';
  sLine := sLine + ',' + 'FAX';
  sLine := sLine + ',' + '(事務所)宛名';
  //自宅
  sLine := sLine + ',' + '(自宅)郵便番号';
  sLine := sLine + ',' + '都道府県';
  sLine := sLine + ',' + '市町村';
  sLine := sLine + ',' + '番地';
  sLine := sLine + ',' + '電話番号';
  sLine := sLine + ',' + 'FAX';
  sLine := sLine + ',' + '(自宅)宛名';


  sLine := sLine + ',' + '請求書発行先';
  sLine := sLine + ',' + '年賀状発送先';
  sLine := sLine + ',' + '請求書出す・出さない';
  sLine := sLine + ',' + '課';
  sLine := sLine + ',' + '担当者コード';
  sLine := sLine + ',' + 'チェーンコード';
  sLine := sLine + ',' + '集計コード';
  sLine := sLine + ',' + '得意先Memo';
  sLine := sLine + ',' + '領収書発行Memo';
  sLine := sLine + ',' + '備考';
  sLine := sLine + ',' + '閉店';
  sLine := sLine + ',' + '420_450';

  //2004.04.15 Added
  sLine := sLine + ',' + '集金コード';
  sLine := sLine + ',' + '売上コード';
  sLine := sLine + ',' + '登録日';
  sLine := sLine + ',' + '休業フラグ';
  sLine := sLine + ',' + '休業日';
  sLine := sLine + ',' + '閉店日';
  sLine := sLine + ',' + '携帯電話';

  //2004.06.03
  sLine := sLine + ',' + '業種コード';

  //2006.07.13
  sLine := sLine + ',' + '予備１';




  HMakeFile(CFileName_Tokuisaki, sLine);


  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 1;
    while not EOF do begin
    	//得意先コード１
    	sLine := FieldByName('TokuisakiCode1').AsString;
    	//得意先コード２
    	sLine := sLine + ',' + FieldByName('TokuisakiCode2').AsString;

    	//得意先名 上
    	sLine := sLine + ',' + FieldByName('TokuisakiNameUp').AsString;

    	//得意先名
    	sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;

      //2004.08.03
      //得意先名2
      sTmp := Trim(FieldByName('TokuisakiNameUp').AsString);
      if Length(sTmp) > 0 then begin
       	sLine := sLine + ',' + sTmp + '　' + FieldByName('TokuisakiName').AsString;
      end else begin
       	sLine := sLine + ',' + FieldByName('TokuisakiName').AsString;
      end;


    	//得意先名 よみ
    	sLine := sLine + ',' + FieldByName('TokuisakiNameYomi').AsString;
    	//チェーン名
    	sLine := sLine + ',' + FieldByName('CName').AsString;
    	//振込人名
    	sLine := sLine + ',' + FieldByName('FName').AsString;
      //請求書に残を載せる載せない
    	sLine := sLine + ',' + FieldByName('ZanOnOff').AsString;
      //支払い区分
    	sLine := sLine + ',' + FieldByName('ShiharaiKubun').AsString;
      //支払い月
    	sLine := sLine + ',' + FieldByName('ShiharaibiM').AsString;
      //支払い日
    	sLine := sLine + ',' + FieldByName('ShiharaibiD').AsString;
      //請求締め日
    	sLine := sLine + ',' + FieldByName('SeikyuuSimebi').AsString;

      //##### 店情報 #####
      //郵便番号
    	sLine := sLine + ',' + FieldByName('Zip').AsString;
      //都道府県
      sLine := sLine + ',' + FieldByName('Add1').AsString;
      //市町村
    	sLine := sLine + ',' + FieldByName('Add2').AsString;
      //番地
      sAdd3 := FieldByName('Add3').AsString;
      sAdd3 := ' ' + sAdd3;
    	sLine := sLine + ',' + sAdd3;
      //電話番号
    	sLine := sLine + ',' + FieldByName('Tel').AsString;
      //FAX
    	sLine := sLine + ',' + FieldByName('Fax').AsString;

      //##### 事務所情報 #####
      //郵便番号
    	sLine := sLine + ',' + FieldByName('ZipOffice').AsString;
      //都道府県
      sLine := sLine + ',' + FieldByName('Add1Office').AsString;
      //市町村
    	sLine := sLine + ',' + FieldByName('Add2Office').AsString;
      //番地
      sAdd3 :=  FieldByName('Add3Office').AsString;
      { -を − に変換する
  		while Pos('-', sAdd3) > 0 do
    		sAdd3[Pos('-', sAdd3)] := 'l';
      }
      sAdd3 := ' ' + sAdd3;
    	sLine := sLine + ',' + sAdd3;

      //電話番号
    	sLine := sLine + ',' + FieldByName('TelOffice').AsString;
      //FAX
    	sLine := sLine + ',' + FieldByName('FaxOffice').AsString;
      //宛名
    	sLine := sLine + ',' + FieldByName('AtenaOffice').AsString;

      //##### 自宅情報 #####
      //郵便番号
    	sLine := sLine + ',' + FieldByName('ZipHome').AsString;
      //都道府県
      sLine := sLine + ',' + FieldByName('Add1Home').AsString;
      //市町村
    	sLine := sLine + ',' + FieldByName('Add2Home').AsString;
      //番地
    	sAdd3 := FieldByName('Add3Home').AsString;
      sAdd3 := ' ' + sAdd3;
    	sLine := sLine + ',' + sAdd3;

      //電話番号
    	sLine := sLine + ',' + FieldByName('TelHome').AsString;
      //FAX
    	sLine := sLine + ',' + FieldByName('FaxHome').AsString;
      //宛名
    	sLine := sLine + ',' + FieldByName('AtenaHome').AsString;

      //請求書発行先
    	sLine := sLine + ',' + FieldByName('SeikyuushoHassouSaki').AsString;
  	  //年賀状発送先
	   	sLine := sLine + ',' + FieldByName('NengajouHassouSaki').AsString;
      //請求書出す・出さない
	   	sLine := sLine + ',' + FieldByName('SeikyuushoFlag').AsString;
      //課
	   	sLine := sLine + ',' + FieldByName('Ka').AsString;

      //担当者コード
	   	sLine := sLine + ',' + FieldByName('TantoushaCode').AsString;
      //チェーンコード
	   	sLine := sLine + ',' + FieldByName('ChainCode').AsString;
      //集計コード
	   	sLine := sLine + ',' + FieldByName('ShuukeiCode').AsString;

      //得意先Memo
      //1999/09/06 改行の入ったメモをExcelで利用するため
    	//sLine := sLine + ',' + FieldByName('Memo').AsString;
    	strMemo := FieldByName('Memo').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;
      //領収書発行Memo
    	strMemo := FieldByName('EstimateMemo').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;
      //備考
    	strMemo := FieldByName('Bikou').AsString;
      strMemo := HKLib.RepraceCh(strMemo, #10, Char(' '));
      strMemo := HKLib.RepraceCh(strMemo, #13, Char(' '));
    	sLine := sLine + ',' + strMemo;
      //2000/06/16 追加
      //閉店コード
	   	sLine := sLine + ',' + FieldByName('HeitenFlag').AsString;

      //2002/06/24 追加
      //420450
	   	sLine := sLine + ',' + FieldByName('Kubun420450').AsString;

      //2004.04.15 追加
	   	sLine := sLine + ',' + FieldByName('ShuukinCode').AsString;
	   	sLine := sLine + ',' + FieldByName('UriageCode').AsString;
	   	sLine := sLine + ',' + FieldByName('Tourokubi').AsString;
	   	sLine := sLine + ',' + FieldByName('KyuugyouCheck').AsString;
	   	sLine := sLine + ',' + FieldByName('KyuugyouDate').AsString;
	   	sLine := sLine + ',' + FieldByName('HeitenDate').AsString;
	   	sLine := sLine + ',' + FieldByName('KeitaiDenwa').AsString;

      //2004.06.03
	   	sLine := sLine + ',' + FieldByName('GCode').AsString;

      //2007.07.13
	   	sLine := sLine + ',' + FieldByName('Yobi_i_1').AsString;

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;

		  HMakeFile(CFileName_Tokuisaki, sLine);
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '得意先一覧.xls', '', SW_SHOW);
end;

//チェーン名
procedure TfrmMTokuisaki2.CBCNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('CName', CBCName.Text, 1);
  end;
end;

//振り込み人名
procedure TfrmMTokuisaki2.CBFNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('FName', CBFName.Text, 1);
  end;
end;

//2000/11/04
//年賀状用タックシール印刷
//0=出さない、1=会社、2=店、3=自宅
procedure TfrmMTokuisaki2.BitBtn4Click(Sender: TObject);
var
	i, iNengajou : Integer;
  sLine, sSql, sYuubin, sAdd1, sAdd2, sAdd3, sName : String;
  sAddPre, sTokuisakiCode1, sTokuisakiCode2, sYomi,sHeitenFlag : String;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('年賀状用タックシールを出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' NengajouHassouSaki>0 ';

  //テスト用
  //sSql := sSql + ' AND TokuisakiCode1 < 50 ';

  //2000/11/20 Added
  sSql := sSql + ' AND HeitenFlag <> 1 ';

  sSql := sSql + ' ORDER BY TokuisakiCode1';

	//出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Nengajou);
	Rewrite(F);
	CloseFile(F);

  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    while not EOF do begin
    	iNengajou := FieldByName('NengajouHassouSaki').AsInteger;
      //  sYuubin, sAdd1, sAdd2, sName : String;
      if iNengajou = 1 then begin  //会社
      	sYuubin := FieldByName('ZipOffice').AsString;
	    	sAdd1   := FieldByName('Add1Office').AsString;
	    	sAdd2   := FieldByName('Add2Office').AsString;
	    	sAdd3   := FieldByName('Add3Office').AsString;
        sName   := FieldByName('AtenaOffice').AsString;
      end else if iNengajou = 2 then begin //店
      	sYuubin := FieldByName('Zip').AsString;
	    	sAdd1   := FieldByName('Add1').AsString;
	    	sAdd2   := FieldByName('Add2').AsString;
	    	sAdd3   := FieldByName('Add3').AsString;
        sName   := FieldByName('TokuisakiName').AsString;
      end else if iNengajou = 3 then begin //自宅
      	sYuubin := FieldByName('ZipHome').AsString;
	    	sAdd1   := FieldByName('Add1Home').AsString;
	    	sAdd2   := FieldByName('Add2Home').AsString;
	    	sAdd3   := FieldByName('Add3Home').AsString;
        sName   := FieldByName('AtenaHome').AsString;
      end else begin
	    	ShowMessage(FieldByName('TokuisakiCode1').AsString+'のデータが不完全です');
      end;
      //2000/11/20 Added
      sTokuisakiCode1 := FieldByName('TokuisakiCode1').AsString;
      sTokuisakiCode2 := FieldByName('TokuisakiCode2').AsString;
      sYomi := FieldByName('TokuisakiNameYomi').AsString;
      sHeitenFlag := FieldByName('HeitenFlag').AsString;
{
      if iNengajou = 1 then begin  //会社
        //sNameの整形 //半角スペース以降を表示
		    if Pos(' ', sName) > 0 then begin
         	sName := Copy(sName, Pos(' ', sName)+1, Length(sName));
        end else if Pos('　', sName) > 0 then begin //全角スペースにも対応
         	sName := Copy(sName, Pos('　', sName)+2, Length(sName));
        end;
        sName := sName + '　御中';
      end else if iNengajou = 2 then begin //店
        //sNameの整形 //半角スペース以降を表示
		    if Pos(' ', sName) > 0 then begin
         	sName := Copy(sName, Pos(' ', sName)+1, Length(sName));
        end else if Pos('　', sName) > 0 then begin //全角スペースにも対応
         	sName := Copy(sName, Pos('　', sName)+2, Length(sName));
        end;
        sName := sName + '　御中';
      end else if iNengajou = 3 then begin //自宅
        sName := Trim(sName) + '　様';
      end;
}
//2004.08.03
      if iNengajou = 1 then begin  //会社
        //sNameの整形 //半角スペース以降を表示
        sName := Trim(sName) + '　御中';
      end else if iNengajou = 2 then begin //店
        //sNameの整形 //半角スペース以降を表示
        if Pos('　', sName) > 0 then begin //全角スペースにも対応
         	sName := Copy(sName, Pos('　', sName)+2, Length(sName));
        end;
        sName := Trim(sName) + '　御中';
      end else if iNengajou = 3 then begin //自宅
        sName := Trim(sName) + '　様';
      end;


      //ファイルに書き込み
    	sLine := sYuubin;
    	sLine := sLine + ',' + sAdd2 + ',' + sAdd3;
    	sLine := sLine + ',' + sName;

      //2000/11/20 Added
    	sLine := sLine + ',' + sTokuisakiCode1;
    	sLine := sLine + ',' + sTokuisakiCode2;
    	sLine := sLine + ',' + sYomi;

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      //If Address is same then do not write.
      if (sAddPre = (sAdd2 + sAdd3)) and (Length(sAdd2 + sAdd3)>0) then begin
      	;
      end else begin
		  	HMakeFile(CFileName_Nengajou, sLine);
      end;
      sAddPre := sAdd2+sAdd3;
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '鷹松屋宛名ラベル.xls', '', SW_SHOW);

end;
//2000/11/20
//年賀状発送先をすべて店にする
procedure TfrmMTokuisaki2.BitBtn5Click(Sender: TObject);
var
	sSql : String;
begin
	//確認メッセージ
	if MessageDlg('年賀状の発送先をすべて店にしますか?',
		mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		Exit;
	end;

	sSql := 'UPDATE ' + CtblMTokuisaki;
  sSql := sSql + ' SET ';

	//年賀状発送区分
	//NengajouHassouSaki (smallint) <--Add
	//0=出さない、1=会社、2=店、3=自宅
  sSql := sSql + 'NengajouHassouSaki  = 2';
  with frmDMMaster.QueryUpdate do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with

 	ShowMessage('登録に成功しました');

end;

procedure TfrmMTokuisaki2.CBChainCodeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('ChainCode', CBChainCode.Text, 2);
  end;

end;

procedure TfrmMTokuisaki2.CBAtenaHomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBTokuisakiName('AtenaHome', CBAtenaHome.Text, 1);
  end;
end;

procedure TfrmMTokuisaki2.CBChainCodeChange(Sender: TObject);
begin

	 EditChainCode.Text:=CBChainCode.text;
end;

procedure TfrmMTokuisaki2.FromClear;
begin
  CBTokuisakiCode1.Text    := '';
	CBTokuisakiCode2.Text    := '';
	CBTokuisakiNameYomi.Text := '';
	CBTokuisakiName.Text     := '';
	CBCName.Text             := '';
  CBFName.Text             := '';
	CBShiharaiKubun.Text     := '10 現金';
	CBSeikyuuSimebi.Text     := '0  月末';
	CBShiharaibiM.Text       := '0  当月';
	CBShiharaibiD.Text       := '0  月末';
	CBTel.Text               := '';
	CBZanOnOff.Text          := '';
	CBMemo.Text              := '';
	RMemo.Text               := '';

  CBZip.Text  := '';
  CBAdd1.Text := '';
  CBAdd2.Text := '';
  CBAdd3.Text := '';
  CBFax.Text  := '';

  //事務所住所
  CBZipOffice.Text     := '';
  CBAdd1Office.Text    := '';
  CBAdd2Office.Text    := '';
  CBAdd3Office.Text    := '';
	CBTelOffice.Text     := '';
  CBFaxOffice.Text     := '';
  EditAtenaOffice.Text := '';

  //自宅住所
  CBZipHome.Text     := '';
  CBAdd1Home.Text    := '';
  CBAdd2Home.Text    := '';
  CBAdd3Home.Text    := '';
  CBTelHome.Text     := '';
  CBFaxHome.Text     := '';
  EditAtenaHome.Text := '';
  CBAtenaHome.Text   := '';

	//請求書発行フラグ
  CBSeikyuushoFlag.Text := '0 出す';

	//請求書発送区分
	//SeikyuushoHassouSaki (smallint) <--Add
	//0=持参、1=会社、2=店、3=自宅
	rbSJisan.Checked := False;

	//年賀状発送区分
  //NengajouHassouSaki (smallint) <--Add
	//0=持参、1=会社、2=店、3=自宅
	rbNNo.Checked := False;

	//課
	//Ka		(smallint)
  EditKa.Text := '0';

	//担当者コード
	//TantoushaCode	(smallint)
  EditTantoushaCode.Text := '0';

	//チェーンコード
	//ChainCode		(smallint)
  EditChainCode.Text := '0';

	//備考
	//Bikou		(Text)
  textBikou.Text := '';

	//集計コード
  //ShuukeiCode	(smallint)
  EditShuukeiCode.Text := '0';

	//閉店フラグ
	//HeitenFlag	(smallint)
 	//	1=閉店
	cbHeitenFlag.Checked := False;

  //業種コード
  CBGCode.Text  := '0';

  //2001/11/26 追加
  //商品単価区分420,450
  RB420.Checked := False;
  RB450.Checked := False;

  //業種コード
  CBGCode.Text := '01';

  //得意先名　上
  edTokuisakiNameUp.Text  := '';

   //2004.04.15 追加 h, Kubota
   //
   ShuukinCode.Text  := '0';

   //2004.08.03
   UriageCode.Text   := '0';

   Tourokubi.Date    := Date();
   KyuugyouDate.Date := Date();
   HeitenDate.Date   := Date();

   KyuugyouCheck.Checked := False;

   InfomartFlag.Checked := False;// add 20140821 utada
   
   FoodParkFlg.Checked := False;// add 20140408 thuy

   rbSHome.Checked   := True;
	 rbSShop.Checked   := False;
	 rbSOffice.Checked := False;
	 rbSJisan.Checked  := False;


   Yobi_i_1.Text   := '0';
   Yobi_i_2.Text   := '0';
   Yobi_i_3.Text   := '0';
   Yobi_i_4.Text   := '0';
   Yobi_i_5.Text   := '0';
   Yobi_c_1.Text   := '';
   Yobi_c_2.Text   := '';
   Yobi_c_3.Text   := '';
   Yobi_c_4.Text   := '';
   Yobi_c_5.Text   := '';

   KeitaiDenwa.Text := '';

   EditKa.text := '0';
   EditTantoushaCode.Text := '0';
   EditChainCode.Text := '0';

   //2004.07.13
   CBZanOnOff.Text := '1 載せる';

end;

procedure TfrmMTokuisaki2.Button1Click(Sender: TObject);
begin
  FromClear;
end;

end.
