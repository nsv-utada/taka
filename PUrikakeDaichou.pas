unit PUrikakeDaichou;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, Grids, StdCtrls, Buttons, ExtCtrls, DBGrids, ComCtrls, DBTables, ShellAPI;

type
  TfrmPUrikakeDaichou = class(TfrmMaster)
    Label5: TLabel;
    EditTokuisakiCode: TEdit;
    Label10: TLabel;
    CBTokuisakiName: TComboBox;
    Label3: TLabel;
    Label4: TLabel;
    BitBtn4: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn5: TBitBtn;
    DTPFrom: TDateTimePicker;
    DTPTo: TDateTimePicker;
    BitBtn6: TBitBtn;
    SGUDaichou: TStringGrid;
    Label6: TLabel;
    EditShiharaijouken: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure EditTokuisakiCodeExit(Sender: TObject);
    procedure CBTokuisakiNameChange(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SGUDaichouDrawCell(Sender: TObject; Col, Row: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SGUDaichouKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SGUDaichouMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn6Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
		procedure MakeSG2(myQuery : TQuery; iFlag:Integer);
		procedure ClearSG;
		function GetZandaka(sTokuisakiCode1, sFrom, sTo:String):Integer;
		procedure PrintSG(iFlag:integer);

  public
    { Public 宣言 }
  end;


  const
	//グリッドの列の定義

{
  CintDate          = 0;
  CintMemo          = 1;
  CintUriKingaku    = 2;
  CintUkeKingaku    = 3;
  CintSasihiki      = 4;
  CintRuikei        = 5;
  CintGatsubun      = 6;   //199/09/10追加
  CintEnd           = 7;   //ターミネーター
}

  CintDate          = 0;
  CintMemo          = 1;
  CintUriKingaku    = 2;
  CintSasihiki      = 3;
  CintRuikei        = 4;
  CintUkeKingaku    = 5;
  CintGatsubun      = 6;   //199/09/10追加
  CintEnd           = 7;   //ターミネーター


	GRowCount = 700; //行数

var
  frmPUrikakeDaichou: TfrmPUrikakeDaichou;

implementation
uses
	DMMAster, MTokuisaki, Inter, HKLib, PBill, TKaishuu;

{$R *.DFM}

var
	GiRow : Integer;
  GiKungakuSum, GiTax: Integer;
  GsShimebi : String;
  GbFlag : Boolean; //締め日の計算をしたかどうか　する前->False したあと- > True


procedure TfrmPUrikakeDaichou.FormCreate(Sender: TObject);
begin
  inherited;
	Width := 780;
  Height := 540;


  //ストリンググリッドの作成
  MakeSG;

  //でーたの引継
  EditTokuisakiCode.Text := GTokuisakiCode;
  GTokuisakiName := CBTokuisakiName.Text;

  //1999/08/10変更
  //DTPFrom.Date := Date() - 60;
  //DTPFrom.Date := StrToDate('1998/12/30');
  //2000/05/04変更
  //DTPFrom.Date := StrToDate('2000/04/01');

  //2001/06/20
  DTPFrom.Date := Date() - 180;

  DTPTo.Date := Date();

end;

procedure TfrmPUrikakeDaichou.MakeSG;
begin
  with SGUDaichou do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;

    ColWidths[CintDate]:= 100;
    Cells[CintDate, 0] := '日付';

    ColWidths[CintMemo]:= 170;
    Cells[CintMemo, 0] := '適 用';

    ColWidths[CintUriKingaku]:= 130;
    Cells[CintUriKingaku, 0] := '売上金額';

    ColWidths[CintUkeKingaku]:= 80;
    Cells[CintUkeKingaku, 0] := '受入金額';

    ColWidths[CintSasihiki]:= 80;
    Cells[CintSasihiki, 0] := '当月請求';

    ColWidths[CintRuikei]:= 80;
    Cells[CintRuikei, 0] := '累　計';

    ColWidths[CintGatsubun]:= 100;
    Cells[CintGatsubun, 0] := '月分';

  end;//of with

end;

procedure TfrmPUrikakeDaichou.EditTokuisakiCodeExit(Sender: TObject);
var
  sShiharaibiM, sShiharaibiD, sWhere:String;
begin
	//得意先名を検索する
  CBTokuisakiName.Text := MTokuisaki.GetTokuisakiName(EditTokuisakiCode.Text);
  //支払い条件を表示する
  sWhere := 'TokuisakiCode1 = ' + Trim(EditTokuisakiCode.Text);
	sShiharaibiM := GetFieldData(CtblMTokuisaki, 'ShiharaibiM', sWhere);
  if sShiharaibiM = '0' then
  	sShiharaibiM := '当月'
  else if sShiharaibiM = '1' then
  	sShiharaibiM := '翌月'
  else if sShiharaibiM = '2' then
  	sShiharaibiM := '翌々月'
  else if sShiharaibiM = '3' then
  	sShiharaibiM := '翌翌々月';

	sShiharaibiD := GetFieldData(CtblMTokuisaki, 'ShiharaibiD', sWhere);
  if sShiharaibiD = '0' then
  	sShiharaibiD := '月末'
  else
  	sShiharaibiD := sShiharaibiD + '日';

  EditShiharaijouken.Text := sShiharaibiM + sShiharaibiD + '払い';
end;

procedure TfrmPUrikakeDaichou.CBTokuisakiNameChange(Sender: TObject);
begin
  inherited;

end;


//グリッドのクリア
procedure TfrmPUrikakeDaichou.ClearSG;
var
  iCol, iRow : Integer;
begin
  with SGUDaichou do begin
  	for iCol := 0 to ColCount do begin
    	for iRow := 1 to RowCount do begin
	      Cells[iCol, iRow] := '';
      end;
    end;

  end;
end;

//得意先の期間の売上高の合計を求めて、グリッドに表示する
function TfrmPUrikakeDaichou.GetZandaka(sTokuisakiCode1, sFrom, sTo: String):Integer;
var
	sSql : String;
begin
	//表示情報の取得
	sSql := 'SELECT KSum = Sum(UriageKingaku) ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'TokuisakiCode1 = ' +  sTokuisakiCode1;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate Between ';
  sSql := sSql + '''' + sFrom + '''';
  sSql := sSql + ' AND ';
  sSql := sSql + '''' + sTo + '''';

  //2000/06/19
  {
  sSql := sSql + ' AND ';
  sSql := sSql + '(';
  sSql := sSql + ' UriageKingaku > 0 ';
  sSql := sSql + ' OR ';
  sSql := sSql + ' ((UriageKingaku < 0) AND (SUBSTRING(Memo, 1, 2) NOT IN (''返品'', ''値引'')))' ;
  sSql := sSql + ')';
  }


  with frmDMMaster.QueryZandaka do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    Result := FieldByName('KSum').AsInteger;
		SGUDaichou.Cells[CintDate, GiRow] := '';
		SGUDaichou.Cells[CintMemo, GiRow] := '繰越残高';
	  SGUDaichou.Cells[CintSasihiki, GiRow] := FormatFloat('0,', Result);
    Close;
  end;
end;


//グリッドのコンテンツ作成
procedure TfrmPUrikakeDaichou.MakeSG2(myQuery : TQuery; iFlag:integer);
var
	iKingaku, iSum, iRuikei : Integer;
  wYyyy, wMm, wDd : Word;
  sMemo, sYyyyMmDdTo, sYyyyMmDdFrom, sDate, sGatsbun:String;
begin
	//締め日の比較
  DecodeDate(StrToDate(myQuery.FieldByName('DDate').AsString), wYyyy, wMm, wDd);
                                     
  //売上金額の取得
  iKingaku := myQuery.FieldByName('UriageKingaku').AsInteger;

	with SGUDaichou do begin
  	sDate := myQuery.FieldByName('DDate').AsString;
  	sMemo := myQuery.FieldByName('Memo').AsString;
    sMemo := Trim(sMemo);

  	Cells[CintMemo, GiRow] := sMemo;
  	Cells[CintDate, GiRow] := sDate;

    GiKungakuSum := GiKungakuSum + iKingaku;

    //1999/09/14追加
    sGatsbun := myQuery.FieldByName('Gatsubun').AsString;
	 	Cells[CintGatsubun, GiRow] := Copy(sGatsbun, 1, 7);

    //通常の売上伝票
    if (iKingaku > 0) AND (iFlag = 0) then begin
	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);//３桁ごとに区切って表示
      //GiKungakuSum := GiKungakuSum + iKingaku;

      //1999/06/01　復活
      if (Cells[CintUkeKingaku, GiRow] <> '') or
         (Cells[CintMemo, GiRow] = CsCTAX) then begin
		  	Cells[CintRuikei, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
      end;

    end else if (iKingaku > 0) AND (iFlag = 1) then begin
	    if (Cells[CintMemo, GiRow] <> CsCTAX) then begin
      	if Copy(Cells[CintMemo, GiRow], 1, 4) = '返金' then begin //2000/03/03　Added
		 			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
	  			Cells[CintRuikei, GiRow] := FormatFloat('0,', GiKungakuSum);
  	    end else begin
		    	GiRow := GiRow - 1;
  	      Exit;
        end;
      end;
    end else if (iKingaku <= 0)then begin
    	//返品・値引の場合
      if (Copy(Cells[CintMemo, GiRow], 1, 4) = '返品') or
         (Copy(Cells[CintMemo, GiRow], 1, 4) = '値引') or
         (Copy(Cells[CintMemo, GiRow], 1, 4) = '取消') then begin

	  		Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku);
	      //GiKungakuSum := GiKungakuSum + iKingaku;
      end else begin
      	//2001/03/15
        if sMemo = '##消費税##' then begin
		  		Cells[CintUkeKingaku, GiRow] := '';
          GiKungakuSum := GiKungakuSum - iKingaku;
	  			Cells[CintRuikei, GiRow] := FormatFloat('0,', GiKungakuSum);
        end else begin
		      //受け入れ金額の場合
	  			Cells[CintUkeKingaku, GiRow] := FormatFloat('0,', iKingaku);
		      //1999/06/01　復活
  		    //GiKungakuSum := GiKungakuSum + iKingaku;
	  			Cells[CintRuikei, GiRow] := FormatFloat('0,', GiKungakuSum);
        end;
      end;//of if
    end;

    //消費税の行だったら当月の税抜き合計の残を計算して表示する
    //1999/06/01　累計を表示する
    if Cells[CintMemo, GiRow] = CsCTAX then begin
	    sYyyyMmDdTo := Cells[CintDate, GiRow];
      DecodeDate(StrToDate(sYyyyMmDdTo), wyyyy, Wmm, wDd);

      if wDd >= 28 then begin //月末ならば
      	wDd := 1;
      end else begin
	      wDd := wDd + 1;
	      wMm := wMm - 1;
  	    if wMm = 0 then begin
    	  	wMm := 12;
      	  wYyyy := wYyyy-1;
	      end;
      end;

      sYyyyMmDdFrom := DateToStr(EncodeDate(wYyyy, wMm, wDd));
	    iRuikei := TKaishuu.GetZandaka(EditTokuisakiCode.Text, sYyyyMmDdFrom, sYyyyMmDdTo);
	  	Cells[CintUriKingaku, GiRow] := FormatFloat('0,', iKingaku) +
                                 ' (' + FormatFloat('0,', iRuikei-iKingaku) + ')' ;//３桁ごとに区切って表示
	  	Cells[CintSasihiki, GiRow] := FormatFloat('0,', iRuikei);
	  	Cells[CintRuikei, GiRow] := FormatFloat('0,', GiKungakuSum);//３桁ごとに区切って表示
    end;
  end;

end;

//右寄せなど
procedure TfrmPUrikakeDaichou.SGUDaichouDrawCell(Sender: TObject; Col,
  Row: Integer; Rect: TRect; State: TGridDrawState);
type
  TVowels = set of char;
var
	DRect:  TRect;  Mode: Integer;
  Vowels: TVowels;
	C : LongInt;
  myRect : TRect;
  iCol : Integer;
begin
  if not (gdFixed in state)  then begin

    Vowels := [char(CintUriKingaku), char(CintUkeKingaku), char(CintSasihiki), char(CintRuikei)];
		SGUDaichou.Canvas.FillRect(Rect);
	  DRect.Top := Rect.Top + 2;
  	DRect.Left := Rect.Left + 2;
	  DRect.Right := Rect.Right - 2;
  	DRect.Bottom := Rect.Bottom - 2;

	  if  char(Col) IN Vowels then begin       //右寄せ
  		Mode := DT_RIGHT;
	  end else begin
    	Mode := DT_LEFT;
    end;

  	//else if Col = 2 then   //左寄せ
    //	Mode := DT_LEFT
	  //else    Mode := DT_CENTER;   //中央


{
  	DrawText(SGUDaichou.Canvas.Handle, PChar(SGUDaichou.Cells[Col,Row]),
	           Length(SGUDaichou.Cells[Col,Row]), DRect, Mode);
}

    //消費税の行は色をつける
  	if SGUDaichou.Cells[1, Row] = CsCTAX then begin
			IdentToColor('clBlue', C);
  		SGUDaichou.Canvas.Brush.Color := C;
    	SGUDaichou.Canvas.Font.Color := $FFFFFF - C;

		  for iCol := 0 to CintEnd - 1 do begin
    		myRect := SGUDaichou.CellRect(iCol, Row);
	      SGUDaichou.Canvas.FillRect(myRect);
		    DrawText(SGUDaichou.Canvas.Handle,
  	    					PChar(SGUDaichou.Cells[iCol, Row]),
	  		          Length(SGUDaichou.Cells[iCol, Row]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;
    //回収の行も色をつける
  	if SGUDaichou.Cells[CintUkeKingaku, Row] <> '' then begin
			IdentToColor('clTeal', C);
  		SGUDaichou.Canvas.Brush.Color := C;
    	SGUDaichou.Canvas.Font.Color := $FFFFFF - C;

		  for iCol := 0 to CintEnd - 1 do begin
    		myRect := SGUDaichou.CellRect(iCol, Row);
	      SGUDaichou.Canvas.FillRect(myRect);
		    DrawText(SGUDaichou.Canvas.Handle,
  	    					PChar(SGUDaichou.Cells[iCol, Row]),
	  		          Length(SGUDaichou.Cells[iCol, Row]),
        		      myRect,
            	    DT_CENTER or DT_VCENTER or DT_SINGLELINE);
      end;//of for
      Exit;
    end;
  	DrawText(SGUDaichou.Canvas.Handle, PChar(SGUDaichou.Cells[Col,Row]),
	           Length(SGUDaichou.Cells[Col,Row]), DRect, Mode);

  end;//of if
end;

//消費税の再計算ボタンがクリックされた
procedure TfrmPUrikakeDaichou.BitBtn4Click(Sender: TObject);
var
	sMm, syyyy, sDd : String;
begin
  if CBTokuisakiName.Text = '' then begin
    Showmessage('得意先名が入力されていません');
    exit
  end;
	sMm := InputBox('月の取得', '消費税を計算する年月度を半角数字で入力してください', '199701');
  if Length(sMm) <> 6 then begin
    Showmessage('年月日の入力が不正です');
    exit
  end;
  if sMm = '199701' then begin
    Showmessage('年月日の入力が不正です');
    exit
  end;

  sYyyy := Copy(sMm, 1, 4);
  sMm := Copy(sMm, 5, 2);
  sDd := MTokuisaki.GetShimebi(EditTokuisakiCode.Text);
	PBill.InsertCTax(sYyyy, sMm, sDd, StrToInt(EditTokuisakiCode.Text), SB1);

  //再表示
  PrintSG(0);

end;

//エクセルへ出力ボタンがクリックされた
procedure TfrmPUrikakeDaichou.BitBtn2Click(Sender: TObject);
begin

end;

//印刷開始ボタンがクリックされた
procedure TfrmPUrikakeDaichou.BitBtn3Click(Sender: TObject);
var
  sLine, sDate, sMemo, sUriKingaku, sUkeKingaku, sSasihiki, sRuikei, sGatsubun:String;
  iRow : Integer;
 	F : TextFile;
begin
	//確認メッセージ
  if MessageDlg('印刷しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

	//スタートトランザクション

	//出力するファイルを作成する、すでにあれば削除する
  //請求明細用
 	AssignFile(F, CFileName_UriDaichou);
  Rewrite(F);
	CloseFile(F);

  //基本情報の取得
  Screen.Cursor := crHourGlass;

  sLine := EditTokuisakiCode.Text  + ',' + CBTokuisakiName.Text;
  sLine := sLine + ',(' + EditShiharaijouken.Text + ')';
  HMakeFile(CFileName_UriDaichou, sLine);
  sLine := '日付,適用,売上金額,受入金額,当月請求,累計,月分';
  HMakeFile(CFileName_UriDaichou, sLine);
  with SGUDaichou do begin
  	for iRow := 1 to RowCount do begin
		  sDate        := Cells[CintDate, iRow];
		  sMemo        := Cells[CintMemo, iRow];
      if (sDate = '') AND (sMemo = '') then begin
      	Break;
      end;
		  sUriKingaku  := Cells[CintUriKingaku, iRow];
		  sUkeKingaku  := Cells[CintUkeKingaku, iRow];
		  sSasihiki    := Cells[CintSasihiki, iRow];
		  sRuikei      := Cells[CintRuikei, iRow];
		  sGatsubun    := Cells[CintGatsubun, iRow];

		  sLine := '"' + sDate + '","' + sMemo + '","' + sUriKingaku + '","' +
               sUkeKingaku + '","' +  sSasihiki + '","' + sRuikei+ '","' + sGatsubun + '"';
 		  HMakeFile(CFileName_UriDaichou, sLine);
    end;//of for
  end;//of with
  Screen.Cursor := crDefault;

	//エンドトランザクション
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '売掛台帳.xls', '', SW_SHOW);
end;

procedure TfrmPUrikakeDaichou.SGUDaichouKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  SGUDaichou.Repaint;
end;

procedure TfrmPUrikakeDaichou.SGUDaichouMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  SGUDaichou.Repaint;
end;

//表示ボタンがクリックされた
//締め日で消費税と残金、入金日で残金
procedure TfrmPUrikakeDaichou.BitBtn5Click(Sender: TObject);
begin
	PrintSG(0);
end;

//
//iFlag = 0  -> 通常表示
//iFlag = 1  -> 折りたたみ表示
//                Ver2.0 2000/03/03 返金も出るように変更
//
//              適用が消費税か金額がマイナスのもののみ表示する
//
procedure TfrmPUrikakeDaichou.PrintSG(iFlag:integer);
var
	sSql, sTokuisakiCode, sDateTO:String;
  sDateFrom : String;
  iZandaka : Integer;
  wYyyy, wMm, wDd:Word;
const
	CsDateFrom = '1998/12/01'; //残高計算対象期間
begin
  ClearSG;

	//締め日の取得
  GsShimebi := MTokuisaki.GetShimebi(EditTokuisakiCode.Text);

	//表示情報の取得
	sSql := 'SELECT * ';
  sSql := sSql + ' FROM ';
  sSql := sSql + CtblTDenpyou;
  sSql := sSql + ' WHERE ';
  sSql := sSql + 'TokuisakiCode1 = ' +  EditTokuisakiCode.Text;
  sSql := sSql + ' AND ';
  sSql := sSql + 'DDate Between ';

  //2000/05/16
  sSql := sSql + '''' + DateToStr(DTPFrom.Date) + '''';

  sSql := sSql + ' AND ';
  sSql := sSql + '''' + DateToStr(DTPTo.Date) + '''';
  sSql := sSql + ' ORDER BY DDate, ID';

  GiRow := 1;
  GiKungakuSum := 0;
  GiTax := 0;
  GbFlag := False;

  //表示期間の前日までの残高を求める
  //この間数の中でグリッドに表示もする
  //iZandaka := GetZandaka(EditTokuisakiCode.Text, '1999/01/01', DateToStr(DTPFrom.Date - 1));
  //締め日の翌月から
  DecodeDate(DTPFrom.Date, wYyyy, wMm, wDd);
  wDd := StrToInt(MTokuisaki.GetShimebi(EditTokuisakiCode.Text));
  //sDdをその月の月末日の翌日に変換する
  if wDd = CSeikyuuSimebi_End then begin
  	//wDd := HKLib.GetGetsumatsu(wYyyy, wMm);
    wDd := 1; //月末の翌日は必ず１日
    wMm := wMm + 1;
    if wMm = 13 then begin
    	wMm := 1;
      wYyyy := wYyyy + 1;
    end;
  end else begin
	  wDd := wDd + 1;
  end;

  if EnCodeDate(wYyyy,wMm,wDd) > DTPFrom.Date then begin
  	//１ヶ月戻す
    wMm := wMm - 1;
    if wMm = 0 then begin
    	wMm := 12;
      wYyyy := wyyyy-1;
    end;//of if
  end;

  //2000/05/16
  //iZandaka := GetZandaka(EditTokuisakiCode.Text, DateToStr(EnCodeDate(wYyyy,wMm,wDd)), DateToStr(DTPFrom.Date - 1));
  iZandaka := GetZandaka(EditTokuisakiCode.Text, CsDateFrom, DateToStr(DTPFrom.Date - 1));

  GiRow := GiRow + 1;
  GiKungakuSum := iZandaka;

  with frmDMMaster.QueryUDaichou do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    While not EOF do begin
	    MakeSG2(frmDMMaster.QueryUDaichou, iFlag);
	   	Next;
	    GiRow := GiRow + 1;
    end;
    Close;
  end;//of with

end;



//折りたたみ表示
//適用が消費税か金額がマイナスのもののみ表示する
procedure TfrmPUrikakeDaichou.BitBtn6Click(Sender: TObject);
begin
	PrintSG(1);
end;

procedure TfrmPUrikakeDaichou.FormResize(Sender: TObject);
var
	iHeight:Integer;
begin
  inherited;
	iHeight := Height;
  SGUDaichou.Height := iHeight-panel1.Height-panel2.Height-panel3.Height-25;
end;

end.
