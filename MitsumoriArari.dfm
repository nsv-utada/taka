object frmMitsumoriArari: TfrmMitsumoriArari
  Left = 410
  Top = 254
  Width = 755
  Height = 287
  Caption = 'frmMitsumoriArari'
  Color = clBtnFace
  Font.Charset = SHIFTJIS_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Shape1: TShape
    Left = 8
    Top = 72
    Width = 473
    Height = 137
    Shape = stRoundRect
  end
  object Label4: TLabel
    Left = 263
    Top = 96
    Width = 17
    Height = 16
    Caption = #65374
    Color = clCream
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label5: TLabel
    Left = 51
    Top = 100
    Width = 46
    Height = 16
    Caption = #24180#12288#26376
    Color = clCream
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label6: TLabel
    Left = 51
    Top = 151
    Width = 51
    Height = 16
    Caption = #31895#21033#29575
    Color = clCream
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label7: TLabel
    Left = 180
    Top = 147
    Width = 17
    Height = 16
    Caption = #65374
    Color = clCream
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label8: TLabel
    Left = 80
    Top = 120
    Width = 188
    Height = 13
    Caption = #26908#32034#12375#12383#12356#24180#26376#12434#36984#25246#12375#12390#19979#12373#12356#12290
    Color = clBtnFace
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clMenuText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label9: TLabel
    Left = 80
    Top = 171
    Width = 240
    Height = 13
    Caption = #26908#32034#12375#12383#12356#31895#21033#29575#12398#31684#22258#12434#20837#21147#12375#12390#19979#12373#12356#12290
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 80
    Top = 185
    Width = 383
    Height = 13
    Caption = #20309#12418#20837#21147#12375#12394#12363#12387#12383#22580#21512#12289#20840#12390#12398#12354#31895#21033#29575#12398#35211#31309#12426#12364#23550#35937#12392#12394#12426#12414#12377#12290
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = []
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 728
    Height = 65
    Align = alTop
    Color = clTeal
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 10
      Width = 120
      Height = 19
      Alignment = taCenter
      Caption = #35211#31309#31895#21033#26908#32034
      Color = clTeal
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object Label3: TLabel
      Left = 25
      Top = 36
      Width = 251
      Height = 13
      AutoSize = False
      Caption = #40441#26494#23627#22770#25499#31649#29702#12471#12473#12486#12512' Ver5.00'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clSilver
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object DTPFrom: TDateTimePicker
    Left = 121
    Top = 92
    Width = 139
    Height = 24
    Date = 36275.451878877310000000
    Time = 36275.451878877310000000
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
  object DTPTo: TDateTimePicker
    Left = 289
    Top = 92
    Width = 139
    Height = 24
    Date = 36275.451878877310000000
    Time = 36275.451878877310000000
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object SB1: TStatusBar
    Left = 0
    Top = 250
    Width = 728
    Height = 19
    Panels = <>
    SimplePanel = True
  end
  object EditArariFrom: TEdit
    Left = 123
    Top = 143
    Width = 54
    Height = 24
    BiDiMode = bdLeftToRight
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 4
    OnKeyPress = EditArariFromKeyPress
  end
  object Panel2: TPanel
    Left = 0
    Top = 209
    Width = 728
    Height = 41
    Align = alBottom
    Color = clTeal
    TabOrder = 5
    object BtnClose: TBitBtn
      Left = 280
      Top = 8
      Width = 89
      Height = 25
      Caption = #38281#12376#12427'(&Z)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnCloseClick
      Kind = bkIgnore
    end
    object BitBtn3: TBitBtn
      Left = 94
      Top = 8
      Width = 155
      Height = 25
      Caption = #12456#12463#12475#12523#12408#20986#21147'(&E)'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn3Click
      Kind = bkRetry
    end
  end
  object EditArariTo: TEdit
    Left = 203
    Top = 143
    Width = 54
    Height = 24
    BiDiMode = bdLeftToRight
    Font.Charset = SHIFTJIS_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
    Font.Style = [fsBold]
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 6
    OnKeyPress = EditArariFromKeyPress
  end
end
