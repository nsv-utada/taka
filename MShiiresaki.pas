unit MShiiresaki;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, DBTables,ShellAPI;

type
  TfrmMShiiresaki = class(TfrmMaster)
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    CBShiiresakiCode: TComboBox;
    //CBYomi: TComboBox;
    CBName: TComboBox;
    Label37: TLabel;
    CBTel: TComboBox;
    Label39: TLabel;
    CBFax: TComboBox;
    Label17: TLabel;
    CBAdd1: TComboBox;
    Label16: TLabel;
    CBZip: TComboBox;
    Label19: TLabel;
    CBAdd2: TComboBox;
    Query1: TQuery;
    BitBtn3: TBitBtn;
    Label4: TLabel;
    CBName2: TComboBox;
    Label7: TLabel;
    EditTantou: TEdit;
    Label8: TLabel;
    EditDaihyousha: TEdit;
    Label9: TLabel;
    EditNaiyou: TEdit;
    Label10: TLabel;
    MemoMemo: TMemo;
    BitBtn4: TBitBtn;
    Panel4: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    CBShimebi: TComboBox;
    CBShiharaibi: TComboBox;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    CBShiharaiHouhou: TComboBox;
    CBSite: TComboBox;
    CBTax: TComboBox;
    PageControl1: TPageControl;
    tsOffice: TTabSheet;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label32: TLabel;
    NounyuuNissuu: TComboBox;
    Shimejikan: TComboBox;
    ChuumonHouhou: TComboBox;
    tsHome: TTabSheet;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label18: TLabel;
    Label38: TLabel;
    Label20: TLabel;
    Label43: TLabel;
    eAdd3: TComboBox;
    eAdd1: TComboBox;
    eZip: TComboBox;
    eAdd2: TComboBox;
    eName: TEdit;
    TabSheet2: TTabSheet;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label61: TLabel;
    YobiStr1: TComboBox;
    YobiStr3: TComboBox;
    YobiStr2: TComboBox;
    YobiStr4: TComboBox;
    TabSheet3: TTabSheet;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    YobiInt1: TEdit;
    YobiInt2: TEdit;
    YobiInt3: TEdit;
    YobiInt4: TEdit;
    Label21: TLabel;
    Label22: TLabel;
    ShukkaKeitai: TComboBox;
    SaiteiShukkaRotto: TEdit;
    eTantousha1: TEdit;
    eShozoku1: TEdit;
    Label23: TLabel;
    TaJouken: TMemo;
    EditYomi: TEdit;
    Label24: TLabel;
    eBikou1: TEdit;
    eShozoku2: TEdit;
    eTantousha2: TEdit;
    eBikou2: TEdit;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label31: TLabel;
    eShozoku3: TEdit;
    Label33: TLabel;
    eTantousha3: TEdit;
    Label40: TLabel;
    eBikou3: TEdit;
    CBnon: TCheckBox;
    CBMon: TCheckBox;
    CBTue: TCheckBox;
    CBWed: TCheckBox;
    CBThu: TCheckBox;
    CBFri: TCheckBox;
    CBSat: TCheckBox;
    CBSan: TCheckBox;
    BitBtn5: TBitBtn;
    CBTT: TComboBox;
    BitBtn6: TBitBtn;
    QueryLabel: TQuery;
    procedure FormCreate(Sender: TObject);
    procedure CBShiiresakiCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameExit(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CBNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EditYomiKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
		procedure MakeForm(sCode:String);
		procedure InsertShiiresaki();
		procedure UpdateShiiresaki(sCode:String);
		procedure ClearForm;
    function MakeYoubiShiteiNouhin : String;
  public
    { Public 宣言 }
  end;

var
  frmMShiiresaki: TfrmMShiiresaki;

implementation
uses
	Inter, DM1, PasswordDlg, DMMaster,HKLib,ShiireDaichou;

{$R *.DFM}

procedure TfrmMShiiresaki.FormCreate(Sender: TObject);
begin
  inherited;
  width  := 650;
  height := 640;

end;

procedure TfrmMShiiresaki.CBShiiresakiCodeKeyDown(Sender: TObject;
var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBShiiresakiName('Code', CBShiiresakiCode.Text, 2);
  end;
end;

{
	仕入先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmMShiiresaki.MakeCBShiiresakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  //sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('Code').AsString + ',' + FieldByName('Name').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBName.DroppedDown := True;
end;


procedure TfrmMShiiresaki.CBNameExit(Sender: TObject);
var
	sCode : String;
begin
	//Codeを取得して仕入先を検索する
  sCode := Copy(CBName.Text, 1, (pos(',', CBName.Text)-1));
  if length(sCode) > 0 then begin
	  MakeForm(sCode);
  end;
end;

//Codeからフォームを表示する
procedure TfrmMShiiresaki.MakeForm(sCode:String);
var
	sSql,strYoubiShiteiNouhin : String;
  i : Integer;
begin
	sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + ' Code = ''' + sCode + '''';
  with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    if not EOF then begin
			EditYomi.Text  := FieldbyName('Yomi').AsString;;
			CBName2.Text := FieldbyName('Name2').AsString;;
			CBTel.Text   := FieldbyName('Tel').AsString;;
			CBFax.Text   := FieldbyName('Fax').AsString;;
			CBZip.Text := FieldbyName('Zip').AsString;;
			CBAdd1.Text := FieldbyName('Add1').AsString;;
			CBAdd2.Text := FieldbyName('Add2').AsString;;
			MemoMemo.Text := FieldbyName('Memo').AsString;;
			EditTantou.Text := FieldbyName('Tantou').AsString;;
			EditDaihyousha.Text := FieldbyName('Daihyousha').AsString;;
			EditNaiyou.Text := FieldbyName('Naiyou').AsString;;
      CBShiiresakiCode.Text := sCode;

      //2004.07.22
			CBName.Text := FieldbyName('Name').AsString;;

      //2006.03.03
      ChuumonHouhou.Text := FieldbyName('ChuumonHouhou').AsString; //SMALLINT,
      Shimejikan.Text := FieldbyName('Shimejikan').AsString;       //char(5),
      NounyuuNissuu.Text := FieldbyName('NounyuuNissuu').AsString; //SMALLINT,

      strYoubiShiteiNouhin := FieldbyName('YoubiShiteiNouhin').AsString; // VARCHAR(7),
      if pos('N',strYoubiShiteiNouhin)>0 then begin
        CBnon.Checked := True;
      end;
      if pos('M',strYoubiShiteiNouhin)>0 then begin
        CBMon.Checked := True;
      end;
      if pos('T',strYoubiShiteiNouhin)>0 then begin
        CBTue.Checked := True;
      end;
      if pos('W',strYoubiShiteiNouhin)>0 then begin
        CBWed.Checked := True;
      end;
      if pos('H',strYoubiShiteiNouhin)>0 then begin
        CBThu.Checked := True;
      end;
      if pos('F',strYoubiShiteiNouhin)>0 then begin
        CBFri.Checked := True;
      end;
      if pos('S',strYoubiShiteiNouhin)>0 then begin
        CBSat.Checked := True;
      end;
      if pos('Z',strYoubiShiteiNouhin)>0 then begin
        CBSan.Checked := True;
      end;
      SaiteiShukkaRotto.Text := FieldbyName('SaiteiShukkaRotto').AsString; // SMALLINT,
      CBTT.Text := FieldbyName('SaiteiShukkaTani').AsString;

      ShukkaKeitai.Text := FieldbyName('ShukkaKeitai').AsString; // SMALLINT,
      TaJouken.Text := FieldbyName('TaJouken').AsString;         // VARCHAR(800),
      eZip.Text := FieldbyName('eZip').AsString;   // VARCHAR(12),
      eAdd1.Text := FieldbyName('eAdd1').AsString; // VARCHAR(12),
      eAdd2.Text := FieldbyName('eAdd2').AsString; // VARCHAR(200),
      eAdd3.Text := FieldbyName('eAdd3').AsString; // VARCHAR(200),
      eName.Text := FieldbyName('eName').AsString; // VARCHAR(200),
      eShozoku1.Text := FieldbyName('eShozoku1').AsString;     // VARCHAR(200),
      eTantousha1.Text := FieldbyName('eTantousha1').AsString; // VARCHAR(200),
      eBikou1.Text := FieldbyName('eBikou1').AsString;     // VARCHAR(800),
      eShozoku2.Text := FieldbyName('eShozoku2').AsString; // VARCHAR(200),
      eTantousha2.Text := FieldbyName('eTantousha2').AsString; // VARCHAR(200),
      eBikou2.Text := FieldbyName('eBikou2').AsString;         // VARCHAR(800),
      eShozoku3.Text := FieldbyName('eShozoku3').AsString;     // VARCHAR(200),
      eTantousha3.Text := FieldbyName('eTantousha3').AsString; // VARCHAR(200),
      eBikou3.Text := FieldbyName('eBikou3').AsString;   // VARCHAR(800),
      YobiStr1.Text := FieldbyName('YobiStr1').AsString; // VARCHAR(400),
      YobiStr2.Text := FieldbyName('YobiStr2').AsString; // VARCHAR(400),
      YobiStr3.Text := FieldbyName('YobiStr3').AsString; // VARCHAR(400),
      YobiStr4.Text := FieldbyName('YobiStr4').AsString; // VARCHAR(400),
      YobiInt1.Text := FieldbyName('YobiInt1').AsString; // int,
      YobiInt2.Text := FieldbyName('YobiInt2').AsString; // int,
      YobiInt3.Text := FieldbyName('YobiInt3').AsString; // int,
      YobiInt4.Text := FieldbyName('YobiInt4').AsString; // int

      //2006.04.20
      CBShimebi.Text := FieldbyName('Shimebi').AsString;
      CBShiharaibi.Text := FieldbyName('Shiharaibi').AsString;
      CBSite.Text := FieldbyName('Site').AsString;
      CBTax.Text := FieldbyName('CTax').AsString;

      //2006.04.24
      CBShiharaiHouhou.Text := FieldbyName('ShiharaiHouhou').AsString;

    end;
    Close;
  end;
end;

procedure TfrmMShiiresaki.BitBtn3Click(Sender: TObject);
var
	sName, sCode, sWhere : String;
begin
	//データチェック
  if CBTel.Text = '' then begin
  	ShowMessage('電話番号は省略できません');
    CBTel.SetFocus;
    Exit;
  end;
  if CBFax.Text = '' then begin
  	ShowMessage('Fax番号は省略できません');
    CBFax.SetFocus;
    Exit;
  end;

	//２重登録の確認
  sCode := CBShiiresakiCode.Text;
  sWhere := ' where Code=' + sCode;
  sName := DM1.Check2Juu3(CtblMSHiiresaki,'Name', sWhere);

  try
	  if sName = '' then begin
			//確認メッセージ
			if MessageDlg('新規データです。登録しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		InsertShiiresaki();
	  end else begin
			//確認メッセージ
			if MessageDlg('既存データです。修正しますか?',
		    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
		    Exit;
		  end;
  		UpdateShiiresaki(sCode);
	  end;
  	ShowMessage('登録に成功しました');
  except
	  on E: EDBEngineError do begin
    	ShowMessage(E.Message);
		  ShowMessage('登録に失敗しました');
    end;
  end;

end;

//仕入先マスターの新規登録
//2001/11/19
procedure TfrmMShiiresaki.InsertShiiresaki();
var
  sTmp, sSql, strYoubiShiteiNouhin : String;
  iTemp : Integer;
begin
	sSql := 'INSERT INTO ' + CtblMShiiresaki;
  sSql := sSql + ' (';
  sSql := sSql + ' Code ';
  sSql := sSql + ' ,Name2';
  sSql := sSql + ' ,Name';
  sSql := sSql + ' ,Yomi';
  sSql := sSql + ' ,Zip';
  sSql := sSql + ' ,Add1';
  sSql := sSql + ' ,Add2';
  sSql := sSql + ' ,Tel';
  sSql := sSql + ' ,Fax';
  sSql := sSql + ' ,Tantou';
  sSql := sSql + ' ,Daihyousha';
  sSql := sSql + ' ,Naiyou';
  sSql := sSql + ' ,Memo';
  //2006.03.03
  sSql := sSql + ' ,ChuumonHouhou';
  sSql := sSql + ' ,Shimejikan';
  sSql := sSql + ' ,NounyuuNissuu';
  sSql := sSql + ' ,YoubiShiteiNouhin';

  sSql := sSql + ' ,SaiteiShukkaRotto';
  sSql := sSql + ' ,SaiteiShukkaTani';

  sSql := sSql + ' ,ShukkaKeitai';
  sSql := sSql + ' ,TaJouken';
  sSql := sSql + ' ,eZip';
  sSql := sSql + ' ,eAdd1';
  sSql := sSql + ' ,eAdd2';
  sSql := sSql + ' ,eAdd3';
  sSql := sSql + ' ,eName';
  sSql := sSql + ' ,eShozoku1';
  sSql := sSql + ' ,eTantousha1';
  sSql := sSql + ' ,eBikou1';
  sSql := sSql + ' ,eShozoku2';
  sSql := sSql + ' ,eTantousha2';
  sSql := sSql + ' ,eBikou2';
  sSql := sSql + ' ,eShozoku3';
  sSql := sSql + ' ,eTantousha3';
  sSql := sSql + ' ,eBikou3';
  sSql := sSql + ' ,YobiStr1';
  sSql := sSql + ' ,YobiStr2';
  sSql := sSql + ' ,YobiStr3';
  sSql := sSql + ' ,YobiStr4';
  sSql := sSql + ' ,YobiInt1';
  sSql := sSql + ' ,YobiInt2';
  sSql := sSql + ' ,YobiInt3';
  sSql := sSql + ' ,YobiInt4';

  //2006.04.20
  sSql := sSql + ' ,Shimebi';
  sSql := sSql + ' ,Shiharaibi';
  sSql := sSql + ' ,Site';
  sSql := sSql + ' ,CTax';

  //2006.04.24
  sSql := sSql + ' ,ShiharaiHouhou';

  sSql := sSql + ')';
  sSql := sSql + ' VALUES ';
  sSql := sSql + ' (';
  sSql := sSql + '' + CBShiiresakiCode.Text     + ',' ;
  sSql := sSql + '''' + CBName2.Text  + ''',' ;
  sSql := sSql + '''' + CBName.Text   + ''',' ;
  sSql := sSql + '''' + EditYomi.Text + ''',' ;
  sSql := sSql + '''' + CBZip.Text    + ''',' ;
  sSql := sSql + '''' + CBAdd1.Text   + ''',' ;
  sSql := sSql + '''' + CBAdd2.Text   + ''',' ;
  sSql := sSql + '''' + CBTel.Text    + ''',' ;
  sSql := sSql + '''' + CBFax.Text    + ''',' ;
  sSql := sSql + '''' + EditTantou.Text     + ''',' ;
  sSql := sSql + '''' + EditDaihyousha.Text + ''',' ;
  sSql := sSql + '''' + EditNaiyou.Text     + ''',' ;
  sSql := sSql + '''' + MemoMemo.Text       + ''',' ;
  //2006.03.03
  sSql := sSql + '''' + Copy(ChuumonHouhou.Text,0,1) + ''',' ;
  sSql := sSql + '''' + Shimejikan.Text + ''',' ;
  sSql := sSql + '''' + Copy(NounyuuNissuu.Text,0,1) + ''',' ;

  //曜日指定納品
  strYoubiShiteiNouhin := MakeYoubiShiteiNouhin;
  sSql := sSql + '''' + strYoubiShiteiNouhin + ''',' ; // VARCHAR(7),
  //最低出荷ロット　最低出荷単位
  sSql := sSql +        SaiteiShukkaRotto.Text + ',' ; // SMALLINT,
  sSql := sSql + '''' + CBTT.Text + ''',';

  sSql := sSql + '''' + Copy(ShukkaKeitai.Text,0,1) + ''',' ; // SMALLINT,
  sSql := sSql + '''' + TaJouken.Text    + ''',' ;     // VARCHAR(800),
  sSql := sSql + '''' + eZip.Text        + ''',' ;  //VARCHAR(12),
  sSql := sSql + '''' + eAdd1.Text       + ''',' ; //VARCHAR(12),
  sSql := sSql + '''' + eAdd2.Text       + ''',' ; //VARCHAR(200),
  sSql := sSql + '''' + eAdd3.Text       + ''',' ; //VARCHAR(200),
  sSql := sSql + '''' + eName.Text       + ''',' ; //VARCHAR(200),
  sSql := sSql + '''' + eShozoku1.Text   + ''',' ; //  VARCHAR(200),
  sSql := sSql + '''' + eTantousha1.Text + ''',' ; // VARCHAR(200),
  sSql := sSql + '''' + eBikou1.Text     + ''',' ; //     VARCHAR(800),
  sSql := sSql + '''' + eShozoku2.Text   + ''',' ; //   VARCHAR(200),
  sSql := sSql + '''' + eTantousha2.Text + ''',' ; // VARCHAR(200),
  sSql := sSql + '''' + eBikou2.Text     + ''',' ; //     VARCHAR(800),
  sSql := sSql + '''' + eShozoku3.Text   + ''',' ; //   VARCHAR(200),
  sSql := sSql + '''' + eTantousha3.Text + ''',' ; // VARCHAR(200),
  sSql := sSql + '''' + eBikou3.Text     + ''',' ;     // VARCHAR(800),
  sSql := sSql + '''' + YobiStr1.Text    + ''',' ;    // VARCHAR(400),
  sSql := sSql + '''' + YobiStr2.Text    + ''',' ;    // VARCHAR(400),
  sSql := sSql + '''' + YobiStr3.Text    + ''',' ;    // VARCHAR(400),
  sSql := sSql + '''' + YobiStr4.Text    + ''',' ;    // VARCHAR(400),
  sSql := sSql        + YobiInt1.Text    + ',' ;    // int,
  sSql := sSql        + YobiInt2.Text    + ',' ;    // int,
  sSql := sSql        + YobiInt3.Text    + ',' ;    // int,
  sSql := sSql        + YobiInt4.Text    + ',' ;    // int

  //2006.04.20
  sTmp := Copy(CBShimebi.Text,0,(Pos(',',CBShimebi.Text)-1));
  if sTmp = '' then sTmp := '0';
  sSql := sSql + sTmp + ',';

  sTmp := Copy(CBShiharaibi.Text,0,(Pos(',',CBShiharaibi.Text)-1));
  if sTmp = '' then sTmp := '0';
  sSql := sSql + sTmp + ',';

  sTmp := CBSite.text;
  if sTmp = '' then sTmp := '0';
  sSql := sSql + sTmp + ',';

  sSql := sSql + '''' + CBTax.Text + ''',' ;    // int

  //2006.04.24
  //支払方法
  sSql := sSql + '''' + CBSHiharaiHouhou.Text + '''' ;    // int

  sSql := sSql + ')';

  with Query1 do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with
end;

//得意先マスターの更新
procedure TfrmMShiiresaki.UpdateShiiresaki(sCode:String);
var
	sSql,strYoubiShiteiNouhin,sTmp : String;
  iTemp : Integer;
begin
	sSql := 'UPDATE ' + CtblMShiiresaki;
  sSql := sSql + ' SET ';
  sSql := sSql + 'Name        = ''' + CBName.Text  + '''';
  sSql := sSql + ',Yomi       = ''' + EditYomi.Text + '''';
  sSql := sSql + ',Name2      = ''' + CBName2.Text + '''';
  sSql := sSql + ',Tel        = ''' + CBTel.Text + '''';
  sSql := sSql + ',Fax        = ''' + CBFax.Text + '''';
  sSql := sSql + ',Zip        = ''' + CBZip.Text + '''';
  sSql := sSql + ',Add1       = ''' + CBAdd1.Text + '''';
  sSql := sSql + ',Add2       = ''' + CBAdd2.Text + '''';
  sSql := sSql + ',Tantou     = ''' + EditTantou.Text + '''';
  sSql := sSql + ',Daihyousha = ''' + EditDaihyousha.Text + '''';
  sSql := sSql + ',Naiyou     = ''' + EditNaiyou.Text + '''';
  sSql := sSql + ',Memo       = ''' + MemoMemo.Text + '''';
  //2006.03.03
  sSql := sSql + ',ChuumonHouhou    = ''' + Copy(ChuumonHouhou.Text,0,1) + ''''; //SMALLINT,
  sSql := sSql + ',Shimejikan       = ''' + Shimejikan.Text + '''';    // char(5),
  sSql := sSql + ',NounyuuNissuu    = ''' + Copy(NounyuuNissuu.Text,0,1) + ''''; // SMALLINT,
  //曜日指定納品
  strYoubiShiteiNouhin := MakeYoubiShiteiNouhin;

  sSql := sSql + ',YoubiShiteiNouhin = ''' + strYoubiShiteiNouhin + ''''; // VARCHAR(7),
  sSql := sSql + ',SaiteiShukkaRotto = ''' + SaiteiShukkaRotto.Text + ''''; // SMALLINT,
  sSql := sSql + ',SaiteiShukkaTani = ''' + CBTT.Text + '''';

  sSql := sSql + ',ShukkaKeitai      = ''' + Copy(ShukkaKeitai.Text,0,1) + '''';      // SMALLINT,
  sSql := sSql + ',TaJouken   = ''' + TaJouken.Text + '''';  // VARCHAR(800),
  sSql := sSql + ',eZip       = ''' + eZip.Text + '''';      // VARCHAR(12),
  sSql := sSql + ',eAdd1      = ''' + eAdd1.Text + '''';     // VARCHAR(12),
  sSql := sSql + ',eAdd2      = ''' + eAdd2.Text + '''';     // VARCHAR(200),
  sSql := sSql + ',eAdd3      = ''' + eAdd3.Text + '''';     // VARCHAR(200),
  sSql := sSql + ',eName      = ''' + eName.Text + '''';     // VARCHAR(200),
  sSql := sSql + ',eShozoku1  = ''' + eShozoku1.Text + ''''; //   VARCHAR(200),
  sSql := sSql + ',eTantousha1 = ''' + eTantousha1.Text + ''''; // VARCHAR(200),
  sSql := sSql + ',eBikou1     = ''' + eBikou1.Text + '''';     // VARCHAR(800),
  sSql := sSql + ',eShozoku2   = ''' + eShozoku2.Text + '''';   // VARCHAR(200),
  sSql := sSql + ',eTantousha2 = ''' + eTantousha2.Text + ''''; // VARCHAR(200),
  sSql := sSql + ',eBikou2     = ''' + eBikou2.Text + '''';     // VARCHAR(800),
  sSql := sSql + ',eShozoku3   = ''' + eShozoku3.Text + '''';   // VARCHAR(200),
  sSql := sSql + ',eTantousha3 = ''' + eTantousha3.Text + ''''; // VARCHAR(200),
  sSql := sSql + ',eBikou3     = ''' + eBikou3.Text + '''';  // VARCHAR(800),
  //締め日、支払日
  sTmp := Copy(CBShimebi.Text,0,pos(',',CBShimebi.Text)-1);
   if sTmp = '' then begin
      if length(CBShimebi.Text) >0 then begin
        sTmp := CBShimebi.Text;
      end else begin
       sTmp := '0';
      end;
   end;

  sSql := sSql + ',Shimebi     = ''' + sTmp + '''';

  sTmp := Copy(CBShiharaibi.Text,0,pos(',',CBShiharaibi.Text)-1);
   if sTmp = '' then begin
      if length(CBShiharaibi.Text) >0 then begin
        sTmp := CBShiharaibi.Text;
      end else begin
       sTmp := '0';
      end;
   end;

  sSql := sSql + ',Shiharaibi    = ''' + sTmp + '''';



  //予備
  sSql := sSql + ',YobiStr1    = ''' + YobiStr1.Text + ''''; // VARCHAR(400),
  sSql := sSql + ',YobiStr2    = ''' + YobiStr2.Text + ''''; // VARCHAR(400),
  sSql := sSql + ',YobiStr3    = ''' + YobiStr3.Text + ''''; // VARCHAR(400),
  sSql := sSql + ',YobiStr4    = ''' + YobiStr4.Text + ''''; // VARCHAR(400),
  sSql := sSql + ',YobiInt1    = ''' + YobiInt1.Text + ''''; // int,
  sSql := sSql + ',YobiInt2    = ''' + YobiInt2.Text + ''''; // int,
  sSql := sSql + ',YobiInt3    = ''' + YobiInt3.Text + ''''; // int,
  sSql := sSql + ',YobiInt4    = ''' + YobiInt4.Text + ''''; // int

  //2006.04.20
  //サイト、消費税

  sTmp := CBSite.text;
  sSql := sSql + ',Site = ''' + sTmp + '''';

  sSql := sSql + ',CTax = ''' + CBTax.Text + '''';

  //2006.04.24
  sSql := sSql + ',ShiharaiHouhou = ''' + CBShiharaiHouhou.Text + '''';



	sSql := sSql + ' WHERE Code = ''' + sCode + '''';

  with Query1 do begin
    Close;
		Sql.Clear;
    Sql.Add(sSql);
		ExecSql;
    Close;
  end;//of with
end;

//曜日指定納品
function TfrmMShiiresaki.MakeYoubiShiteiNouhin : String;
var
 sTmp : String;
begin
  if CBnon.Checked = True then begin
    sTmp := 'N'
  end;
  if CBMon.Checked = True then begin
    sTmp := sTmp + 'M'
  end;
  if CBTue.Checked = True then begin
    sTmp := sTmp + 'T'
  end;
  if CBWed.Checked = True then begin
    sTmp := sTmp + 'W'
  end;
  if CBThu.Checked = True then begin
    sTmp := sTmp + 'H'
  end;
  if CBFri.Checked = True then begin
    sTmp := sTmp + 'F'
  end;
  if CBSat.Checked = True then begin
    sTmp := sTmp + 'S'
  end;
  if CBSan.Checked = True then begin
    sTmp := sTmp + 'Z'
  end;
  Result := sTmp
end;

procedure TfrmMShiiresaki.CBNameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBShiiresakiName('Name', CBName.Text, 1);
  end;
end;

//新規登録ボタン
procedure TfrmMShiiresaki.BitBtn4Click(Sender: TObject);
begin
//	ClearForm;
  GShiiresakiCode := CBShiiresakiCode.Text;
  TfrmShiireDaichou.Create(Self);
end;

procedure TfrmMShiiresaki.ClearForm;
begin
  CBShiiresakiCode.Text := '';
  CBName.Text := '';
  CBName2.Text := ''; //略称
  CBTel.Text := '';
  CBFax.Text := '';
  CBZip.Text := '';
  CBAdd1.Text := '';
  CBAdd2.Text := '';
  EditTantou.Text := '';
  EditDaihyousha.Text := '';
  EditNaiyou.Text := '';
  MemoMemo.Text := '';

  //2006.03.25
  ChuumonHouhou.Text := '';
  Shimejikan.Text := '';
  NounyuuNissuu.Text := '';
  CBnon.Checked := False;
  CBMon.Checked := False;
  CBTue.Checked := False;
  CBWed.Checked := False;
  CBThu.Checked := False;
  CBFri.Checked := False;
  CBSat.Checked := False;
  CBSan.Checked := False;
  SaiteiShukkaRotto.Text := '';
  ShukkaKeitai.text := '';
  ShukkaKeitai.Text := '';
  TaJouken.Text := '';
  EditYomi.Text := '';

  eZip.Text := '';
  eName.Text := '';
  eAdd1.Text := '';
  eAdd2.Text := '';
  eAdd3.Text := '';
  eShozoku1.Text := '';
  eTantousha1.Text := '';
  eBikou1.Text := '';
  eShozoku2.Text := '';
  eTantousha2.Text := '';
  eBikou2.Text := '';
  eShozoku3.Text := '';
  eTantousha3.Text := '';
  eBikou3.Text := '';
  YobiStr1.Text := '';
  YobiStr2.Text := '';
  YobiStr3.Text := '';
  YobiStr4.Text := '';
  YobiInt1.Text := '';
  YobiInt2.Text := '';
  YobiInt3.Text := '';
  YobiInt4.Text := '';

  //2006.04.24
  CBShiharaiHouhou.text := '';
end;

procedure TfrmMShiiresaki.BitBtn2Click(Sender: TObject);
var
  sSql, sLine, strMemo, sAdd3 : String;
  F : TextFile;
  i : integer;
begin
//2002.12.28
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
   //コメントアウト 2009.04.08 utada
	//end else if GPassWord = CPassword2 then begin
  //  Beep;
  //ここまで
	end else begin
	//ShowMessage('PassWordが違います');
   exit;
	end;
	GPassWord := '';



	//確認メッセージ
  if MessageDlg('仕入先一覧を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMShiiresaki;

  sSql := sSql + ' ORDER BY Code';

  //出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Tokuisaki);
  Rewrite(F);
  CloseFile(F);

  //作成日
  sLine := DateToStr(Date);
  HMakeFile(CFileName_Tokuisaki, sLine);

  //タイトルを出力する
  sLine := '仕入先コード';
  sLine := sLine + ',' + '仕入先先名';
  sLine := sLine + ',' + '仕入先先名';
  sLine := sLine + ',' + 'よみ';
  sLine := sLine + ',' + '郵便番号';
  sLine := sLine + ',' + '住所1';
  sLine := sLine + ',' + '住所2';
  sLine := sLine + ',' + '電話';
  sLine := sLine + ',' + 'Fax';
  sLine := sLine + ',' + '担当者';
  sLine := sLine + ',' + '代表者';
  sLine := sLine + ',' + '内容';

  //2006.05.27
  //sLine := sLine + ',' + 'メモ';

  //2006.03.03
  sLine := sLine + ',' + '注文方法';  //　　電話　　int
  sLine := sLine + ',' + '締め時間';  //　10:00　 time　
  sLine := sLine + ',' + '納入日数';  //    当日　　int
  sLine := sLine + ',' + '曜日指定納品';  //　月から日まで複数選択
  sLine := sLine + ',' + '最低出荷ロット';  //　　int
  sLine := sLine + ',' + '出荷形態';  //　　　　　int

  //2006.05.27
  //sLine := sLine + ',' + '他（条件）';  //　　　　Varchar 400

  //　・営業所
  sLine := sLine + ',' + '郵便番号';  //
  sLine := sLine + ',' + '都道府県';  //
  sLine := sLine + ',' + '市町村';  //
  sLine := sLine + ',' + '番地';  //
  sLine := sLine + ',' + '営業所名';  //
  sLine := sLine + ',' + '所属1';  //　　　　Varchar
  sLine := sLine + ',' + '担当者名1';  //　　Varchar
  sLine := sLine + ',' + '備考1';  //　　　　Varchar
  sLine := sLine + ',' + '所属2';  //　　　　Varchar
  sLine := sLine + ',' + '担当者名2';  //　　Varchar
  sLine := sLine + ',' + '備考2';  //　　　　Varchar
  sLine := sLine + ',' + '所属3';  //　　　　Varchar
  sLine := sLine + ',' + '担当者名3';  //　　Varchar
  sLine := sLine + ',' + '備考3';  //　　　　Varchar
  //  ・予備（文字）
  sLine := sLine + ',' + '予備文字1';  //
  sLine := sLine + ',' + '予備文字2';  //
  sLine := sLine + ',' + '予備文字3';  //
  sLine := sLine + ',' + '予備文字4';  //
  //　・予備（数字）
  sLine := sLine + ',' + '予備数字1';  //
  sLine := sLine + ',' + '予備数字2';  //
  sLine := sLine + ',' + '予備数字3';  //
  sLine := sLine + ',' + '予備数字4';  //
  //2006.11.16
  //〆日、支払日、支払い方法、サイトを出力
  sLine := sLine + ',' + '〆日';  //
  sLine := sLine + ',' + '支払日';  //
  sLine := sLine + ',' + '支払い方法';  //
  sLine := sLine + ',' + 'サイト';  //



  HMakeFile(CFileName_Tokuisaki, sLine);


  //ファイルへ書き込み
  with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 1;
    while not EOF do begin
    	sLine := FieldByName('Code').AsString;
    	sLine := sLine + ',' + FieldByName('Name2').AsString;
    	sLine := sLine + ',' + FieldByName('Name').AsString;
    	sLine := sLine + ',' + FieldByName('Yomi').AsString;
    	sLine := sLine + ',' + FieldByName('Zip').AsString;
    	sLine := sLine + ',' + FieldByName('Add1').AsString;
    	sLine := sLine + ',' + FieldByName('Add2').AsString;
    	sLine := sLine + ',' + FieldByName('Tel').AsString;
    	sLine := sLine + ',' + FieldByName('Fax').AsString;
    	sLine := sLine + ',' + FieldByName('Tantou').AsString;
    	sLine := sLine + ',' + FieldByName('Daihyousha').AsString;
    	sLine := sLine + ',' + FieldByName('Naiyou').AsString;

      //2006.05.17
      //sLine := sLine + ',' + FieldByName('Memo').AsString;

      //2006.03.03
      sLine := sLine + ',' + FieldByName('ChuumonHouhou').AsString; // SMALLINT,
      sLine := sLine + ',' + FieldByName('Shimejikan').AsString; // char(5),
      sLine := sLine + ',' + FieldByName('NounyuuNissuu').AsString; // SMALLINT,
      sLine := sLine + ',' + FieldByName('YoubiShiteiNouhin').AsString; // VARCHAR(7),
      sLine := sLine + ',' + FieldByName('SaiteiShukkaRotto').AsString; // SMALLINT,
      sLine := sLine + ',' + FieldByName('ShukkaKeitai').AsString; // SMALLINT,

      //2006.05.27
      //sLine := sLine + ',' + FieldByName('TaJouken').AsString; // VARCHAR(800),
      sLine := sLine + ',' + FieldByName('eZip').AsString; //  VARCHAR(12),
      sLine := sLine + ',' + FieldByName('eAdd1').AsString; // VARCHAR(12),
      sLine := sLine + ',' + FieldByName('eAdd2').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eAdd3').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eName').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eShozoku1').AsString; //   VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eTantousha1').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eBikou1').AsString; //     VARCHAR(800),
      sLine := sLine + ',' + FieldByName('eShozoku2').AsString; //   VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eTantousha2').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eBikou2').AsString; //     VARCHAR(800),
      sLine := sLine + ',' + FieldByName('eShozoku3').AsString; //   VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eTantousha3').AsString; // VARCHAR(200),
      sLine := sLine + ',' + FieldByName('eBikou3').AsString; //     VARCHAR(800),
      sLine := sLine + ',' + FieldByName('YobiStr1').AsString; //      VARCHAR(400),
      sLine := sLine + ',' + FieldByName('YobiStr2').AsString; //      VARCHAR(400),
      sLine := sLine + ',' + FieldByName('YobiStr3').AsString; //      VARCHAR(400),
      sLine := sLine + ',' + FieldByName('YobiStr4').AsString; //      VARCHAR(400),
      sLine := sLine + ',' + FieldByName('YobiInt1').AsString; //      int,
      sLine := sLine + ',' + FieldByName('YobiInt2').AsString; //      int,
      sLine := sLine + ',' + FieldByName('YobiInt3').AsString; //      int,
      sLine := sLine + ',' + FieldByName('YobiInt4').AsString; //      int

      //2006.11.16
      //〆日、支払日、支払い方法、サイトを出力
      sLine := sLine + ',' + FieldByName('Shimebi').AsString; //      int,
      sLine := sLine + ',' + FieldByName('Shiharaibi').AsString; //      int,
      sLine := sLine + ',' + FieldByName('ShiharaiHouhou').AsString; //      int,
      sLine := sLine + ',' + FieldByName('Site').AsString; //      int


      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;

		  HMakeFile(CFileName_Tokuisaki, sLine);
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '得意先一覧.xls', '', SW_SHOW);


end;

procedure TfrmMShiiresaki.EditYomiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
	 	MakeCBShiiresakiName('Yomi', EditYomi.Text, 1);
  end;
end;

//クリアボタンがクリックされた
procedure TfrmMShiiresaki.BitBtn5Click(Sender: TObject);
begin
  ClearForm;

  //初期値の入力が必要
  YobiInt1.Text := '0';
  YobiInt2.Text := '0';
  YobiInt3.Text := '0';
  YobiInt4.Text := '0';
  NounyuuNissuu.Text := '0, 当日';
  SaiteiShukkaRotto.Text := '0';
  ChuumonHouhou.Text := '1.電話';
end;

procedure TfrmMShiiresaki.BitBtn6Click(Sender: TObject);
var
  i, iNengajou : Integer;
  sLine, sSql, sYuubin, sAdd1, sAdd2, sAdd3, sName : String;
  sAddPre, sTokuisakiCode1, sTokuisakiCode2, sYomi,sHeitenFlag : String;
  F : TextFile;
begin
  //確認メッセージ
  if MessageDlg('年賀状用タックシールを出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;
  sSql := 'SELECT * FROM ' + CtblMShiiresaki;
  sSql := sSql + ' ORDER BY Code';

  //出力するファイルを作成する、すでにあれば削除する
  AssignFile(F, CFileName_Nengajou);
  Rewrite(F);
  CloseFile(F);

  //ファイルへ書き込み
  with QueryLabel do begin
    Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    i := 0;
    while not EOF do begin
      sYuubin := FieldByName('Zip').AsString;
      sAdd1   := FieldByName('Add1').AsString;
      sAdd2   := FieldByName('Add2').AsString;
      sAdd3   := '　';
      sName   := FieldByName('Name').AsString;
      //2000/11/20 Added
      sTokuisakiCode1 := FieldByName('Code').AsString;
      sTokuisakiCode2 := '　';
      sYomi := FieldByName('Yomi').AsString;
      sHeitenFlag := '　';
      sName := Trim(sName) + '　御中';

      //ファイルに書き込み
      sLine := sYuubin;
      sLine := sLine + ',' + sAdd2 + ',' + sAdd3;
      sLine := sLine + ',' + sName;

      //2000/11/20 Added
      sLine := sLine + ',' + sTokuisakiCode1;
      sLine := sLine + ',' + sTokuisakiCode2;
      sLine := sLine + ',' + sYomi;

      i := i + 1;
      SB1.SimpleText := IntToStr(i) + '件目処理中 ' + sLine;
      //If Address is same then do not write.
      if (sAddPre = (sAdd2 + sAdd3)) and (Length(sAdd2 + sAdd3)>0) then begin
        ;
      end else begin
        HMakeFile(CFileName_Nengajou, sLine);
      end;
      sAddPre := sAdd2+sAdd3;
      Next;
    end;//of while
  end;//of with
  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '鷹松屋宛名ラベル.xls', '', SW_SHOW);


end;

procedure TfrmMShiiresaki.FormActivate(Sender: TObject);
begin
  inherited;
  CBSite.Items.LoadFromFile('site.txt');
end;

end.
