inherited frmGennkinnZanndaka: TfrmGennkinnZanndaka
  Left = 190
  Top = 258
  VertScrollBar.Range = 0
  BorderStyle = bsSingle
  Caption = 'frmGennkinnZanndaka'
  ClientHeight = 154
  ClientWidth = 587
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 587
    Height = 34
    inherited Label1: TLabel
      Top = 8
      Width = 126
      Caption = #29694#37329#26410#21454#12522#12473#12488
    end
    inherited Label2: TLabel
      Left = 334
      Top = 16
      Width = 247
    end
  end
  inherited Panel2: TPanel
    Top = 100
    Width = 587
    Height = 35
    inherited BitBtn1: TBitBtn
      Left = 480
      Height = 23
      Caption = #38281#12376#12427
    end
    inherited BitBtn2: TBitBtn
      Left = 312
      Height = 23
      Caption = #12456#12463#12475#12523#12408#20986#21147
      OnClick = BitBtn2Click
    end
  end
  inherited Panel3: TPanel
    Top = 34
    Width = 587
    Height = 66
    object Label3: TLabel
      Left = 22
      Top = 15
      Width = 117
      Height = 39
      Caption = #29694#37329#22238#21454#20253#31080#12522#12473#12488#12434#34920#31034#12377#12427#26376#12434#25351#23450#12375#12390#12367#12384#12373#12356#12290
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label4: TLabel
      Left = 252
      Top = 28
      Width = 14
      Height = 13
      Caption = #24180
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 336
      Top = 28
      Width = 28
      Height = 13
      Caption = #26376#20998
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBYyyy: TComboBox
      Left = 178
      Top = 20
      Width = 71
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 0
      Text = '2000'
      Items.Strings = (
        '1999'
        '2000'
        '2001'
        '2002'
        '2003'
        '2004'
        '2005'
        '2006'
        '2007'
        '2008'
        '2009'
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016')
    end
    object CBMm: TComboBox
      Left = 288
      Top = 20
      Width = 43
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ItemHeight = 13
      ParentFont = False
      TabOrder = 1
      Text = '3'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
    end
    object rdgShuturyokuTaishou: TRadioGroup
      Left = 392
      Top = 8
      Width = 177
      Height = 41
      Caption = #20986#21147#23550#35937
      Columns = 2
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      Items.Strings = (
        #20840#12390
        #26410#22238#21454#12398#12415)
      ParentFont = False
      TabOrder = 2
    end
  end
  inherited SB1: TStatusBar
    Top = 135
    Width = 587
  end
  object QueryTokuisaki: TQuery
    DatabaseName = 'taka'
    Left = 220
    Top = 7
  end
end
