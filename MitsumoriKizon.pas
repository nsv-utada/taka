unit MitsumoriKizon;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mitsumori, DB, DBTables, ComCtrls, Grids, StdCtrls, ExtCtrls,
  Buttons;

type

  TfrmMitsumoriKizon = class(TfrmMitsumori)
    procedure FormCreate(Sender: TObject);
    procedure cmdHontorokueClick(Sender: TObject);
    procedure CBTokuisakiNameExit(Sender: TObject);
    procedure cmdHontorokuClick(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
  private
    { Private declarations }
    function DeleteTblmItem3(): Boolean;
    function UpdateTblmItem2(mode:Integer): Boolean;
    function InsertItemRireki(sTokuisakiCode,sCode1,sCode2,sTanka:String): Boolean;

  public
    { Public declarations }
  end;

var
  frmMitsumoriKizon: TfrmMitsumoriKizon;
  frmMitsumori: TfrmMitsumori;

implementation

uses PasswordDlg,inter;

{$R *.dfm}

procedure TfrmMitsumoriKizon.FormCreate(Sender: TObject);
begin
  inherited;
  lblFormName.Caption := '見積作成（既存店）';
  cmdHontorokue.Caption := '確定';
  cmdTanka420.Visible := False;
  cmdTanka450.Visible := False;
  GroupBox1.Visible := False;
  cmdKabangoToroku.Enabled  := True;
  CBKizonNo.Visible := False;
  EditTokuisakiLock.Visible := False;
  Button1.Visible := False;
  label4.Visible := False;
  label5.Visible := False;
  SG1.ColWidths[CintItemHindo]:= 42;
  SG1.ColWidths[CintItemSaishuShukkabi]:=72;

end;

procedure TfrmMitsumoriKizon.cmdHontorokueClick(Sender: TObject);
var
mNo:String;

begin
  //パスワード認証
	dlgPasswordDlg := TdlgPasswordDlg.Create(Self);
	if dlgPasswordDlg.ShowModal = mrOK then begin

	end else begin
		Exit;
	end;

	if GPassWord = CPassword1 then begin
    Beep;
	end else if GPassWord = CPassword2 then begin
    Beep;
	end else begin
	//ShowMessage('PassWordが違います');
   exit;
	end;
	GPassWord := '';
  //パスワード認証ここまで

  if MessageDlg('確定処理をする前に見積書を出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  mNo := EditKaribango.Text;
  if mNo = '' then exit;

  MakeEstimate(mNo,0);

   //ステータスを変更する
  dabMitsumori.Open;
  dabMitsumori.StartTransaction;

  if InsertMitsumori(2) = False then begin
    ShowMessage('確定処理に失敗しました。管理者に連絡して下さい。');
    dabMitsumori.Rollback;
    Exit;
  end;

  // 0はItem2を更新しない。、履歴だけとる
  if UpdateTblmItem2(0) = False then
    begin
    	Showmessage('本登録に失敗しました。管理者に連絡して下さい。');
      dabMitsumori.Rollback;
  end;

   //tblmitem3を削除
  if DeleteTblmItem3 = False then
    begin
    	Showmessage('確定処理に失敗しました。管理者に連絡して下さい。');
      dabMitsumori.Rollback;
    end
  else
    begin
      dabMitsumori.Commit;
	  	Beep();
    end;

  dabMitsumori.Close;

  //cmdHontoroku.Enabled := True;

  cmdHontorokue.Enabled := True;
  showMessage('見積り書の確定が完了しました。' + #13#10 + #13#10 + '実際の価格変更は、得意先別商品マスター画面にて変更して下さい。');



end;

procedure TfrmMitsumoriKizon.CBTokuisakiNameExit(Sender: TObject);
begin
  inherited;

    if Trim(EditKaribango.Text) = '' then begin
      cmdKabangoToroku.Enabled := True;
    end;

    cmdExcel.Enabled := False;
    cmdMitsumoriIkkatsuPrint.Enabled := False;
    cmdMitsumoriExcel.Enabled := False;
    cmdHacchushoIkkatsuPrint.Enabled := False;
    cmdHacchushoExcel.Enabled := False;
    //cmdHozon.Enabled := bEnabled;
    //cmdHontoroku.Enabled := bEnabled;
    cmdHontorokue.Enabled := False;
    if (Trim(CBTokuisakiName.Text) <> '') then begin
      CBTokuisakiName.Enabled := False;
      CBTokuisakiNameYomi.Enabled := False;
    end;

end;

function TfrmMitsumoriKizon.DeleteTblmItem3(): Boolean;
var
sSql,strMitsumoriNo : String;

begin
  strMitsumoriNo  := EditKaribango.text;

  sSql := 'DELETE FROM ' + CtblMItem3;
  sSql := sSql + ' WHERE MitsumoriNo = ' + '''' + strMitsumoriNo + '''';
  with Query1 do begin
       Close;
       Sql.Clear;
    	 Sql.Add(sSql);
	     ExecSql;
       Close;
  end;
  Result := True;
  
end;
procedure TfrmMitsumoriKizon.cmdHontorokuClick(Sender: TObject);
var dlg: TForm;
    res: Word;
    sMessage : String;
    mNo : String;
begin
  mNo := EditKaribango.Text;
  if mNo = '' then exit;

  sMessage := 'チェックが付いた商品の単価を更新します。よろしいですか？';

  dlg := CreateMessageDialog(sMessage, mtInformation,[mbYes, mbNo]);
  dlg.ActiveControl := TWinControl(dlg.FindComponent('No'));
  res := dlg.ShowModal;
  if res <> mrYes then begin
   exit;
  end;
  dlg.Free;


  dabMitsumori.Open;
  dabMitsumori.StartTransaction;

  if InsertMitsumori(2) = False then begin
    ShowMessage('本登録に失敗しました。管理者に連絡して下さい。');
    Exit;
  end;

  if UpdateTblmItem2(1) = False then
    begin
    	Showmessage('本登録に失敗しました。管理者に連絡して下さい。');
      dabMitsumori.Rollback;
  end;

  if DeleteTblmItem3 = False then
    begin
    	Showmessage('確定処理に失敗しました。管理者に連絡して下さい。');
      dabMitsumori.Rollback;
    end
  else
    begin
      dabMitsumori.Commit;
	  	Beep();
      Showmessage('本登録が完了しました');
    end;

  dabMitsumori.Close;

end;


function TfrmMitsumoriKizon.UpdateTblmItem2(mode:Integer): Boolean;
var
i : Integer;
strCode1,strCode2,strTanka,strTeiseiKaisuu,strHindo,strTokuisakiCode : String;
sSql : String;
begin

   strTokuisakiCode := EditTokuisakiCode.Text;
   

 // tblmItem2へデータを挿入
  with SG1 do begin

    i := 0;

  	while Cells[CintItemCode1,i+1]<>'' do begin

      if Cells[CintItemCheck,  i+1] = CStrChkValue then begin
         // データ作成（個別部分)
         strCode1           := Cells[CintItemCode1,  i+1];
         strCode2           := Cells[CintItemCode2,  i+1];
         strTanka           := Cells[CintItemTanka,  i+1];
         strTeiseiKaisuu    := Cells[CintItemTeiseiKaisu,  i+1];
         if StrToInt(strTeiseiKaisuu) > 0 then begin
           strHindo           := '1';
         end else begin
           strHindo           := '0';
         end;

         SB2.SimpleText := IntToStr(i) + ' ** Code1->' + strCode1 +
                          ' Code2->'  +strCode2;
         SB2.Update;

        //価格履歴をとる
        if InsertItemRireki(strTokuisakiCode,strCode1,strCode2,strTanka) = False then begin
           Result := False;
           exit;
        end;

        //本登録のとき
        if mode = 1 then begin
           // SQL文作成
	         sSql := 'UPDATE ' + CtblMItem2;
           sSql := sSql + ' SET Tanka = ''' + strTanka + '''';
           sSql := sSql + ' ,UpdatedDate = Now()';           
           sSql := sSql + ' WHERE';
           sSql := sSql + ' TokuisakiCode = ''' + strTokuisakiCode + '''';
           sSql := sSql + ' And Code1 = ''' + strCode1 + '''';
           sSql := sSql + ' And Code2 = ''' + strCode2 + '''';

          // SQL文実行
          with Query1 do begin
  		   	  Close;
	          Sql.Clear;
  	    	  Sql.Add(sSql);
  	  	    ExecSql;
	          Close;
          end;
        end;

    end;

    i := i + 1;

    end;

  end;

  Result := True;

end;

function TfrmMitsumoriKizon.InsertItemRireki(sTokuisakiCode,sCode1,sCode2,sTanka:String): Boolean;
var
sSql : String;
strTankaOrg : String;

begin

    // 現在の単価を取得する。
    sSql := 'SELECT Tanka FROM ' + CtblMItem2;
//    sSql := sSql + ' WHERE TokuisakiCode = ' + '''' + sTokuisakiCode + ''''; #thuyptt 20190313
    sSql := sSql + ' WHERE TokuisakiCode = ' + sTokuisakiCode;
    sSql := sSql + ' AND Code1 = ' + '''' + sCode1 + '''';
    sSql := sSql + ' AND Code2 = ' + '''' + sCode2 + '''';

    with Query1 do begin
         Close;
         Sql.Clear;
         Sql.Add(sSql);
         ExecSql;
         Open;

         FetchAll;
         strTankaOrg := FieldbyName('Tanka').AsString;
         Close;
    end;

    if sTanka <>  strTankaOrg then begin
	     sSql := 'INSERT INTO ' + CtblTItemRireki + ' (';
       sSql := sSql + ' TokuisakiCode, Code1, Code2, Tanka, Tanka_old, ModDate, CreatedDate, UpdatedDate, UserID';
       sSql := sSql + ' )';
       sSql := sSql + ' VALUES (';
       sSql := sSql + '''' + sTokuisakiCode     + ''', ' ;
       sSql := sSql + '''' + sCode1           + ''', ' ;
       sSql := sSql + '''' + sCode2           + ''', ' ;
       sSql := sSql + '''' + sTanka           + ''', ' ;
       sSql := sSql + '''' + strTankaOrg       + ''', ' ;
       sSql := sSql + '''' + DateTimeToStr(Now())    + ''', ' ;
      sSql := sSql + 'Now(), ' ; //thuyptt 20190515
      sSql := sSql + 'Now(), ' ;  //thuyptt 20190515
       sSql := sSql + '''' + CBTantosha.Text       + ''') ' ;


       with Query1 do begin
            Close;
            Sql.Clear;
            Sql.Add(sSql);
            ExecSql;
            Close;
       end;

    end;


    Result := True;


end;

procedure TfrmMitsumoriKizon.SG1DblClick(Sender: TObject);
var
i :Integer;
sTokuisakiCode,sCode1,sCode2,sSql,strMsg : String;
begin

  if (SG1.Col <> CintItemTankaOld) Or (SG1.Row < 1)
      or (SG1.Cells[CintItemCode1,SG1.Row] = '') then begin exit end;


  sTokuisakiCode := EditTokuisakiCode.Text;
  sCode1 := SG1.Cells[CintItemCode1, SG1.Row];
  sCode2 := SG1.Cells[CintItemCode2, SG1.Row];

  sSql := 'SELECT * FROM ' + CtblTItemRireki;
  sSql :=  sSql + ' WHERE';
  sSql :=  sSql + ' Code1 = ''' + sCode1 + '''';
  sSql :=  sSql + ' AND Code2 = ''' + sCode2 + '''';
  sSql :=  sSql + ' AND TokuisakiCode = ''' + sTokuisakiCode + '''';
  sSql :=  sSql + ' ORDER BY ModDate';


  // Excecute SQL
  with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;

      FetchAll;

      // もしレコードが無かったら，メッセージボックスを表示して処理を抜ける．
      if RecordCount <= 0 then
        begin
          ShowMessage('この商品の価格変更履歴はありません');
          Close;
          Exit;
       end;

      // Set Data to StringGrid
      i := 0;
      strMsg := '■商品価格変更履歴■'+ #13#10;
      strMsg := strMsg + '商品コード：Code1=' + sCode1 + ' Code2=' + sCode2 + #13#10;
      strMsg := strMsg + '---------------------------------------------------' + #13#10;
      while not EOF do begin
    	  i := i+1;
        strMsg := strMsg + IntToStr(i) + '.' ;
        strMsg := strMsg + '価格：' + FieldbyName('Tanka_old').AsString + '→' + FieldbyName('Tanka').AsString;
        strMsg := strMsg + '/ 変更日：' + FieldbyName('ModDate').AsString;
        strMsg := strMsg + '/ 担当者：' + FieldbyName('UserID').AsString;
        strMsg := strMsg + #13#10;
        next
      end;
      strMsg := strMsg + '---------------------------------------------------' + #13#10;

      showMessage(strMsg);

  end;

end;

procedure TfrmMitsumoriKizon.btnResetClick(Sender: TObject);
begin
  inherited;
      CBTokuisakiName.Enabled := True;
      CBTokuisakiNameYomi.Enabled := True;
end;

end.
