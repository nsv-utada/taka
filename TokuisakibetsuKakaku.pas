unit TokuisakibetsuKakaku;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UriageShuukei, Db, DBTables, ComCtrls, StdCtrls, Buttons, ExtCtrls, ShellAPI;

type
  TfrmTokuisakibetsuKakaku = class(TfrmUriageShuukei)
    EditChainCode: TEdit;
    Query1: TQuery;
    Label6: TLabel;
    MemoTokuisakiCode: TMemo;
    Label11: TLabel;
    Button1: TButton;
    CBName: TComboBox;
    CheckBox1: TCheckBox;
    Label12: TLabel;
    MemoItemCode: TMemo;
    Label13: TLabel;
    Button2: TButton;
    Label15: TLabel;
    CBCode1: TComboBox;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    CBTokuisakiName: TComboBox;
    QueryHindo: TQuery;
    Label18: TLabel;
    procedure CBTokuisakiNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBNameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure EditChainCodeKeyPress(Sender: TObject; var Key: Char);
    procedure CBCode1KeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure MemoTokuisakiCodeChange(Sender: TObject);
    procedure MemoItemCodeChange(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CBNameChange(Sender: TObject);
    procedure CBCode1Change(Sender: TObject);
    procedure Panel3Click(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
		procedure MakeCBName(sKey, sWord:String; iKubun:Integer);
		//procedure MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
		Function GetHindo(sTokuisakiCode,sCode1,sCode2,sYyyyMmDdFrom,sYyyyMmDdTo:String):String;
  public
    { Public 宣言 }
  end;

var
  frmTokuisakibetsuKakaku: TfrmTokuisakibetsuKakaku;
  Gi, Gj : Integer;
implementation
uses
	Inter, HKLib, DMMaster, DM1;

{$R *.DFM}


{
	得意先名のコンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmTokuisakibetsuKakaku.MakeCBTokuisakiName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBTokuisakiName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMTokuisaki;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY TokuisakiCode1 ';
  CBTokuisakiName.Clear;
	with frmDMMaster.QueryMTokuisaki do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBTokuisakiName.Items.Add(FieldByName('TokuisakiCode1').AsString + ',' +
       FieldByName('TokuisakiNameUp').AsString + ' ' +
       FieldByName('TokuisakiName').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBTokuisakiName.DroppedDown := True;
end;



procedure TfrmTokuisakibetsuKakaku.CBNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
	if (Key=VK_F1) then begin
    CBName.SetFocus;
	 	MakeCBName('Yomi', CBName.Text, 1);
  end;

end;
{
	コンボボックスを作成する
  引数：sKey   -> 検索列名
  			sWord  -> 検索ワード
  			iKubun -> 0=前方一致, 1=曖昧検索, 2=数字の完全一致
}
procedure TfrmTokuisakibetsuKakaku.MakeCBName(sKey, sWord:String; iKubun:Integer);
var
	sSql : String;
begin

  CBName.Items.Clear;


	sSql := 'SELECT * FROM ' + CtblMItem;
  sSql := sSql + ' WHERE ';
  sSql := sSql + sKey;
  case iKubun of
  0: sSql := sSql + ' LIKE ''' + sWord + '%''';
  1: sSql := sSql + ' LIKE ''%' + sWord + '%''';
  2: sSql := sSql + ' = ' + sWord;
  end;

  sSql := sSql + ' ORDER BY Yomi ';
  CBName.Clear;
	with Query1 do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    Open;
    while not EOF do begin
    	CBName.Items.Add(FieldByName('SID').AsString +
       ',' + FieldByName('Name').AsString +
       ',' + FieldByName('Irisuu').AsString +
       ',' + FieldByName('Kikaku').AsString);
    	Next;
    end;//of while
    Close;
  end;//of with

  CBName.DroppedDown := True;
end;

//Excelへ出力ボタン
procedure TfrmTokuisakibetsuKakaku.BitBtn2Click(Sender: TObject);
var
	sSql, sTokuisakiCode1, sLine, sTokuisakiCode, sCode1, sDateTo : String;
 	F : TextFile;
  sSqlTokuisakiCode2,sMaker,sTanni,sName,sSqlCode1 : String;
  i, j : Integer;
  sTableName, sReturnFieldName, sWhere,sLine2,sHaisouCode,sTokuisakiName : String;
  sHIndo, sYyyyMmDdFrom,sYyyyMmDdTo : String;
	sICode1, sICode2 : String;
begin
	//エラーチェック
  if (MemoTokuisakiCode.Lines[0]='') then begin
    ShowMessage('得意先コードを指定してください');
    exit;
  end;
  if (MemoItemCode.Lines[0]='') then begin
    ShowMessage('商品コードを指定してください');
    exit;
  end;
  if Gi*Gj> 10000 then begin
    ShowMessage('得意先と商品の組み合わせが多すぎます -> '+ IntToStr(Gi*Gj));
    exit;
  end;

	//確認メッセージ
  if MessageDlg('エクセルへ出力しますか?',
    mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    Exit;
  end;

  //得意先コード・商品コードの取得
  //sTokuisakiCode :=Copy(CBTokuisakiName.Text, 1, (pos(',', CBTokuisakiName.Text)-1));
  //sCode1 :=Copy(CBCode1.Text, 1, (pos(',', CBCode1.Text)-1));

	//出力するファイルを作成する、すでにあれば削除する
 	AssignFile(F, CFileName_TokusakiBetauKakaku);
  Rewrite(F);
	CloseFile(F);

  //タイトル部分の出力
  //得意先コード,得意先名,
  //商品コード,商品名,420価格,450価格,仕入れ価格,実売価格
  {
  sLine := '商品コード';
  sLine := sLine + ',' + '商品名';
  sLine := sLine + ',' + '420価格';
  sLine := sLine + ',' + '450価格';
  sLine := sLine + ',' + '仕入れ価格';
  sLine := sLine + ',' + '実売価格';
  HMakeFile(CFileName_TokusakiBetauKakaku, sLine);
  }
  //ここまでタイトル作成
  //得意先コードでループ
  i := 0;
	sLine := MemoTokuisakiCode.Lines[i];
  while (MemoTokuisakiCode.Lines[i] <> '') do begin
  	//得意先コード
  	sTokuisakiCode1 := Copy(MemoTokuisakiCode.Lines[i],1,Pos(',',MemoTokuisakiCode.Lines[i])-1);
    //配送コード
    sTableName := CtblMTokuisaki;
    sReturnFieldName := 'TokuisakiCode2';
    sWhere := ' where TokuisakiCode1 = ' +  sTokuisakiCode1;
		sHaisouCode := DM1.GetFieldData(sTableName, sReturnFieldName, sWhere);
    //得意先名
    sReturnFieldName := 'TokuisakiName';
		sTokuisakiName := DM1.GetFieldData(sTableName, sReturnFieldName, sWhere);
    sLine := sTokuisakiCode1 + ',' + sHaisouCode + ',' +  sTokuisakiName;
		SB1.SimpleText := '計算中 ' + sLine;
    //商品コードでループ
    j := 0;
	  while (MemoItemCode.Lines[j] <> '') do begin
      sTokuisakiCode := Copy(MemoTokuisakiCode.Lines[i],1,Pos(',',MemoTokuisakiCode.Lines[i])-1);
    	sCode1 := Copy(MemoItemCode.Lines[j],1,Pos(',',MemoItemCode.Lines[j])-1);


			sSql := 'SELECT Distinct N.Code1,N.Code2, N.Tanka, M.Name, M.Tanka420, M.Tanka450';
			sSql := sSql + ', M.Genka';
			sSql := sSql + ', M.ShiiresakiCode';
			sSql := sSql + ', M.Tanni';
			sSql := sSql + ', M.Kikaku';
			sSql := sSql + ', N.Hindo';
			sSql := sSql + ', M.Irisuu';
			sSql := sSql + ' from tblMItem M, tblMItem2 N';
			sSql := sSql + ' WHERE ';
			sSql := sSql + ' N.Code1 = M.Code1';
			sSql := sSql + ' AND ';
			sSql := sSql + ' N.Code2 = M.Code2';
			sSql := sSql + ' AND ';
			sSql := sSql + ' N.TokuisakiCode= ' + sTokuisakiCode;
	    sSql := sSql + ' AND ';
			sSql := sSql + ' M.SID= ' + sCode1 ;

      with QueryTokuisaki do begin
				Close;
			  Sql.Clear;
			  Sql.Add(sSql);
			  Open;
			  if not EOF then begin
          //商品コード
          sICode1 := FieldByName('Code1').AsString;
          sICode2 := FieldByName('Code2').AsString;
      		sLine2 := FieldByName('Code1').AsString + '_' + FieldByName('Code2').AsString;
          //商品名
			  	sLine2 := sLine2 + ',' + FieldByName('Name').AsString;
          //規格
          if FieldByName('Kikaku').AsString = '' then begin
				  	sLine2 := sLine2 + ',' + '(';
				  	sLine2 := sLine2 + FieldByName('Irisuu').AsString;
				  	sLine2 := sLine2 + ')';
          end else begin
				  	sLine2 := sLine2 + ',' + FieldByName('Kikaku').AsString;
				  	sLine2 := sLine2 + '(';
				  	sLine2 := sLine2 + FieldByName('Irisuu').AsString;
				  	sLine2 := sLine2 + ')';
          end;
          //単位
			  	sLine2 := sLine2 + ',' + FieldByName('Tanni').AsString;
          //価格
				  sLine2 := sLine2 + ',' + FieldByName('Tanka420').AsString;
				  sLine2 := sLine2 + ',' + FieldByName('Tanka450').AsString;
				  sLine2 := sLine2 + ',' + FieldByName('Genka').AsString;
				  sLine2 := sLine2 + ',' + FieldByName('Tanka').AsString;

          //頻度
          //2003.09.21 期間限定にする
          sYyyyMmDdFrom := CBYyyyFrom.Text + '/' + CBMmFrom.Text + '/01';
          sYyyyMmDdTo   := CBYyyyTo.Text   + '/' + CBMmTo.Text   + '/';
					sYyyyMmDdTo   := sYyyyMmDdTo + IntToStr(HKLib.GetGetsumatsu(StrToint(CBYyyyTo.Text), StrToInt(CBMmTo.Text)));

          sHIndo := GetHindo(sTokuisakiCode,sICode1,sICode2,sYyyyMmDdFrom,sYyyyMmDdTo);
				  //sLine2 := sLine2 + ',' + FieldByName('Hindo').AsString;
				  sLine2 := sLine2 + ',' + sHIndo;

			    //仕入れ先コード
      		sLine2 := sLine2 + ',' + FieldByName('ShiiresakiCode').AsString;

        	SB1.SimpleText := sLine2;
				  SB1.Update;
				 	//Next;
        end else begin
          sLine2 := '';

			  end;//of if

		  end;//of with
    	j := j + 1;
	    SB1.SimpleText := sLine2;
		  SB1.Update;
		  HMakeFile(CFileName_TokusakiBetauKakaku, sLine + ',' + sLine2);
    end;//end of while
  	i := i + 1;
  end;

  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '得意先別価格一覧.xls', '', SW_SHOW);

  exit;

  if EditChainCode.text <> '' then begin
	  if MessageDlg('チェーンコードで出力します?',
  	  mtConfirmation, [mbYes, mbNo], 0) = mrNo then begin
    	Exit;
	  end;

		SB1.SimpleText := '計算中 ';

    sSqlTokuisakiCode2 := 'select * from tblMTokuisaki where ChainCode=';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + EditChainCode.text;
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' and ';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' HeitenFlag = 0';
    sSqlTokuisakiCode2 := sSqlTokuisakiCode2 + ' order by TokuisakiCode1';
    with QueryTokuisaki do begin
	  	Close;
  	  Sql.Clear;
    	Sql.Add(sSqlTokuisakiCode2);
	    open;
  	  while not EOF do begin
      	sTokuisakiCode := FieldByName('TokuisakiCode1').AsString;
        sLine := FieldByName('TokuisakiCode1').AsString + FieldByName('TokuisakiName').AsString;
			  HMakeFile(CFileName_TokusakiBetauKakaku, sLine);

				sSql := 'SELECT N.Code1,N.Code2, N.Tanka, M.Name, M.Tanka420, M.Tanka450';
			  sSql := sSql + ', M.Genka';
			  sSql := sSql + ' from tblMItem M, tblMItem2 N';
			  sSql := sSql + ' WHERE ';
			  sSql := sSql + ' N.Code1 = M.Code1';
			  sSql := sSql + ' AND ';
			  sSql := sSql + ' N.Code2 = M.Code2';
			  sSql := sSql + ' AND ';
			  sSql := sSql + ' N.TokuisakiCode= ' + sTokuisakiCode;
			  if sCode1<>'' then begin
				  sSql := sSql + ' AND ';
			  	sSql := sSql + ' N.Code1= ''' + sCode1 + '''';
			  end;

        with QueryTokuisaki do begin
			  	Close;
			    Sql.Clear;
			    Sql.Add(sSql);
			    Open;
			    While not EOF do begin
      			sLine := FieldByName('Code1').AsString + '_' + FieldByName('Code2').AsString;
			      sLine := sLine + ',' + FieldByName('Name').AsString;
			      sLine := sLine + ',' + FieldByName('Tanka420').AsString;
			      sLine := sLine + ',' + FieldByName('Tanka450').AsString;
			      sLine := sLine + ',' + FieldByName('Genka').AsString;
			      sLine := sLine + ',' + FieldByName('Tanka').AsString;

			      SB1.SimpleText := sLine;
			      SB1.Update;
					  HMakeFile(CFileName_TokusakiBetauKakaku, sLine);
			    	Next;
			    end;//of while
	    		next;
			  end;//of with

      end;//of while
    end;//of with
  end else begin
  	;
  end;//of if


  //エクセルの起動
  ShellExecute(Handle, 'OPEN', 'EXCEL.EXE', CFileDir + '得意先別価格一覧.xls', '', SW_SHOW);

end;


//期間限定の頻度を返す
//2003.09.21
Function TfrmTokuisakibetsuKakaku.GetHindo(sTokuisakiCode,sCode1,sCode2,sYyyyMmDdFrom,sYyyyMmDdTo:String):String;
var
	sSql : String;
begin
  sSql := 'Select sC=Count(*) from ' + CtblTDenpyouDetail;
  sSql := sSql + ' where TokuisakiCode1=' + sTokuisakiCode;
  sSql := sSql + ' and Code1=''' + sCode1 + '''';
  sSql := sSql + ' and Code2=''' + sCode2 + '''';
  sSql := sSql + ' and NouhinDate between ''' + sYyyyMmDdFrom + '''';
  sSql := sSql + ' and ''' + sYyyyMmDdTo + '''';

  with QueryHindo do begin
  	Close;
    Sql.Clear;
    Sql.Add(sSql);
    open;
	  result := FieldByName('sC').AsString;
    Close;
  end;
end;


procedure TfrmTokuisakibetsuKakaku.EditChainCodeKeyPress(Sender: TObject;
  var Key: Char);
begin
	MemoTokuisakiCode.Text := '';
  CBTokuisakiName.Text   := '';
end;

procedure TfrmTokuisakibetsuKakaku.CBCode1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  //CBName.text := '';

end;

procedure TfrmTokuisakibetsuKakaku.Button1Click(Sender: TObject);
var
	ArrayTokuisakiCode : array[1..5000] of Integer;
  sLastDay,sSql,sDateFrom,sDateTo,sWhere : String;
  i,j : Integer;
  sTokuisakiName : String;
  iTokuisakiCount : integer;
begin
{
	1.チェーンコードがあればその得意先を抽出する
  2.得意先コードの件数を把握する
  3.得意先コードを配列に入れる
  4.得意先コードが200件を超える場合にはアラートをだす
}
//配列の初期化
  FillChar(ArrayTokuisakiCode, SizeOf(ArrayTokuisakiCode), False);

	//MemoTokuisakiCode.Text := '';
	if CBTokuisakiName.Text <> '' then begin
		//sTokuisakiName := Copy(CBTokuisakiName.Text, 1, Pos(',',CBTokuisakiName.Text)-1);
	  //ArrayTokuisakiCode[1] := StrToInt(sTokuisakiName);
	  MemoTokuisakiCode.Lines.Add(CBTokuisakiName.Text);
  	Exit;
	end;


//チェーンコードを見る
  if EditChainCode.Text<>'' then begin
    MemoTokuisakiCode.Text := '';

  	sSql := 'Select TokuisakiCode1 from ' + CtblMTokuisaki;
    sSql := sSql + ' where ';
    sSql := sSql + ' TokuisakiCode2 = ' + Trim(EditChainCode.Text);

  	with QueryTokuisaki do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      i := 1;
      while not EOF do begin
      	ArrayTokuisakiCode[i] := FieldByname('TokuisakiCode1').asInteger;

        i := i + 1;
//        if i > 500 then begin
//      2004.04.01 配送コードで集計すると500件を超えるので
        iTokuisakiCount := 5000;
        if i > iTokuisakiCount then begin
        	ShowMessage('Over ' + IntToStr(iTokuisakiCount));
        	Break;
        end;
      	Next;
      end;//of while
      Close;
      Label4.Caption := IntToStr(i) + '件の得意先で処理します。';
    end;//of with
  end;


//その期日に取引があるかどうか見る
 	sLastDay := IntToStr(HKLib.GetGetsumatsu(
                    StrToInt(CBYyyyTo.Text),StrToInt(CBMmTo.Text))
      	        );
  sDateFrom := CBYyyyFrom.Text + '/' + CbMmFrom.Text + '/01';
  sDateTo   := CBYyyyTo.Text   + '/' + CbMmTo.Text   + '/' + sLastDay;
	i := 1;
	while ArrayTokuisakiCode[i]>0 do begin
		sSql := 'Select UriageKingaku from tblTDenpyou';
  	sSql := sSql + ' where ';
	  sSql := sSql + ' TokuisakiCode1=' + IntTostr(ArrayTokuisakiCode[i]);
	  sSql := sSql + ' And ';
	  sSql := sSql + ' DDate between ''' + sDateFrom + ''' and ''' + sDateTo + '''';
  	with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      if FieldByName('UriageKingaku').asInteger=0 then begin
        ArrayTokuisakiCode[i] := 0;
      end;
      Close;
    end;//end of with
    i := i + 1;
  end;//of while

	//最終結果を配列に格納する
  j := 0;
  for i := 1 to iTokuisakiCount do begin
  	if ArrayTokuisakiCode[i]>0 then begin
		  //その配列の内容をMemoに表示する
      //MemoTokuisakiCode.Text := MemoTokuisakiCode.Text + ',' + IntToStr(ArrayTokuisakiCode[i]);
 			//特定のテーブルから特定のフィールドデータを文字型で返す
      sWhere := ' TokuisakiCode1 = ' + IntToStr(ArrayTokuisakiCode[i]);
			sTokuisakiName := DMMaster.GetFieldData(CtblMTokuisaki, 'TokuisakiName', sWhere);

      MemoTokuisakiCode.Lines.Add(IntToStr(ArrayTokuisakiCode[i])+','+sTokuisakiName);
       //Text := MemoTokuisakiCode.Text + ',' + IntToStr(ArrayTokuisakiCode[i]);
    	j := j + 1;
    end;
  end;

  if j > iTokuisakiCount then begin
  	ShowMessage('得意先数が多すぎます 処理件数=' + IntToStr(j) + '件');
  end;
  Label4.Caption := IntToStr(j) + '件の得意先で処理します。';

  Gi := j;
end;

procedure TfrmTokuisakibetsuKakaku.FormCreate(Sender: TObject);
begin
  inherited;

  //WindowSize
  Width := 855;
  Height := 450;
  //本日の期日を入れる
	CBYyyyTo.Text := Copy(DateToStr(date),0,4);
	CBMmTo.Text := Copy(DateToStr(date),6,2);
	CBYyyyFrom.Text := Copy(DateToStr(date),0,4);
	CBMmFrom.Text := Copy(DateToStr(date),6,2);
end;

//商品コードの確定
procedure TfrmTokuisakibetsuKakaku.Button2Click(Sender: TObject);
var
	sSql, sItemCode, sCode1 : String;
  iTokuisakiCount, iItemCount, i, j, iCount: Integer;
begin
//  inherited;
//2004.04.01
// 得意先コード数×商品アイテム数が10000件以下になるように
iCount := 10000;

//商品名・商品コード・すべて
if CBName.Text <> '' then begin
  MemoItemCode.Lines.Add(CBName.Text);
{
	j :=0;
  j :=j+1;
  Gj := Gj + j;
  Label5.Caption := IntToStr(Gj) + 'アイテムの商品で出力します。';
}
  Exit;
end;

	//MemoItemCodeのクリア
	MemoItemCode.Lines.Clear;

	//商品コード
	if CBCode1.Text <> '' then begin
		sCode1 := Copy(CBCode1.Text, 1, (Pos(',',CBCode1.Text)-1));
		sSql := 'Select SID,Name from ' + CtblMItem;
  	sSql := sSql + ' where ';
	  sSql := sSql + ' Code1=''' + sCode1 + '''';
	  sSql := sSql + ' order by Code2 ';
  	i := 0;
    with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      while not EOF do begin
        sItemCode := FieldByName('SID').AsString + ',';
        sItemCode := sItemCode + FieldByName('Name').AsString;
			  MemoItemCode.Lines.Add(sItemCode);
		    i := i + 1;
      	Next;
      end;//of while
      Close;
    end;//end of with
	  Label5.Caption := IntToStr(i) + 'アイテムの商品で出力します。';
    Gj := i;
	end else 	//すべて
  if CheckBox1.Checked = True then begin
		sSql := 'Select SID,Name from ' + CtblMItem;
	  sSql := sSql + ' order by Code1, Code2 ';
  	i := 0;
    with Query1 do begin
    	Close;
      Sql.Clear;
      Sql.Add(sSql);
      Open;
      while not EOF do begin
        sItemCode := FieldByName('SID').AsString + ',';
        sItemCode := sItemCode + FieldByName('Name').AsString;
			  MemoItemCode.Lines.Add(sItemCode);
		    i := i + 1;
      	Next;
      end;//of while
      Close;
    end;//end of with
    i := i + 1;
	  Label5.Caption := IntToStr(i) + 'アイテムの商品で出力します。';
    Gj := i;
  	Exit;
  end;
  //2004.04.01
  //10,000件のチェック
  iTokuisakiCount := MemoTokuisakiCode.Lines.Count;
  iItemCount      := MemoItemCode.Lines.Count;

  if (iTokuisakiCount*iItemCount) > iCount then begin
    Showmessage('得意先数×商品アイテム数が'+InttoStr((iTokuisakiCount*iItemCount))+
                '件になりました。');
    Showmessage(InttoStr(iCount) + '件以下になるように調節してください');
  end else begin
    Showmessage('得意先数×商品アイテム数は'+InttoStr((iTokuisakiCount*iItemCount))+'件です。');
  end
end;

procedure TfrmTokuisakibetsuKakaku.MemoTokuisakiCodeChange(
  Sender: TObject);
begin
//  inherited;
//再計算
	Gi := MemoTokuisakiCode.Lines.Count;
  Label4.Caption := IntToStr(Gi) + '件の得意先で処理します。';
end;

procedure TfrmTokuisakibetsuKakaku.MemoItemCodeChange(Sender: TObject);
begin
//  inherited;
//再計算
	Gj := MemoItemCode.Lines.Count;
  Label5.Caption := IntToStr(Gj) + 'アイテムの商品で出力します。';

end;

procedure TfrmTokuisakibetsuKakaku.CheckBox1Click(Sender: TObject);
begin
//  inherited;
//2003.08.03
	CBName.Text  := '';
  CBCode1.Text := '';
  MemoItemCode.Text := '';


end;

procedure TfrmTokuisakibetsuKakaku.CBNameChange(Sender: TObject);
begin
//  inherited;
//  inherited;
//2003.08.03
  CBCode1.Text := '';
  CheckBox1.Checked := False;

end;

procedure TfrmTokuisakibetsuKakaku.CBCode1Change(Sender: TObject);
begin
//  inherited;
  MemoItemCode.Text := '';
  CheckBox1.Checked := False;
	CBName.Text  := '';

end;

procedure TfrmTokuisakibetsuKakaku.CBTokuisakiNameKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  inherited;
	if (Key=VK_F1) then begin
    CBTokuisakiName.SetFocus;
  	MakeCBTokuisakiName('TokuisakiNameYomi', CBTokuisakiName.Text, 1);
  end;

end;

procedure TfrmTokuisakibetsuKakaku.Panel3Click(Sender: TObject);
var
 iItemCount,iTokuisakiCount : integer;
begin
  iItemCount      := MemoItemCode.Lines.Count;
  iTokuisakiCount := MemoTokuisakiCode.Lines.Count;
  SB1.SimpleText := 'TokuisakiCount=' + IntToStr(iTokuisakiCount) + ' ItemCount=' + IntToStr(iItemCount);
  SB1.Update;
end;

end.
