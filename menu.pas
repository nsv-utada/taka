unit menu;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, DB, DBTables, StdCtrls;

type
  TfrmMenu = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenu: TfrmMenu;

implementation
uses
DM1,inter,MTokuisaki, TDenpyou, TKaishuu,Denpyou, Mitsumori,MitsumoriKizon,MitsumoriArari,
PBill, PEstimate, PUrikakeDaichou, PUrikakeDaichou2,PCourceUriage, PMishuukin,
MItemT,MItem,MShiiresaki,MTokuisaki2, UrikakeZan, UriageShuukei,MGyoushu,
Hacchuu,ZaikoKanri,Shukko,MItemC,CopyItem,UriageDenpyouIkkatuInnsatu, GennkinnKaishuu,
GennkinnZanndaka,CheckItem2,PasswordDlg,TokuisakibetsuKakaku,
KaChouhyou,Shime,JuchuuHindo2,IkkatsuHenkou4,Sync_Hindo,Nyuuko,ShiireDaichou,ShiharaiNyuuryoku,ChangeItem,InfoMart, ListInfoMart,InfoBuy, ListInfoBuy;

{$R *.dfm}

procedure TfrmMenu.Button1Click(Sender: TObject);
begin
    TfrmPBill.Create(Self);
end;

procedure TfrmMenu.FormContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin
  top  := 5;
  Left := 5;
	Width := 700+50;
  Height := 500;
end;

end.
