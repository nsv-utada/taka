inherited frmJyuchuuHindo: TfrmJyuchuuHindo
  Left = 654
  Top = 87
  Width = 567
  Height = 265
  Caption = 'frmJyuchuuHindo'
  PixelsPerInch = 96
  TextHeight = 12
  inherited Panel1: TPanel
    Width = 559
    inherited Label1: TLabel
      Caption = #21463#27880#38971#24230#19968#35239
    end
  end
  inherited Panel2: TPanel
    Top = 178
    Width = 559
  end
  inherited Panel3: TPanel
    Width = 559
    Height = 137
    inherited Label3: TLabel
      Left = 12
      Top = 8
      Width = 200
      Caption = #38598#35336#12377#12427#26399#38291#12434#25351#23450#12375#12390#12367#12384#12373#12356#12290
    end
    inherited Label4: TLabel
      Left = 220
      Top = 34
    end
    inherited Label5: TLabel
      Left = 452
      Top = 34
    end
    inherited Label7: TLabel
      Left = 348
      Top = 34
    end
    inherited Label8: TLabel
      Left = 422
      Top = 34
    end
    inherited Label9: TLabel
      Left = 116
      Top = 34
    end
    inherited Label10: TLabel
      Left = 186
      Top = 34
    end
    object Label6: TLabel [7]
      Left = 12
      Top = 74
      Width = 56
      Height = 13
      Caption = #24471#24847#20808#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel [8]
      Left = 14
      Top = 102
      Width = 81
      Height = 13
      Caption = #12481#12455#12540#12531#12467#12540#12489
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel [9]
      Left = 232
      Top = 94
      Width = 27
      Height = 13
      Caption = #12363#12388
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel [10]
      Left = 306
      Top = 76
      Width = 42
      Height = 13
      Caption = #21830#21697#21517
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel [11]
      Left = 282
      Top = 102
      Width = 70
      Height = 13
      Caption = #21830#21697#12467#12540#12489'1'
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited CBYyyyTo: TComboBox
      Left = 274
      Top = 30
      Text = '2003'
    end
    inherited CBMmTo: TComboBox
      Left = 374
      Top = 30
      Text = '5'
    end
    inherited CBYyyyFrom: TComboBox
      Left = 42
      Top = 30
      Text = '2002'
    end
    inherited CBMmFrom: TComboBox
      Left = 140
      Top = 30
    end
    object EditChainCode: TEdit
      Left = 104
      Top = 100
      Width = 63
      Height = 20
      ImeMode = imClose
      TabOrder = 4
      OnKeyPress = EditChainCodeKeyPress
    end
    object CBTokuisakiName: TComboBox
      Left = 76
      Top = 72
      Width = 145
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 5
      OnKeyDown = CBTokuisakiNameKeyDown
    end
    object CBName: TComboBox
      Left = 356
      Top = 74
      Width = 155
      Height = 20
      ImeMode = imOpen
      ItemHeight = 12
      TabOrder = 6
      OnKeyDown = CBNameKeyDown
    end
    object CBCode1: TComboBox
      Left = 356
      Top = 98
      Width = 163
      Height = 21
      Font.Charset = SHIFTJIS_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = #65325#65331' '#65328#12468#12471#12483#12463
      Font.Style = []
      ImeMode = imClose
      ItemHeight = 13
      ParentFont = False
      TabOrder = 7
      Items.Strings = (
        '01,'#26989#21209#29992#39135#26448
        '02,'#35519#21619#26009#35519#21619#39135#21697
        '03,'#39321#36763#26009
        '04,'#12499#12531#12289#32566#35440#39006
        '05,'#12467#12540#12498#12540#12289#21931#33590#26448#26009
        '06,'#20083#35069#21697#65288#12481#12540#12474#39006#65289
        '07,'#39135#29992#27833
        '08,'#31881#35069#21697
        '09,'#12418#12385
        '10,'#12473#12497#40634
        '11,'#40634
        '12,'#31859
        '13,'#20013#33775#26448#26009
        '14,'#21508#31278#12472#12517#12540#12473
        '15,'#12362#33590
        '16,'#27703#12471#12525#12483#12503
        '17,'#28460#29289#24803#33756
        '18,'#12362#36890#12375#29289
        '19,'#39135#32905#21152#24037#21697
        '20,'#28023#29987#29645#21619
        '21,'#20919#20941#39135#21697#39770#20171#39006
        '22,'#20919#20941#39135#21697#36786#29987#29289
        '23,'#20919#20941#39135#21697#19968#27425#21152#24037#21697
        '24,'#20919#20941#39135#21697#28857#24515#39006
        '25,'#12486#12522#12540#12492#39006
        '26,'#12524#12488#12523#12488#39006#28271#29006
        '27,'#20919#20941#35201#20919#12289#39321#36763#26009
        '28,'#12362#12388#12414#12415#35910#39006
        '29,'#12362#12388#12414#12415#12362#12363#12365#39006
        '30,'#12362#12388#12414#12415#12481#12483#12503#39006
        '31,'#12481#12519#12467#12524#12540#12488#12289#12476#12522#12540#39006
        '32,'#28023#29987#29289
        '33,'#24178#12375#32905
        ''
        '')
    end
  end
  inherited SB1: TStatusBar
    Top = 219
    Width = 559
  end
  object Query1: TQuery
    DatabaseName = 'taka'
    Left = 232
    Top = 1
  end
  object QueryCode1: TQuery
    DatabaseName = 'taka'
    Left = 266
    Top = 1
  end
end
