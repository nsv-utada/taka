unit ItemKensaku;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Master, ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids;

type
  TfrmItemKensaku = class(TfrmMaster)
    SG1: TStringGrid;
    Label3: TLabel;
    Edit1: TEdit;
    BitBtn3: TBitBtn;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SG1DblClick(Sender: TObject);
  private
    { Private 宣言 }
		procedure MakeSG;
  public
    { Public 宣言 }
  end;

  const
	//グリッドの列の定義

  CintNum           = 0;
  CintItemCode      = 1;
  CintItemName      = 2;
  CintItemPrice     = 3;
  CintItemTanni     = 4;
  CintEnd           = 5;   //ターミネーター


	GRowCount = 700; //行数

var
  frmItemKensaku: TfrmItemKensaku;

implementation

{$R *.DFM}

procedure TfrmItemKensaku.FormCreate(Sender: TObject);
begin
  inherited;

	Width := 600;
  Height := 500;

  //ストリンググリッドの作成
  MakeSG;

end;
procedure TfrmItemKensaku.MakeSG;
begin
	with SG1 do begin
    DefaultDrawing := True;
    RowCount := GRowCount;
    ColCount := CintEnd;
    Align := alBottom;
    FixedCols := 0;
    FixedColor := clYellow;
    Canvas.Font.Size := 11;
    DefaultRowHeight := 20;
    ColWidths[0]:= 64;
    Font.Color := clNavy;


    ColWidths[CintNum]:= 100;
    Cells[CintNum, 0] := 'No.';

    ColWidths[CintItemCode]:= 130;
    Cells[CintItemCode, 0] := '商品コード';

    ColWidths[CintItemName]:= 80;
    Cells[CintItemName, 0] := '商品名';

    ColWidths[CintItemPrice]:= 80;
    Cells[CintItemPrice, 0] := '売価';

    ColWidths[CintItemTanni]:= 80;
    Cells[CintItemTanni, 0] := '単位';
  end;
end;

procedure TfrmItemKensaku.FormResize(Sender: TObject);
begin
  inherited;
	SG1.Align := alClient;

end;

procedure TfrmItemKensaku.SG1DblClick(Sender: TObject);
begin
	close;

end;

end.
